package com.runemate.game.internal.input.mlp.activation.impl;

import com.runemate.game.internal.input.mlp.activation.ActivationFunction;


/**
 * Linear Activation Function f(x) = x
 */
public class Linear implements ActivationFunction {
    /**
     * {@inheritDoc}
     */
    @Override
    public double activate(double x) {
        return x;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public String getIdentifier() {
        return "linear";
    }

}
