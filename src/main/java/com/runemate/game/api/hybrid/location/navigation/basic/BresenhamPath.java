package com.runemate.game.api.hybrid.location.navigation.basic;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.region.*;
import java.util.*;
import org.jetbrains.annotations.*;

/**
 * A path generated based on Bresenham's line algorithm.
 * http://en.wikipedia.org/wiki/Bresenham's_line_algorithm
 * It is a direct line between two points and doesn't navigate around objects
 * These are best used in large open areas for basic walking.
 */
public final class BresenhamPath extends CoordinatePath {
    private final List<Coordinate> path;

    private BresenhamPath(final Coordinate start, final Coordinate destination) {
        final int plane = start.getPlane();

        int currentX = start.getX();
        int desiredX = destination.getX();
        int deltaX = desiredX - currentX;
        if (deltaX < 0) {
            deltaX = -deltaX;
        }

        int currentY = start.getY();
        int desiredY = destination.getY();
        int deltaY = desiredY - currentY;
        if (deltaY < 0) {
            deltaY = -deltaY;
        }

        //Controls whether we increment or decrement x every loop until we reach its desired value
        int changeX = currentX < desiredX ? 1 : -1;
        //Controls whether we increment or decrement y every loop until we reach its desired value
        int changeY = currentY < desiredY ? 1 : -1;
        //Controls the direction and distance that we'll search for coordinates within
        int difference = deltaX - deltaY;

        ArrayList<Coordinate> path = new ArrayList<>(deltaX + deltaY);
        while (true) {
            path.add(new Coordinate(currentX, currentY, plane));
            if (currentX == desiredX && currentY == desiredY) {
                break;
            }
            int double_difference = difference << 1;
            if (double_difference > -deltaY) {
                difference -= deltaY;
                currentX += changeX;
            }
            if (double_difference < deltaX) {
                difference += deltaX;
                currentY += changeY;
            }
        }
        this.path = Collections.unmodifiableList(path);
    }

    /**
     * Builds a BresenhamPath between the local player and the destination if possible.
     *
     * @param destination An entity with a location
     * @return A BresenhamPath or null
     */
    @Nullable
    public static BresenhamPath buildTo(final Locatable destination) {
        Player local = Players.getLocal();
        if (local == null) {
            return null;
        }
        return buildBetween(local, destination);
    }

    /**
     * @see BresenhamPath#buildBetween(Locatable, Locatable)
     */
    @Deprecated
    public static BresenhamPath build(final Locatable start, final Locatable destination) {
        return buildBetween(start, destination);
    }

    /**
     * Builds a BresenhamPath between the start and the destination if possible.
     * Both start and destination must be on the same plane.
     *
     * @param start       An entity with a location to start the path generation from
     * @param destination An entity with a location to use as the destination
     * @return A BresenhamPath or null
     */
    @Nullable
    public static BresenhamPath buildBetween(final Locatable start, final Locatable destination) {
        if (start == null || destination == null) {
            return null;
        }
        Coordinate spos = start.getPosition();
        if (spos == null) {
            return null;
        }
        Coordinate dpos = destination.getPosition();
        if (dpos == null || spos.getPlane() != dpos.getPlane()) {
            return null;
        }
        return new BresenhamPath(spos, dpos);
    }

    @Override
    public List<Coordinate> getVertices() {
        return path;
    }
}
