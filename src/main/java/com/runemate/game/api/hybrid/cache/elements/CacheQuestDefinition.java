package com.runemate.game.api.hybrid.cache.elements;

import com.runemate.game.api.hybrid.cache.configs.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.location.*;
import java.util.*;
import lombok.*;
import org.jetbrains.annotations.*;

@Data
public class CacheQuestDefinition implements QuestDefinition {

    int id, rowId;
    @Nullable
    String sortName;
    @Nullable
    String displayName;
    @Nullable
    Type type;
    boolean members;
    @Nullable
    Difficulty difficulty;
    @Nullable
    Length length;
    int releaseYear;
    Date releaseDate;
    @Nullable
    Coordinate startPosition;
    int rewardPoints;
    List<Integer> progressionStatus;
    int completionStatus;
    List<SkillRequirement> skillRequirements;
    List<SkillRequirement> skillRecommendations;
    List<QuestRequirement> questRequirements;
    int questPointRequirement;
    int combatLevelRequirement;
    int combatLevelRecommended;

    public CacheQuestDefinition() {
        id = -1;
        progressionStatus = new ArrayList<>();
        completionStatus = -1;
        skillRequirements = new ArrayList<>();
        skillRecommendations = new ArrayList<>();
        questRequirements = new ArrayList<>();
    }

    public void decode(DBRow row) {
        if (row == null || row.getTableId() != 0) {
            throw new IllegalArgumentException("'row' must have a tableId of 0");
        }
        final var values = row.getColumnValues();
        if (values == null || values.length < 36) {
            throw new IllegalStateException("'row' should have at least 36 values");
        }

        setId((int) readFirst(values, ColumnID.ID, -1));
        setRowId(row.getId());
        setSortName((String) readFirst(values, ColumnID.SORT_NAME, null));
        setDisplayName((String) readFirst(values, ColumnID.NAME, null));

        var type = (int) readFirst(values, ColumnID.TYPE, -1);
        if (type != -1) {
            for (Type t : Type.values()) {
                if (t.getId() == type) {
                    setType(t);
                    break;
                }
            }
        }

        setMembers((int) readFirst(values, ColumnID.MEMBERS, 0) == 1);

        var difficulty = (int) readFirst(values, ColumnID.DIFFICULTY, -1);
        if (difficulty != -1) {
            for (Difficulty d : Difficulty.values()) {
                if (d.getId() == difficulty) {
                    setDifficulty(d);
                    break;
                }
            }
        }

        var length = (int) readFirst(values, ColumnID.LENGTH, -1);
        if (length != -1) {
            for (Length l : Length.values()) {
                if (l.getId() == length) {
                    setLength(l);
                    break;
                }
            }
        }

        List<Object> releaseData = readAll(values, ColumnID.RELEASE_DATE);
        if (releaseData != null && releaseData.size() == 3) {
            Calendar calendar = Calendar.getInstance();
            calendar.clear();
            calendar.set(Calendar.DAY_OF_MONTH, (int) releaseData.get(0));
            calendar.set(Calendar.MONTH, (int) releaseData.get(1));
            calendar.set(Calendar.YEAR, (int) releaseData.get(2));
            setReleaseDate(calendar.getTime());
        }

        var pos = (int) readFirst(values, ColumnID.STARTPOS, -1);
        if (pos != -1) {
            //x/y are inverted here, so we can't use Coordinate(int) constructor
            var y = pos & 0x3fff;
            var x = pos >> 14 & 0x3fff;
            var z = pos >> 28 & 0x3;
            setStartPosition(new Coordinate(x, y, z));
        }

        setRewardPoints((int) readFirst(values, ColumnID.POINTS, 0));

        for (final var status : readAll(values, ColumnID.STATUS_PROGRESS)) {
            progressionStatus.add((int) status);
        }

        setCompletionStatus((int) readFirst(values, ColumnID.STATUS_COMPLETE, -1));

        final var statRequirements = readAll(values, ColumnID.SKILLS_REQUIRED);
        for (int i = 0; i < statRequirements.size(); i += 2) {
            var req = new SkillRequirement(Skills.getByIndex((int) statRequirements.get(i)), (int) statRequirements.get(i + 1));
            skillRequirements.add(req);
        }

        final var statRecommendations = readAll(values, ColumnID.SKILLS_RECOMMENDED);
        for (int i = 0; i < statRecommendations.size(); i += 2) {
            var req = new SkillRequirement(Skills.getByIndex((int) statRecommendations.get(i)), (int) statRecommendations.get(i + 1));
            skillRecommendations.add(req);
        }

        for (final var quest : readAll(values, ColumnID.QUEST_REQ)) {
            final var req = new QuestRequirement(QuestDefinitions.loadByCacheId((int) quest));
            questRequirements.add(req);
        }

        setQuestPointRequirement((int) readFirst(values, ColumnID.POINT_REQ, 0));
        setCombatLevelRequirement((int) readFirst(values, ColumnID.COMBAT_REQ, 0));
        setCombatLevelRecommended((int) readFirst(values, ColumnID.COMBAT_REC, 0));
    }

    private Object readFirst(Object[][] arr, int index, Object def) {
        return arr[index] == null ? def : arr[index][0];
    }

    private List<Object> readAll(Object[][] arr, int index) {
        if (arr[index] != null && arr[index].length > 0) {
            return Arrays.asList(arr[index]);
        }
        return Collections.emptyList();
    }

    private interface ColumnID {

        int ID = 0;
        int SORT_NAME = 1;
        int NAME = 2;
        int AUTO_DISABLE = 3;
        int TYPE = 4;
        int MEMBERS = 5;
        int DIFFICULTY = 6;
        int LENGTH = 7;
        int LOCATION = 8;
        int RELEASE_DATE = 9;
        int STORYLINE = 10;
        int STORY_SECTION = 11;
        int UNKNOWN_12 = 12;
        int STARTPOS = 13;
        int START_NPC = 14;
        int MAPICON = 16;
        int POINTS = 17;
        int STATUS_PROGRESS = 18;
        int STATUS_COMPLETE = 19;
        int SKILLS_REQUIRED = 23;
        int SKILLS_RECOMMENDED = 24;
        int QUEST_REQ = 25;
        int POINT_REQ = 26;
        int COMBAT_REQ = 27;
        int COMBAT_REC = 28;
        int SPEEDRUNNING_MODE = 29;
        int SPEEDRUNNING_BRONZE = 30;
        int SPEEDRUNNING_SILVER = 31;
        int SPEEDRUNNING_GOLD = 32;
        int SPEEDRUNNING_PLATINUM = 33;
        int SPEEDRUNNING_INV = 34;
        int SPEEDRUNNING_WORN = 35;
        int SPEEDRUNNING_BANK = 36;
    }
}
