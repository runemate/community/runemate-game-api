package com.runemate.game.api.hybrid.local;

/**
 * Information of the currently logged-in player's account.
 */
public class AccountInfo {

    public static int getDaysOfMembershipRemaining() {
        return Varps.getAt(1780).getValue();
    }

    public static boolean isMember() {
        return Varcs.getInt(103) == 1;
    }

    /**
     * Gets the account type associated with the currently logged-in player.
     *
     * @return the associated AccountType.
     */
    public static AccountType getType() {
        final var varbit = Varbits.load(VarbitID.ACCOUNT_TYPE.getId());
        if (varbit == null) {
            return AccountType.REGULAR;
        }
        switch (varbit.getValue()) {
            case 1:
                return AccountType.IRONMAN;
            case 2:
                return AccountType.ULTIMATE_IRONMAN;
            case 3:
                return AccountType.HARDCORE_IRONMAN;
            case 4:
                return AccountType.GROUP_IRONMAN;
            case 5:
                return AccountType.HARDCORE_GROUP_IRONMAN;
            case 6:
                return AccountType.UNRANKED_GROUP_IRONMAN;
            default:
                return AccountType.REGULAR;
        }
    }

    /**
     * Known account types.
     */
    public enum AccountType {
        REGULAR,
        IRONMAN,
        ULTIMATE_IRONMAN,
        HARDCORE_IRONMAN,
        GROUP_IRONMAN,
        HARDCORE_GROUP_IRONMAN,
        UNRANKED_GROUP_IRONMAN;

        public boolean isIronman() {
            return this != REGULAR;
        }

        public boolean isGroupIronman() {
            return this == GROUP_IRONMAN || this == HARDCORE_GROUP_IRONMAN || this == UNRANKED_GROUP_IRONMAN;
        }
    }
}
