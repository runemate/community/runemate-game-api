package com.runemate.game.api.hybrid.queries;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.hybrid.util.collections.*;
import java.awt.*;
import java.util.List;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class NpcQueryBuilder extends ActorQueryBuilder<Npc, NpcQueryBuilder> {
    private int[] ids, appearance, levels;
    private Integer minLevel, maxLevel;
    private Pattern[] actions;
    private Map<Color, Color> colorSubstitutions;

    public final NpcQueryBuilder actions(final String... actions) {
        return actions(
            Regex.getPatternsForExactStrings(actions).toArray(new Pattern[actions.length]));
    }

    public final NpcQueryBuilder actions(final Pattern... actions) {
        this.actions = actions;
        return get();
    }

    @Override
    public NpcQueryBuilder get() {
        return this;
    }

    @Override
    public Callable<List<? extends Npc>> getDefaultProvider() {
        return () -> Npcs.getLoaded().asList();
    }

    public NpcQueryBuilder appearance(final int... appearance) {
        this.appearance = appearance;
        return get();
    }

    public NpcQueryBuilder minLevel(final int minLevel) {
        this.minLevel = minLevel;
        return get();
    }

    public NpcQueryBuilder maxLevel(final int maxLevel) {
        this.maxLevel = maxLevel;
        return get();
    }

    public NpcQueryBuilder levels(int... levels) {
        this.levels = levels;
        return get();
    }

    public NpcQueryBuilder levels(Collection<Integer> levels) {
        return levels(levels.stream().mapToInt(i -> i).toArray());
    }

    public NpcQueryBuilder ids(final int... ids) {
        this.ids = ids;
        return get();
    }

    public final NpcQueryBuilder colorSubstitutions(Color original, Color substitution) {
        Map<Color, Color> m = new HashMap<>(1);
        m.put(original, substitution);
        return colorSubstitutions(m);
    }

    public final NpcQueryBuilder colorSubstitutions(Pair<Color, Color> substitution) {
        Map<Color, Color> m = new HashMap<>(1);
        m.put(substitution.getLeft(), substitution.getRight());
        return colorSubstitutions(m);
    }

    public final NpcQueryBuilder colorSubstitutions(Map<Color, Color> substitutions) {
        this.colorSubstitutions = substitutions;
        return get();
    }

    @Override
    public boolean accepts(Npc argument) {
        NpcDefinition definition = null;

        boolean condition;
        if (ids != null) {
            condition = false;
            int id = argument.getId();
            for (final int value : ids) {
                if (value == id) {
                    condition = true;
                    break;
                }
            }
            if (!condition) {
                definition = argument.getActiveDefinition();
                if (definition != null) {
                    id = definition.getId();
                    for (final int value : ids) {
                        if (value == id) {
                            condition = true;
                            break;
                        }
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        int currentLevel = -1;
        if (minLevel != null) {
            if (currentLevel == -1) {
                currentLevel = argument.getLevel();
            }
            if (currentLevel < minLevel) {
                return false;
            }
        }
        if (maxLevel != null) {
            if (currentLevel == -1) {
                currentLevel = argument.getLevel();
            }
            if (currentLevel > maxLevel) {
                return false;
            }
        }

        if (levels != null) {
            condition = false;
            if (currentLevel == -1) {
                currentLevel = argument.getLevel();
            }
            for (int level : levels) {
                if (level == currentLevel) {
                    condition = true;
                    break;
                }
            }
            if (!condition) {
                return false;
            }
        }

        if (appearance != null) {
            condition = false;
            if (definition == null) {
                definition = argument.getActiveDefinition();
            }
            if (definition != null) {
                List<Integer> appearance = definition.getAppearance();
                outer:
                for (final int value : appearance) {
                    for (final int value2 : this.appearance) {
                        if (value == value2) {
                            condition = true;
                            break outer;
                        }
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (actions != null) {
            if (definition == null) {
                definition = argument.getActiveDefinition();
            }
            condition = false;
            if (definition != null) {
                List<String> actions = definition.getActions();
                outer:
                for (final String string : actions) {
                    for (final Pattern pattern : this.actions) {
                        if (pattern.matcher(string).find()) {
                            condition = true;
                            break outer;
                        }
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (colorSubstitutions != null) {
            if (definition == null) {
                definition = argument.getActiveDefinition();
            }
            condition = false;
            if (definition != null) {
                Map<Color, Color> substitutions = definition.getColorSubstitutions();
                outer:
                for (Map.Entry<Color, Color> as : substitutions.entrySet()) {
                    for (Map.Entry<Color, Color> cs : colorSubstitutions.entrySet()) {
                        if (Objects.equals(as.getKey(), cs.getKey())
                            && Objects.equals(as.getValue(), cs.getValue())) {
                            condition = true;
                            break outer;
                        }
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        return super.accepts(argument);
    }
}
