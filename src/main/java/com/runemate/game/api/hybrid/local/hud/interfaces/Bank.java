package com.runemate.game.api.hybrid.local.hud.interfaces;

import com.runemate.game.api.client.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.net.*;
import com.runemate.game.api.hybrid.player_sense.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.api.script.*;
import com.runemate.game.api.script.annotations.*;
import com.runemate.game.api.script.exceptions.*;
import com.runemate.game.api.script.framework.*;
import java.util.*;
import java.util.function.*;
import java.util.regex.*;
import org.jetbrains.annotations.*;
import lombok.extern.log4j.*;
import lombok.*;

@Log4j2
public final class Bank {

    private Bank() {
    }

    public static SpriteItemQueryBuilder newQuery() {
        return new SpriteItemQueryBuilder(Inventories.Documented.BANK);
    }

    public static boolean isOpen() {
        return OSRSBank.isOpen();
    }

    /**
     * Opens the nearest visible bank.
     * If the bank is already opened, this method will return true and do nothing.
     */
    public static boolean open() {
        return open(Banks.newQuery().visible().results().nearest());
    }

    /**
     * Opens the specified bank.
     * If the bank is already opened, this method will return true and do nothing.
     */
    public static boolean open(LocatableEntity bank) {
        return open(bank, Regex.getPatternForExactStrings("Bank", "Open", "Use"));
    }

    /**
     * Opens the specified bank with the specified action if it's visible.
     * If the bank is already opened, this method will return true and do nothing.
     */
    public static boolean open(LocatableEntity bank, String action) {
        return open(bank, Regex.getPatternForExactString(action));
    }

    /**
     * Opens the specified bank with the specified action if it's visible.
     * If the bank is already opened, this method will return true and do nothing.
     */
    public static boolean open(LocatableEntity bank, Pattern action) {
        if (GrandExchange.isOpen()) {
            GrandExchange.close();
        }
        if (isOpen()) {
            return true;
        }
        if (bank != null && bank.isVisible()) {
            SpriteItem selected = Inventory.getSelectedItem();
            if (selected != null) {
                selected.click();
            }
            if (bank.interact(action)) {
                Player local = Players.getLocal();
                return local != null &&
                    Execution.delayUntil(Bank::isOpen, local::isMoving, 1200, 1800);
            }
        }
        return false;
    }

    /**
     * Closes the bank if open
     */
    public static boolean close() {
        return close(PlayerSense.getAsBoolean(PlayerSense.Key.CLOSE_BANK_WITH_ESCAPE));
    }

    public static boolean close(boolean hotkey) {
        return OSRSBank.close(hotkey);
    }

    public static boolean deposit(final String name, final int amount) {
        return deposit(Items.getNamePredicate(name), amount);
    }

    public static boolean deposit(final int id, final int amount) {
        return deposit(spriteItem -> spriteItem.getId() == id, amount);
    }

    /**
     * Gets the currently opened bank tab where 0 is the main tab. When using RuneLite some plugins will create fake bank tabs which
     * RuneMate does not support - if open, {@code #getCurrentTab()} will return -1.
     */
    public static int getCurrentTab() {
        int value = Optional.ofNullable(Varbits.load(VarbitID.CURRENT_BANK_TAB.getId()))
            .map(Varbit::getValue)
            .orElse(-1);

        if (value == 0) {
            //Some RuneLite plugins will open "fake" bank tabs - the best way I have found to identify these is by checking the bank title
            boolean hasPluginTabOpen = Optional.ofNullable(Interfaces.getAt(12, 3))
                .map(InterfaceComponent::getRawText)
                .map(t -> t.contains("Tag tab") || t.contains("<col=ff0000>"))
                .orElse(false);

            if (hasPluginTabOpen) {
                return -1;
            }
        }

        return value;
    }

    /**
     * Deposits the first item that matches the specified filter.
     * If amount is equal to 0 it will deposit all of the item.
     * If amount is equal to -1 it will deposit all of the item
     */
    public static boolean deposit(final Predicate<SpriteItem> filter, final int amount) {
        final SpriteItem item = Inventory.getItems(filter).first();
        return item != null && deposit(item, amount);
    }

    public static boolean deposit(final Pattern name, final int amount) {
        final SpriteItem item = Inventory.getItems(name).first();
        return item != null && deposit(item, amount);
    }

    public static boolean deposit(final SpriteItem item, final int amount) {
        if (amount < -1) {
            throw new IllegalArgumentException(amount + " is not a valid value for Bank.deposit's amount argument.");
        } else if (amount == Integer.MAX_VALUE) {
            log.warn("Integer.MAX_VALUE was provided as the deposit amount, if you simply want to deposit all pass in 0.");
        }
        return OSRSBank.deposit(item, amount);
    }

    public static boolean depositAllExcept(final Pattern... names) {
        return depositAllExcept(Items.getNamePredicate(names));
    }

    public static boolean depositAllExcept(final String... names) {
        return depositAllExcept(item -> {
            final ItemDefinition definition = item.getDefinition();
            if (definition != null) {
                final String itemName = definition.getName();
                for (final String name : names) {
                    if (Objects.equals(name, itemName)) {
                        return true;
                    }
                }
            }
            return false;
        });
    }

    public static boolean depositAllExcept(final int... ids) {
        return depositAllExcept(item -> {
            final int itemId = item.getId();
            for (final int id : ids) {
                if (itemId == id) {
                    return true;
                }
            }
            return false;
        });
    }

    /**
     * Deposits all of the items except those that match the exclusions filter.
     * If exclusions is null, it redirects to the depositInventory() method.
     *
     * @return true if ALL of the items that don't match the filter are deposited successfully.
     */
    public static boolean depositAllExcept(final Predicate<SpriteItem> exclusion) {
        if (exclusion == null || !Inventory.containsAnyOf(exclusion)) {
            return depositInventory();
        }
        boolean result = true;
        for (final SpriteItem item : Inventory.getItems()) {
            //verify that it wasn't already deposited
            if (item.isValid() && !exclusion.test(item) && !deposit(item.getId(), 0)) {
                result = false;
            }
        }
        return result;
    }

    /**
     * Withdraws the item with the specified name
     */
    public static boolean withdraw(final String name, final int amount) {
        return withdraw(Items.getNamePredicate(name), amount);
    }

    public static boolean withdraw(final int id, final int amount) {
        return withdraw(item -> item.getId() == id, amount);
    }

    public static boolean withdraw(final Pattern name, final int amount) {
        return withdraw(Items.getNamePredicate(name), amount);
    }

    /**
     * Withdraws the first item matching the specified filter.
     * To withdraw all of a specific item pass in 0 for the amount
     */
    public static boolean withdraw(final Predicate<SpriteItem> filter, final int amount) {
        SpriteItem item = Bank.getItems(filter).first();
        return item != null && withdraw(item, amount);
    }

    /**
     * Withdraws the specified quantity of an item from the bank
     *
     * @param item   the {@link SpriteItem} to be withdrawn
     * @param amount The amount of the item to withdraw, or 0 to withdraw all, or -1 to withdraw all but one.
     * @return true if it was able to successfully withdraw the item
     */
    public static boolean withdraw(final SpriteItem item, final int amount) {
        if (amount < -1) {
            throw new IllegalArgumentException(amount + " is not a valid value for Bank.withdraw's amount argument.");
        } else if (amount == Integer.MAX_VALUE) {
            log.warn("Integer.MAX_VALUE was provided as the withdraw amount, if you want to withdraw all of the item instead pass in 0.");
        }
        return OSRSBank.withdraw(item, amount);
    }

    /**
     * Returns the index of the bank tab containing `item`, where `0` is the main tab
     * @param item the {@link SpriteItem} to lookup
     * @return index of the containing bank tab.
     */
    public static int getTabContaining(@NonNull SpriteItem item) {
        if (!SpriteItem.Origin.BANK.equals(item.getOrigin())) {
            return -1;
        }

        final var tabSizeVarbits = Arrays.asList(
            VarbitID.BANK_TAB_ONE_COUNT,
            VarbitID.BANK_TAB_TWO_COUNT,
            VarbitID.BANK_TAB_THREE_COUNT,
            VarbitID.BANK_TAB_FOUR_COUNT,
            VarbitID.BANK_TAB_FIVE_COUNT,
            VarbitID.BANK_TAB_SIX_COUNT,
            VarbitID.BANK_TAB_SEVEN_COUNT,
            VarbitID.BANK_TAB_EIGHT_COUNT,
            VarbitID.BANK_TAB_NINE_COUNT
        );

        var running = 0;
        for (int i = 0; i < tabSizeVarbits.size(); i++) {
            final VarbitID tabSizeVarbit = tabSizeVarbits.get(i);
            final var varbit = Varbits.load(tabSizeVarbit.getId());
            if (varbit == null) {
                return -1;
            }

            final var tabSize = varbit.getValue();
            if (tabSize == 0) { //Tab is not in use, therefore slot must be in the "main" tab
                return 0;
            }

            running += tabSize;
            if (item.getIndex() < running) {
                return i + 1;
            }
        }

        //Not in any of the tabs 1-9, so must be in the main tab
        return 0;
    }

    @Deprecated
    public static boolean isUsingCachedContents() {
        return false;
    }

    @Deprecated
    public static void useCachedContents(boolean enable) {

    }

    @Deprecated
    public static boolean isUsingCachedContents(AbstractBot bot) {
        return false;
    }

    @Deprecated
    public static void useCachedContents(AbstractBot bot, boolean enable) {

    }

    public static SpriteItemQueryResults getItems() {
        return newQuery().results();
    }

    public static SpriteItemQueryResults getItems(final Predicate<SpriteItem> filter) {
        return newQuery().filter(filter).results();
    }

    public static SpriteItemQueryResults getItems(final int... ids) {
        return getItems(spriteItem -> {
            for (final int id : ids) {
                if (id == spriteItem.getId()) {
                    return true;
                }
            }
            return false;
        });
    }

    public static SpriteItemQueryResults getItems(final String... names) {
        return getItems(item -> {
            final ItemDefinition definition = item.getDefinition();
            if (definition != null) {
                final String itemName = definition.getName();
                for (final String name : names) {
                    if (name.equals(itemName)) {
                        return true;
                    }
                }
            }
            return false;
        });
    }

    public static boolean openFirstTab() {
        return isOpen() && (OSRSBank.openMainTab());
    }

    public static SpriteItemQueryResults getItems(final Pattern... names) {
        return getItems(Items.getNamePredicate(names));
    }

    /**
     * Checks if any items match the given {@link Predicate filter}
     *
     * @param filter the predicate to check the items against
     * @return true if an item matches the filter
     */
    public static boolean contains(Predicate<SpriteItem> filter) {
        return Items.contains(getItems(), filter);
    }

    /**
     * Checks if any items match the given id
     *
     * @param id the id to check the items against
     * @return true if an item matches the id
     */
    public static boolean contains(int id) {
        return Items.contains(getItems(), id);
    }

    /**
     * Checks if any items match the given name
     *
     * @param name the name to check the items against
     * @return true if an item matches the name
     */
    public static boolean contains(String name) {
        return Items.contains(getItems(), name);
    }

    /**
     * Checks if any items match the given name
     *
     * @param name the name to check the items against
     * @return true if an item matches the name
     */
    public static boolean contains(Pattern name) {
        return Items.contains(getItems(), name);
    }

    /**
     * Checks if the supplied {@link Predicate filter} matches at least one item
     *
     * @param predicate the predicate to check the items against
     * @return true if the predicate matches an item
     */
    public static boolean containsAllOf(final Predicate<SpriteItem> predicate) {
        return Items.containsAllOf(getItems(), predicate);
    }

    /**
     * Checks if all of the supplied {@link Predicate filter}s match at least one item each
     *
     * @param filters the predicates to check the items against
     * @return true if all of the predicates have a match
     */
    @SafeVarargs
    public static boolean containsAllOf(final Predicate<SpriteItem>... filters) {
        return Items.containsAllOf(getItems(), filters);
    }

    /**
     * Checks if all of the supplied ids match at least one item each
     *
     * @param ids the ids to check the items against
     * @return true if all of the ids have a match
     */
    public static boolean containsAllOf(final int... ids) {
        return Items.containsAllOf(getItems(), ids);
    }

    /**
     * Checks if all of the supplied names match at least one item each
     *
     * @param names the names to check the items against
     * @return true if all of the names have a match
     */
    public static boolean containsAllOf(final String... names) {
        return Items.containsAllOf(getItems(), names);
    }

    /**
     * Checks if all of the supplied names match at least one item each
     *
     * @param names the names to check the items against
     * @return true if all of the names have a match
     */
    public static boolean containsAllOf(final Pattern... names) {
        return Items.containsAllOf(getItems(), names);
    }

    /**
     * Checks if any items don't match the given {@link Predicate filter}
     *
     * @param filter the predicate to check the items against
     * @return true if at least one item doesn't match the filter
     */
    public static boolean containsAnyExcept(final Predicate<SpriteItem> filter) {
        return Items.containsAnyExcept(getItems(), filter);
    }

    /**
     * Checks if any items don't match the given {@link Predicate filter}s
     *
     * @param filters the predicates to check the items against
     * @return true if at least one item doesn't match the filters
     */
    @SafeVarargs
    public static boolean containsAnyExcept(final Predicate<SpriteItem>... filters) {
        return Items.containsAnyExcept(getItems(), filters);
    }

    /**
     * Checks if any items don't match the given ids
     *
     * @param ids the ids to check the items against
     * @return true if at least one item doesn't match the ids
     */
    public static boolean containsAnyExcept(final int... ids) {
        return Items.containsAnyExcept(getItems(), ids);
    }

    /**
     * Checks if any items don't match the given names
     *
     * @param names the names to check the items against
     * @return true if at least one item doesn't match the names
     */
    public static boolean containsAnyExcept(final String... names) {
        return Items.containsAnyExcept(getItems(), names);
    }

    /**
     * Checks if any items don't match the given names
     *
     * @param names the names to check the items against
     * @return true if at least one item doesn't match the names
     */
    public static boolean containsAnyExcept(final Pattern... names) {
        return Items.containsAnyExcept(getItems(), names);
    }

    /**
     * Checks if any item matches the given {@link Predicate filter}
     *
     * @param filter the filter to check the items against
     * @return true if at least one item matches the filter
     */
    public static boolean containsAnyOf(final Predicate<SpriteItem> filter) {
        return Items.containsAnyOf(getItems(), filter);
    }

    /**
     * Checks if any item matches the given {@link Predicate filter}s
     *
     * @param filters the filters to check the items against
     * @return true if at least one item matches a filter
     */
    @SafeVarargs
    public static boolean containsAnyOf(final Predicate<SpriteItem>... filters) {
        return Items.containsAnyOf(getItems(), filters);
    }

    /**
     * Checks if any item matches the given ids
     *
     * @param ids the ids to check the items against
     * @return true if at least one item matches an id
     */
    public static boolean containsAnyOf(final int... ids) {
        return Items.containsAnyOf(getItems(), ids);
    }

    /**
     * Checks if any item matches the given names
     *
     * @param names the names to check the items against
     * @return true if at least one item matches a name
     */
    public static boolean containsAnyOf(final String... names) {
        return Items.containsAnyOf(getItems(), names);
    }

    /**
     * Checks if any item matches the given names
     *
     * @param names the names to check the items against
     * @return true if at least one item matches a name
     */
    public static boolean containsAnyOf(final Pattern... names) {
        return Items.containsAnyOf(getItems(), names);
    }

    /**
     * Checks if all of the items match the given {@link Predicate filter}
     *
     * @param filter the filter to check the items against
     * @return true if all items match the filter
     */
    public static boolean containsOnly(final Predicate<SpriteItem> filter) {
        return Items.containsOnly(getItems(), filter);
    }

    /**
     * Checks if all of the items match the given {@link Predicate filter}s
     *
     * @param filters the filters to check the items against
     * @return true if all items match at least one filter each
     */
    @SafeVarargs
    public static boolean containsOnly(final Predicate<SpriteItem>... filters) {
        return Items.containsOnly(getItems(), filters);
    }

    /**
     * Checks if all of the items match the given names
     *
     * @param names the filters to check the items against
     * @return true if all items match at least one name each
     */
    public static boolean containsOnly(final String... names) {
        return Items.containsOnly(getItems(), names);
    }

    /**
     * Checks if all of the items match the given names
     *
     * @param names the filters to check the items against
     * @return true if all items match at least one name each
     */
    public static boolean containsOnly(final Pattern... names) {
        return Items.containsOnly(getItems(), names);
    }

    /**
     * Gets the total quantity of items
     *
     * @return the total quantity of items
     */
    public static int getQuantity() {
        return Items.getQuantity(getItems());
    }

    /**
     * Gets the total quantity of items matching the filter
     *
     * @param filter the filter to check the items against
     * @return the total quantity of items matching the filter
     */
    public static int getQuantity(final Predicate<SpriteItem> filter) {
        return Items.getQuantity(getItems(), filter);
    }

    /**
     * Gets the total quantity of items matching the {@link Predicate filter}s
     *
     * @param filters the filters to check the items against
     * @return the total quantity of items matching the filters
     */
    @SafeVarargs
    public static int getQuantity(final Predicate<SpriteItem>... filters) {
        return Items.getQuantity(getItems(), filters);
    }

    /**
     * Gets the total quantity of items matching the ids
     *
     * @param ids the ids to check the items against
     * @return the total quantity of items matching the ids
     */
    public static int getQuantity(final int... ids) {
        return Items.getQuantity(getItems(), ids);
    }

    /**
     * Gets the total quantity of items matching the names
     *
     * @param names the ids to check the items against
     * @return the total quantity of items matching the names
     */
    public static int getQuantity(final String... names) {
        return Items.getQuantity(getItems(), names);
    }

    /**
     * Gets the total quantity of items matching the names
     *
     * @param names the ids to check the items against
     * @return the total quantity of items matching the names
     */
    public static int getQuantity(final Pattern... names) {
        return Items.getQuantity(getItems(), names);
    }

    public static boolean isEmpty() {
        return getItems().isEmpty();
    }

    public static SpriteItem getItemIn(final int slot) {
        return newQuery().indices(slot).results().first();
    }

    public static InteractableRectangle getViewport() {
        return OSRSBank.getViewport();
    }

    public static List<InteractableRectangle> getSlotBounds() {
        return OSRSBank.getSlotBounds();
    }

    public static InteractableRectangle getBoundsOf(final int index) {
        return OSRSBank.getBoundsOf(index);
    }

    /**
     * Clicks the deposit inventory button and waits
     *
     * @return true if the entire inventory was deposited
     */
    public static boolean depositInventory() {
        return OSRSBank.depositInventory();
    }

    /**
     * Clicks the deposit worn items button
     *
     * @return true if all equipment was deposited
     */
    public static boolean depositEquipment() {
        return OSRSBank.depositWornItems();
    }

    public static WithdrawMode getWithdrawMode() {
        Varbit varbit = Varbits.load(3958);
        if (varbit == null) {
            return null;
        }
        return varbit.getValue() == 1 ? WithdrawMode.NOTE : WithdrawMode.ITEM;
    }

    public static boolean setWithdrawMode(WithdrawMode mode) {
        if (mode == null || !Bank.isOpen()) {
            return false;
        }
        if (mode.equals(getWithdrawMode())) {
            return true;
        }
        InterfaceComponent toggle = Interfaces.newQuery()
            .containers(12).types(InterfaceComponent.Type.LABEL).grandchildren(false)
            .texts(WithdrawMode.ITEM.equals(mode) ? "Item" : "Note").results().first();
        return toggle != null && toggle.isVisible()
            && toggle.interact(WithdrawMode.ITEM.equals(mode) ? "Item" : "Note")
            && Execution.delayUntil(() -> mode.equals(getWithdrawMode()), 600, 1200);
    }

    public static boolean setDefaultQuantity(final DefaultQuantity quantity) {
        if (quantity == null || !Bank.isOpen()) {
            return false;
        }
        if (quantity.isSelected()) {
            return true;
        }
        final InterfaceComponent component = quantity.getComponent();
        return component != null && component.click() &&
            Execution.delayUntil(quantity::isSelected, 600, 1200);
    }

    /**
     * Sets the "Default Quantity" when left-clicking inside the OSRS bank.
     * <p>
     * Currently only an OSRS feature.
     *
     * @param quantity - the DefaultQuantity representing the quantity to be withdrawn.
     * @return true if the DefaultwQuantity has been set, false otherwise.
     */
    public static boolean setDefaultQuantity(final int quantity) {
        if (quantity == 1) {
            return setDefaultQuantity(DefaultQuantity.ONE);
        }
        if (quantity == 5) {
            return setDefaultQuantity(DefaultQuantity.FIVE);
        }
        if (quantity == 10) {
            return setDefaultQuantity(DefaultQuantity.TEN);
        }
        if (quantity == 0) {
            return setDefaultQuantity(DefaultQuantity.ALL);
        }
        if (!InputDialog.isOpen()) {
            final InterfaceComponent component = DefaultQuantity.X.getComponent();
            if (component == null || !(
                component.interact("Set custom quantity") &&
                    Execution.delayUntil(InputDialog::isOpen, 600, 1200)
            )) {
                return false;
            }
        }
        if (InputDialog.isOpen()) {
            return InputDialog.enterAmount(quantity) &&
                Execution.delayUntil(DefaultQuantity.X::isSelected, 600, 1200);
        }
        return false;
    }

    public static DefaultQuantity getDefaultQuantity() {
        return DefaultQuantity.getSelected();
    }

    public static int getExactDefaultQuantity() {
        DefaultQuantity defaultQuantity = getDefaultQuantity();
        switch (defaultQuantity) {
            case ONE:
                return 1;
            case FIVE:
                return 5;
            case TEN:
                return 10;
            case ALL:
                return 0;
            case X:
                final Varbit varbit = Varbits.load(OSRSBank.CUSTOM_DEFAULT_QUANTITY_VARBIT);
                return varbit != null ? varbit.getValue() : Integer.MIN_VALUE;
            default:
                throw new IllegalStateException("Unexpected value: " + defaultQuantity);
        }
    }

    public enum DefaultQuantity {
        ONE(0, 2, "1"),
        FIVE(1, 3, "5"),
        TEN(2, 4, "10"),
        X(3, 5, "X"),
        ALL(4, 7, "All");
        private final int osrsValue;
        private final int rs3Value;
        private final String icText;

        DefaultQuantity(int osrsValue, int rs3Value, String icText) {
            this.osrsValue = osrsValue;
            this.rs3Value = rs3Value;
            this.icText = icText;
        }

        @Nullable
        public static DefaultQuantity getSelected() {
            boolean rs3 = false;
            final int selected = getSelectedId();
            for (DefaultQuantity q : values()) {
                if ((rs3 ? q.rs3Value : q.osrsValue) == selected) {
                    return q;
                }
            }
            return null;
        }

        private static int getSelectedId() {
            final Varbit varbit = Varbits.load(OSRSBank.DEFAULT_QUANTITY_VARBIT);
            return varbit == null ? 0 : varbit.getValue();
        }

        public String getText() {
            return icText;
        }

        public boolean isSelected() {
            return getSelectedId() == osrsValue;
        }

        private InterfaceComponent getComponent() {
            return Interfaces.newQuery().containers(12).widths(25).heights(22)
                .types(InterfaceComponent.Type.LABEL).texts(icText).results().first();
        }
    }

    public enum WithdrawMode {
        ITEM,
        NOTE
    }
}
