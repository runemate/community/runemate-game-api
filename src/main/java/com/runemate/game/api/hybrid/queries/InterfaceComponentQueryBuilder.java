package com.runemate.game.api.hybrid.queries;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.api.script.annotations.*;
import com.google.common.primitives.*;
import java.awt.*;
import java.util.List;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;

public class InterfaceComponentQueryBuilder extends
    InteractableQueryBuilder<InterfaceComponent, InterfaceComponentQueryBuilder, InterfaceComponentQueryResults> {

    public boolean scanGrandchildren = true;
    public Collection<Integer> containers;
    public Collection<InterfaceComponent.Type> types;
    private int[] ids;
    private Boolean scrolls;
    private Collection<Pattern> actions, texts, names;
    private Predicate<ItemDefinition> containedItemFilter;
    private Collection<Integer> sprites;
    private Collection<Integer> fonts;
    private Collection<Integer> spriteRotations;
    private Collection<Integer> widths;
    private Collection<Integer> heights;
    private Collection<Integer> layerDepths;
    private Predicate<Integer> projectedBufferFilter;
    private Predicate<NpcDefinition> projectedNpcFilter;
    private Predicate<Player> projectedPlayerFilter;
    private Collection<Color> textColors;
    private Collection<Integer> grandchildQuantities;
    private Collection<Integer> specializationIndicators;

    @Override
    public boolean accepts(InterfaceComponent argument) {
        boolean condition;
        if (containers != null) {
            condition = false;
            final int container = argument.getId() >> 16;
            for (final int value : containers) {
                if (container == value) {
                    condition = true;
                    break;
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (ids != null) {
            condition = false;
            int id = argument.getId();
            for (final int value : ids) {
                if (value == id) {
                    condition = true;
                    break;
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (this.types != null) {
            condition = false;
            final InterfaceComponent.Type type = argument.getType();
            for (final InterfaceComponent.Type value : this.types) {
                if (type == value) {
                    condition = true;
                    break;
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (this.specializationIndicators != null) {
            condition = false;
            final int specializationIndicator = argument.getSpecializationIndicator();
            for (final int value : this.specializationIndicators) {
                if (specializationIndicator == value) {
                    condition = true;
                    break;
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (this.grandchildQuantities != null &&
            InterfaceComponent.Type.CONTAINER.equals(argument.getType())) {
            condition = false;
            final int componentCount = argument.getChildQuantity();
            for (final int count : this.grandchildQuantities) {
                if (componentCount == count) {
                    condition = true;
                    break;
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (this.fonts != null) {
            condition = false;
            final int font = argument.getFontId();
            for (final int value : this.fonts) {
                if (font == value) {
                    condition = true;
                    break;
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (this.sprites != null) {
            condition = false;
            final int sprite = argument.getSpriteId();
            if (this.sprites.contains(sprite)) {
                condition = true;
            }
            if (!condition) {
                return false;
            }
        }
        if (this.spriteRotations != null) {
            condition = false;
            final int spriteRotation = argument.getSpriteRotation();
            for (final int value : this.spriteRotations) {
                if (spriteRotation == value) {
                    condition = true;
                    break;
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (this.widths != null) {
            condition = false;
            final int width = argument.getWidth();
            for (final int value : this.widths) {
                if (width == value) {
                    condition = true;
                    break;
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (this.heights != null) {
            condition = false;
            final int height = argument.getHeight();
            for (final int value : this.heights) {
                if (height == value) {
                    condition = true;
                    break;
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (textColors != null) {
            condition = false;
            final Color color = argument.getTextColor();
            for (final Color value : textColors) {
                if (color.equals(value)) {
                    condition = true;
                    break;
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (scrolls != null) {
            Point shift = argument.getScrollShift();
            if (shift != null && !scrolls) {
                return false;
            }
        }
        if (this.actions != null) {
            condition = false;
            final List<String> actions = argument.getActions();
            outer:
            for (final Pattern value : this.actions) {
                for (String action : actions) {
                    if (action != null && value.matcher(action).find()) {
                        condition = true;
                        break outer;
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (this.names != null) {
            condition = false;
            final String name = argument.getName();
            if (name != null) {
                for (final Pattern value : this.names) {
                    if (value.matcher(name).find()) {
                        condition = true;
                        break;
                    }
                }
            }

            if (!condition) {
                return false;
            }
        }
        if (this.texts != null) {
            condition = false;
            final String text = argument.getText();
            if (text != null) {
                for (final Pattern value : this.texts) {
                    if (value.matcher(text).find()) {
                        condition = true;
                        break;
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (this.containedItemFilter != null) {
            final ItemDefinition item = argument.getContainedItem();
            condition = item != null && this.containedItemFilter.test(item);
            if (!condition) {
                return false;
            }
        }
        if (this.projectedBufferFilter != null) {
            final int buffer = argument.getProjectedBufferId();
            condition = buffer != -1 && this.projectedBufferFilter.test(buffer);
            if (!condition) {
                return false;
            }
        }
        if (this.projectedNpcFilter != null) {
            final NpcDefinition npc = argument.getProjectedNpc();
            condition = npc != null && projectedNpcFilter.test(npc);
            if (!condition) {
                return false;
            }
        }
        if (this.projectedPlayerFilter != null) {
            final Player player = argument.getProjectedPlayer();
            condition = player != null && projectedPlayerFilter.test(player);
            if (!condition) {
                return false;
            }
        }
        if (super.accepts(argument)) {
            if (this.layerDepths != null) {
                condition = false;
                final int layerDepth = argument.getLayerDepth();
                for (final int value : this.layerDepths) {
                    if (layerDepth == value) {
                        condition = true;
                        break;
                    }
                }
                return condition;
            }
            return true;
        }
        return false;
    }

    public InterfaceComponentQueryBuilder ids(final int... ids) {
        this.ids = ids;
        return get();
    }

    public InterfaceComponentQueryBuilder actions(String... actions) {
        return actions(
            Regex.getPatternsForExactStrings(actions).toArray(new Pattern[actions.length]));
    }

    public InterfaceComponentQueryBuilder actions(Pattern... actions) {
        return actions(Arrays.asList(actions));
    }

    public InterfaceComponentQueryBuilder actions(Collection<Pattern> actions) {
        this.actions = actions;
        return get();
    }

    public InterfaceComponentQueryBuilder containedItem(Predicate<ItemDefinition> filter) {
        this.containedItemFilter = filter;
        return get();
    }

    public InterfaceComponentQueryBuilder containers(int... indices) {
        return containers(Ints.asList(indices));
    }

    public InterfaceComponentQueryBuilder containers(Collection<Integer> indices) {
        this.containers = indices instanceof Set ? indices : new HashSet<>(indices);
        return get();
    }

    public InterfaceComponentQueryBuilder scrolls(boolean scrolls) {
        this.scrolls = scrolls;
        return get();
    }

    public InterfaceComponentQueryBuilder grandchildren(boolean search) {
        this.scanGrandchildren = search;
        return get();
    }

    public InterfaceComponentQueryBuilder grandchildren(int quantity) {
        return grandchildren(Ints.asList(quantity));
    }

    public InterfaceComponentQueryBuilder grandchildren(int... quantities) {
        return grandchildren(Ints.asList(quantities));
    }

    public InterfaceComponentQueryBuilder grandchildren(Collection<Integer> quantities) {
        this.grandchildQuantities = quantities;
        return get();
    }

    public boolean grandchildren() {
        return this.scanGrandchildren;
    }

    public InterfaceComponentQueryBuilder heights(int... heights) {
        return heights(Ints.asList(heights));
    }

    public InterfaceComponentQueryBuilder heights(Collection<Integer> heights) {
        this.heights = heights;
        return get();
    }

    public InterfaceComponentQueryBuilder layerDepths(int... layerDepths) {
        return layerDepths(Ints.asList(layerDepths));
    }

    public InterfaceComponentQueryBuilder layerDepths(Collection<Integer> layerDepths) {
        this.layerDepths = layerDepths;
        return get();
    }

    public InterfaceComponentQueryBuilder names(String... names) {
        return names(Regex.getPatternsForExactStrings(names));
    }

    public InterfaceComponentQueryBuilder names(Collection<Pattern> names) {
        this.names = names;
        return get();
    }

    public InterfaceComponentQueryBuilder names(Pattern... names) {
        return names(Arrays.asList(names));
    }

    public InterfaceComponentQueryBuilder projectedBufferId(int... ids) {
        Arrays.sort(ids);
        return projectedBufferId(integer -> Arrays.binarySearch(ids, integer) >= 0);
    }

    public InterfaceComponentQueryBuilder projectedBufferId(Predicate<Integer> filter) {
        this.projectedBufferFilter = filter;
        return get();
    }

    public InterfaceComponentQueryBuilder projectedNpc(Predicate<NpcDefinition> filter) {
        this.projectedNpcFilter = filter;
        return get();
    }

    public InterfaceComponentQueryBuilder projectedPlayer(Predicate<Player> filter) {
        this.projectedPlayerFilter = filter;
        return get();
    }

    public InterfaceComponentQueryBuilder spriteRotations(int... spriteRotations) {
        return spriteRotations(Ints.asList(spriteRotations));
    }

    public InterfaceComponentQueryBuilder spriteRotations(Collection<Integer> spriteRotations) {
        this.spriteRotations = spriteRotations;
        return get();
    }

    public InterfaceComponentQueryBuilder sprites(int... sprites) {
        return sprites(Ints.asList(sprites));
    }

    public InterfaceComponentQueryBuilder sprites(Collection<Integer> sprites) {
        this.sprites = sprites;
        return get();
    }

    public InterfaceComponentQueryBuilder fonts(int... fonts) {
        return fonts(Ints.asList(fonts));
    }

    public InterfaceComponentQueryBuilder fonts(Collection<Integer> fonts) {
        this.fonts = fonts;
        return get();
    }

    public InterfaceComponentQueryBuilder textColors(Color... textColors) {
        return textColors(Arrays.asList(textColors));
    }

    public InterfaceComponentQueryBuilder textColors(Collection<Color> textColors) {
        this.textColors = textColors;
        return get();
    }

    public InterfaceComponentQueryBuilder textContains(String... textContains) {
        return texts(Regex.getPatternsForContainsStrings(textContains));
    }

    public InterfaceComponentQueryBuilder texts(Collection<Pattern> texts) {
        this.texts = texts;
        return get();
    }

    public InterfaceComponentQueryBuilder texts(String... texts) {
        return texts(Regex.getPatternsForExactStrings(texts));
    }

    public InterfaceComponentQueryBuilder texts(Pattern... texts) {
        return texts(Arrays.asList(texts));
    }

    public InterfaceComponentQueryBuilder types(InterfaceComponent.Type... types) {
        return types(Arrays.asList(types));
    }

    public InterfaceComponentQueryBuilder types(Collection<InterfaceComponent.Type> types) {
        this.types = types;
        return get();
    }

    public InterfaceComponentQueryBuilder widths(int... widths) {
        return widths(Ints.asList(widths));
    }

    public InterfaceComponentQueryBuilder widths(Collection<Integer> widths) {
        this.widths = widths;
        return get();
    }

    public InterfaceComponentQueryBuilder specializationIndicators(int... values) {
        return specializationIndicators(Ints.asList(values));
    }

    public InterfaceComponentQueryBuilder specializationIndicators(Collection<Integer> values) {
        this.specializationIndicators = values;
        return get();
    }

    @Override
    public InterfaceComponentQueryBuilder get() {
        return this;
    }

    @Override
    public Callable<List<? extends InterfaceComponent>> getDefaultProvider() {
        return () -> {
            List<OpenInterfaceComponent> loaded;

            if (containers == null || containers.isEmpty()) {
                loaded = OpenInterfaceComponentManager.getLoadedComponents(scanGrandchildren);
            } else {
                loaded = OpenInterfaceComponentManager.getLoadedComponents(
                    scanGrandchildren,
                    containers.stream().mapToInt(i -> i).toArray()
                );
            }

            return loaded.stream().map(OSRSInterfaceComponent::new).collect(Collectors.toList());
        };
    }

    @Override
    public InterfaceComponentQueryResults results() {
        return results(true);
    }

    @Override
    protected InterfaceComponentQueryResults results(Collection<? extends InterfaceComponent> entries, ConcurrentMap<String, Object> cache) {
        return new InterfaceComponentQueryResults(entries, cache);
    }
}
