package com.runemate.game.api.hybrid.cache.elements;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.cache.materials.*;
import com.runemate.game.api.hybrid.entities.attributes.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.hybrid.util.collections.*;
import com.runemate.game.api.script.framework.core.*;
import com.runemate.game.cache.io.*;
import com.runemate.game.cache.item.*;
import com.runemate.game.internal.*;
import com.google.common.collect.*;
import java.awt.*;
import java.io.*;
import java.util.List;
import java.util.*;
import javax.annotation.*;
import lombok.*;

@Getter
public class CacheItemDefinition extends IncrementallyDecodedItem {

    private static final String[] DEFAULT_GROUND_ACTIONS_OSRS = new String[] { null, null, "Take", null, null };
    private static final String[] DEFAULT_GROUND_ITEMS_RS3 = new String[] { null, null, null, null, "Examine" };

    private final boolean osrs = true;
    public String examine;
    String[][] subops;
    private HashMap<Long, Attribute> attributes;
    private String[] groundActions;
    private int id;
    private String name = "null";
    private int modelId = 0;
    private boolean membersOnly = false;
    private boolean stockMarket = false;
    private byte ambiance = 0;
    private byte contrast = 0;
    private int value = 1;
    private int lentLinkId = -1;
    private int lentTemplateId = -1;
    private int cosmeticLinkId = -1;
    private int cosmeticTemplateId = -1;
    private int noteLinkId = -1;
    private int noteTemplateId = -1;
    private int combineLinkId = -1;
    private int combineTemplateId = -1;
    private String shardName = "null";
    private int shardCount = 0;
    private int dummyItem = 0;
    //seems to for canBe/isDummyItem(where a value of 0 means true and one need false)
    private int primaryEquipmentSlot = -1;
    private int secondaryEquipmentSlot = -1;
    private int tertiaryEquipmentSlot = -1;
    private String[] inventoryActions = { null, null, null, null, "Drop" };
    private short[] originalColors, replacementColors;
    private short[] originalMaterialIds, replacementMaterialIds;
    private byte[] modifiedColorPalette;
    private int multiStackSize = 0;
    private int pickSizeShift = 0;
    private int[] stackIds;
    private int[] stackQuantities;
    private Stackability stackable = Stackability.SOMETIMES;
    private int weight = 0;
    private int teamId = 0;
    private int shiftClickInventoryActionIndex = -2;
    private int[] groundActionCursorIds;
    private int[] inventoryActionCursorIds;
    private int[] quests;
    private int zoom2D = 2000;
    private int xRotation2D = 0;
    private int zRotation2D = 0;
    private int yRotation2D = 0;
    private int xTranslation2D = 0;
    private int yTranslation2D = 0;
    private int xModelScale;
    private int yModelScale;
    private int zModelScale;
    private int yMaleModelTranslation = 0;
    private int yFemaleModelTranslation = 0;
    private int firstEquippedMaleModelId = -1;
    private int secondEquippedMaleModelId = -1;
    private int thirdEquippedMaleModelId = -1;
    private int firstEquippedFemaleModelId = -1;
    private int secondEquippedFemaleModelId = -1;
    private int thirdEquippedFemaleModelId = -1;
    private int firstMaleHeadModelId = -1;
    private int secondMaleHeadModelId = -1;
    private int secondFemaleHeadModelId = -1;
    private int firstFemaleHeadModelId = -1;
    private int category = -1;
    private int minimenuColor = 0;
    private boolean minimenuColorOverridden = false;
    private int placeholderLinkId = -1;
    private int placeholderTemplateId = -1;
    private int manwearxoff = 0;
    private int manwearyoff = 0;
    private int manwearzoff = 0;
    private int womanwearxoff = 0;
    private int womanwearyoff = 0;
    private int womanwearzoff = 0;

    public CacheItemDefinition(int id) {
        this.id = id;
        xModelScale = 128;
        yModelScale = 128;
        zModelScale = 128;
        groundActions = Arrays.copyOf(DEFAULT_GROUND_ACTIONS_OSRS, 5);
    }

    @Override
    protected void decode(Js5InputStream stream, int opcode) throws IOException {
        if (opcode == 1) {
            this.modelId = stream.readUnsignedShort();
        } else if (opcode == 2) {
            this.name = JagTags.remove(stream.readCStyleLatin1String());
        } else if (opcode == 3) {
            this.examine = stream.readCStyleLatin1String();
        } else if (opcode == 4) {
            this.zoom2D = stream.readUnsignedShort();
        } else if (opcode == 5) {
            this.xRotation2D = stream.readUnsignedShort();
        } else if (opcode == 6) {
            this.zRotation2D = stream.readUnsignedShort();
        } else if (opcode == 7) {
            this.xTranslation2D = stream.readUnsignedShort();
            if (this.xTranslation2D > 32767) {
                this.xTranslation2D -= 65536;
            }
        } else if (opcode == 8) {
            this.yTranslation2D = stream.readUnsignedShort();
            if (this.yTranslation2D > 32767) {
                this.yTranslation2D -= 65536;
            }
        } else if (opcode == 11) {
            this.stackable = Stackability.ALWAYS;
        } else if (opcode == 12) {
            this.value = stream.readInt();
        } else if (opcode == 13) {
            this.primaryEquipmentSlot = stream.readUnsignedByte();
        } else if (opcode == 14) {
            this.secondaryEquipmentSlot = stream.readUnsignedByte();
        } else if (opcode == 15) {
            //aboolean=false;
        } else if (opcode == 16) {
            this.membersOnly = true;
        } else if (opcode == 18) {
            this.multiStackSize = stream.readUnsignedShort();
        } else if (opcode == 23) {
            this.firstEquippedMaleModelId = stream.readUnsignedShort();
            this.yMaleModelTranslation = stream.readUnsignedByte();
        } else if (opcode == 24) {
            this.secondEquippedMaleModelId = stream.readUnsignedShort();
        } else if (opcode == 25) {
            this.firstEquippedFemaleModelId = stream.readUnsignedShort();
            this.yFemaleModelTranslation = stream.readUnsignedByte();
        } else if (opcode == 26) {
            this.secondEquippedFemaleModelId = stream.readUnsignedShort();
        } else if (opcode == 27) {
            this.tertiaryEquipmentSlot = stream.readUnsignedByte();
        } else if (opcode >= 30 && opcode < 35) {
            this.groundActions[opcode - 30] = stream.readCStyleLatin1String();
            if (this.groundActions[opcode - 30].equalsIgnoreCase("Hidden")) {
                this.groundActions[opcode - 30] = null;
            }
        } else if (opcode >= 35 && opcode < 40) {
            this.inventoryActions[opcode - 35] = stream.readCStyleLatin1String();
        } else {
            int length;
            int index;
            if (opcode == 40) {
                length = stream.readUnsignedByte();
                originalColors = new short[length];
                replacementColors = new short[length];
                for (index = 0; index < length; index++) {
                    originalColors[index] = (short) stream.readUnsignedShort();
                    replacementColors[index] = (short) stream.readUnsignedShort();
                }
            } else if (opcode == 41) {
                length = stream.readUnsignedByte();
                originalMaterialIds = new short[length];
                replacementMaterialIds = new short[length];
                for (index = 0; index < length; index++) {
                    originalMaterialIds[index] = (short) stream.readUnsignedShort();
                    replacementMaterialIds[index] = (short) stream.readUnsignedShort();
                }
            } else if (opcode == 42) {
                shiftClickInventoryActionIndex = stream.readByte();
            } else if (opcode == 43) {
                int opId = stream.readUnsignedByte();
                if (subops == null) {
                    subops = new String[5][];
                }
                boolean valid = opId < 5;
                if (valid && subops[opId] == null) {
                    subops[opId] = new String[20];
                }

                while (true) {
                    int subOpId = stream.readUnsignedByte() - 1;
                    if (subOpId == -1) {
                        break;
                    }

                    String op = stream.readCStyleLatin1String();
                    if (valid && subOpId < 20) {
                        subops[opId][subOpId] = op;
                    }
                }
            } else if (opcode == 44 || opcode == 45) {
                stream.readUnsignedShort();
            } else if (opcode == 65) {
                this.stockMarket = true;
            } else if (opcode == 75) {
                this.weight = stream.readShort();
            } else if (opcode == 78) {
                this.thirdEquippedMaleModelId = stream.readUnsignedShort();
            } else if (opcode == 79) {
                this.thirdEquippedFemaleModelId = stream.readUnsignedShort();
            } else if (opcode == 90) {
                this.firstMaleHeadModelId = stream.readUnsignedShort();
            } else if (opcode == 91) {
                this.firstFemaleHeadModelId = stream.readUnsignedShort();
            } else if (opcode == 92) {
                this.secondMaleHeadModelId = stream.readUnsignedShort();
            } else if (opcode == 93) {
                this.secondFemaleHeadModelId = stream.readUnsignedShort();
            } else if (opcode == 94) {
                this.category = stream.readUnsignedShort();
            } else if (opcode == 95) {
                this.yRotation2D = stream.readUnsignedShort();
            } else if (opcode == 96) {
                //1 is a link, 2 is a template, no guarantees
                dummyItem = stream.readUnsignedByte();
            } else if (opcode == 97) {
                this.noteLinkId = stream.readUnsignedShort();
            } else if (opcode == 98) {
                this.noteTemplateId = stream.readUnsignedShort();
            } else if (opcode >= 100 && opcode < 110) {
                if (stackIds == null) {
                    stackIds = new int[10];
                    stackQuantities = new int[10];
                }
                stackIds[opcode - 100] = stream.readUnsignedShort();
                stackQuantities[opcode - 100] = stream.readUnsignedShort();
            } else if (opcode == 110) {
                this.xModelScale = stream.readUnsignedShort();
            } else if (opcode == 111) {
                this.yModelScale = stream.readUnsignedShort();
            } else if (opcode == 112) {
                this.zModelScale = stream.readUnsignedShort();
            } else if (opcode == 113) {
                this.ambiance = stream.readByte();
            } else if (opcode == 114) {
                this.contrast = stream.readByte();
            } else if (opcode == 115) {
                this.teamId = stream.readUnsignedByte();
            } else if (opcode == 121) {
                lentLinkId = stream.readUnsignedShort();
            } else if (opcode == 122) {
                lentTemplateId = stream.readUnsignedShort();
            } else if (opcode == 125) {
                this.manwearxoff = stream.readByte() << 2;
                this.manwearyoff = stream.readByte() << 2;
                this.manwearzoff = stream.readByte() << 2;
            } else if (opcode == 126) {
                this.womanwearxoff = stream.readByte() << 2;
                this.womanwearyoff = stream.readByte() << 2;
                this.womanwearzoff = stream.readByte() << 2;
            } else if (opcode == 127 || opcode == 128 || opcode == 129 || opcode == 130) {
                stream.readUnsignedByte();
                stream.readUnsignedShort();
            } else if (opcode == 132) {
                length = stream.readUnsignedByte();
                quests = new int[length];
                for (index = 0; index < length; ++index) {
                    quests[index] = stream.readUnsignedShort();
                }
            } else if (opcode == 134) {
                pickSizeShift = stream.readUnsignedByte();
            } else if (opcode == 139) {
                cosmeticLinkId = stream.readUnsignedShort();
            } else if (opcode == 140) {
                cosmeticTemplateId = stream.readUnsignedShort();
            } else if (opcode >= 142 && opcode < 147) {
                if (null == groundActionCursorIds) {
                    groundActionCursorIds = new int[6];
                    Arrays.fill(groundActionCursorIds, -1);
                }
                groundActionCursorIds[opcode - 142] = stream.readUnsignedShort();
            } else if (opcode == 148) {
                this.placeholderLinkId = stream.readUnsignedShort();
            } else if (opcode == 149) {
                this.placeholderTemplateId = stream.readUnsignedShort();
            } else if (opcode >= 150 && opcode < 155) {
                if (inventoryActionCursorIds == null) {
                    inventoryActionCursorIds = new int[5];
                    Arrays.fill(inventoryActionCursorIds, -1);
                }
                inventoryActionCursorIds[opcode - 150] = stream.readUnsignedShort();
            } else if (opcode == 157) {
            } else if (opcode == 161) {
                combineLinkId = stream.readUnsignedShort();
            } else if (opcode == 162) {
                combineTemplateId = stream.readUnsignedShort();
            } else if (opcode == 163) {
                shardCount = stream.readUnsignedShort();
            } else if (opcode == 164) {
                shardName = JagTags.remove(stream.readCStyleLatin1String());
            } else if (opcode == 165) {
                stackable = Stackability.NEVER;
            } else if (opcode == 167) {
                //aboolean2 = true;
            } else if (opcode == 168) {
                //boolean = false;
            } else if (opcode == 249) {
                length = stream.readUnsignedByte();
                if (this.attributes == null) {
                    this.attributes = new HashMap<>(length);
                }
                for (index = 0; index < length; ++index) {
                    boolean isString = stream.readUnsignedByte() == 1;
                    int key = stream.readBytes(3);
                    this.attributes.put((long) key, new Attribute(
                        key,
                        isString ? stream.readCStyleLatin1String() : stream.readInt()
                    ));
                }
            } else {
                throw new DecodingException(opcode);
            }
        }
    }

    @Override
    public void decode(Js5InputStream stream) throws IOException {
        super.decode(stream);
        if (noteTemplateId != -1) {
            noteTransform();
        }
        if (cosmeticTemplateId != -1) {
            cosmeticTransform();
        }
        if (placeholderTemplateId != -1) {
            placeholderTransform();
        }
    }

    private void placeholderTransform() {
        Extended templateAs = (Extended) ItemDefinition.get(placeholderTemplateId);
        if (templateAs == null) {
            return;
        }
        Extended linkAs = (Extended) ItemDefinition.get(placeholderLinkId);
        if (linkAs == null) {
            return;
        }
        CacheItemDefinition template = templateAs.getBase();
        CacheItemDefinition link = linkAs.getBase();
        this.modelId = template.modelId;
        this.zoom2D = template.zoom2D;
        this.xRotation2D = template.xRotation2D;
        this.zRotation2D = template.zRotation2D;
        this.yRotation2D = template.yRotation2D;
        this.xTranslation2D = template.xTranslation2D;
        this.yTranslation2D = template.yTranslation2D;
        this.originalColors = template.originalColors;
        this.replacementColors = template.replacementColors;
        this.originalMaterialIds = template.originalMaterialIds;
        this.replacementMaterialIds = template.replacementMaterialIds;
        this.stackable = template.stackable;
        this.name = link.name;
        this.value = 0;
        this.membersOnly = false;
        this.stockMarket = false;
    }

    private void cosmeticTransform() {
        Extended templateAs = (Extended) ItemDefinition.get(cosmeticTemplateId);
        if (templateAs == null) {
            return;
        }
        Extended linkAs = (Extended) ItemDefinition.get(cosmeticLinkId);
        if (linkAs == null) {
            return;
        }
        CacheItemDefinition template = templateAs.getBase();
        CacheItemDefinition link = linkAs.getBase();
        this.modelId = template.modelId;
        this.zoom2D = template.zoom2D;
        this.xRotation2D = template.xRotation2D;
        this.zRotation2D = template.zRotation2D;
        this.yRotation2D = template.yRotation2D;
        this.xTranslation2D = template.xTranslation2D;
        this.yTranslation2D = template.yTranslation2D;
        this.originalColors = link.originalColors;
        this.replacementColors = link.replacementColors;
        this.originalMaterialIds = link.originalMaterialIds;
        this.replacementMaterialIds = link.replacementMaterialIds;
        this.name = link.name;
        this.membersOnly = link.membersOnly;
        this.stackable = link.stackable;
        this.firstEquippedMaleModelId = link.firstEquippedMaleModelId;
        this.secondEquippedMaleModelId = link.secondEquippedMaleModelId;
        this.thirdEquippedMaleModelId = link.thirdEquippedMaleModelId;
        this.firstEquippedFemaleModelId = link.firstEquippedFemaleModelId;
        this.secondEquippedFemaleModelId = link.secondEquippedFemaleModelId;
        this.thirdEquippedFemaleModelId = link.thirdEquippedFemaleModelId;
        this.firstMaleHeadModelId = link.firstMaleHeadModelId;
        this.secondMaleHeadModelId = link.secondMaleHeadModelId;
        this.firstFemaleHeadModelId = link.firstFemaleHeadModelId;
        this.secondFemaleHeadModelId = link.secondFemaleHeadModelId;
        this.teamId = link.teamId;
        this.groundActions = link.groundActions;
        this.inventoryActions = new String[5];
        if (link.inventoryActions != null) {
            System.arraycopy(link.inventoryActions, 0, this.inventoryActions, 0, 4);
        }
        this.inventoryActions[4] = "Discard";
        this.value = 0;
    }

    private void noteTransform() {
        Extended templateAs = (Extended) ItemDefinition.get(noteTemplateId);
        if (templateAs == null) {
            return;
        }
        Extended linkAs = (Extended) ItemDefinition.get(noteLinkId);
        if (linkAs == null) {
            return;
        }
        CacheItemDefinition template = templateAs.getBase();
        CacheItemDefinition link = linkAs.getBase();
        this.modelId = template.modelId;
        this.zoom2D = template.zoom2D;
        this.xRotation2D = template.xRotation2D;
        this.zRotation2D = template.zRotation2D;
        this.yRotation2D = template.yRotation2D;
        this.xTranslation2D = template.xTranslation2D;
        this.yTranslation2D = template.yTranslation2D;
        this.originalColors = template.originalColors;
        this.replacementColors = template.replacementColors;
        this.originalMaterialIds = template.originalMaterialIds;
        this.replacementMaterialIds = template.replacementMaterialIds;
        this.name = link.name;
        this.membersOnly = link.membersOnly;
        this.value = link.value;
        this.stackable = Stackability.ALWAYS;
    }

    private boolean isCurrentWorldMembersOnlyPlatformSafe() {
        return !BotPlatform.isBotThread() || OpenClient.isMembersWorld();
    }


    public Extended extended() {
        return new Extended();
    }

    private enum Stackability {
        SOMETIMES(0),
        ALWAYS(1),
        NEVER(2);
        private final int id;

        Stackability(int id) {
            this.id = id;
        }

        public int getId() {
            return this.id;
        }
    }

    private enum DummyType {
        LINK(1),
        TEMPLATE(2);
        private final int value;

        DummyType(int value) {
            this.value = value;
        }
    }

    private enum TransformationType {
        NOTE(null),
        LENT("Discard"),
        COSMETIC("Discard"),
        SHARD("Drop");
        private final String dropAction;

        TransformationType(final String dropAction) {
            this.dropAction = dropAction;
        }

        public String getDestroyAction() {
            return dropAction;
        }
    }

    public class Extended extends ItemDefinition {

        /**
         * Don't draw any border
         **/
        public static final int BORDER_NONE = 0;
        /**
         * Draw a 1px black border
         **/
        public static final int BORDER_BLACK = 1;
        /**
         * Draw a compound border: 1px black border, and then a 1px white border
         **/
        public static final int BORDER_WHITE = 2;
        /**
         * Never render the quantity text
         **/
        public static final int TEXT_POLICY_NEVER = 0;
        /**
         * Always render the quantity text, as long as quantity != -1
         **/
        public static final int TEXT_POLICY_PREFERRED = 1;
        /**
         * Render the quantity text if quantity != 1, and the item is stackable
         **/
        public static final int TEXT_POLICY_SELECTIVE = 2;

        private Extended() {
        }

        public int getModelXScale() {
            return xModelScale;
        }

        public int getModelYScale() {
            return yModelScale;
        }

        public int getModelZScale() {
            return zModelScale;
        }

        @Override
        public Attribute getAttribute(long id) {
            if (attributes == null) {
                return null;
            }
            return attributes.get(id);
        }

        @Override
        public List<Attribute> getAttributes() {
            if (attributes == null) {
                return Collections.emptyList();
            }
            return new ArrayList<>(attributes.values());
        }

        @Override
        public Map<Color, Color> getColorSubstitutions() {
            if (originalColors == null) {
                return Collections.emptyMap();
            }
            Map<Color, Color> colorMapping = new HashMap<>(originalColors.length);
            for (int i = 0; i < originalColors.length; ++i) {
                colorMapping.put(
                    RSColors.fromHSV(originalColors[i]),
                    RSColors.fromHSV(replacementColors[i])
                );
            }
            return colorMapping;
        }

        @Override
        public Map<Material, Material> getMaterialSubstitutions() {
            if (originalMaterialIds == null) {
                return Collections.emptyMap();
            }
            Map<Material, Material> materialMapping = new HashMap<>(originalMaterialIds.length);
            for (int i = 0; i < originalMaterialIds.length; ++i) {
                materialMapping.put(
                    Materials.load(originalMaterialIds[i]),
                    Materials.load(replacementMaterialIds[i])
                );
            }
            return materialMapping;
        }

        @Override
        public String[] getRawGroundActions() {
            if (membersOnly && !isCurrentWorldMembersOnlyPlatformSafe()) {
                return new String[0];
            }
            return groundActions;
        }

        @Override
        public int getId() {
            return id;
        }

        @Override
        public String[] getRawInventoryActions() {
            return inventoryActions;
        }

        @Override
        public boolean isNoted() {
            return noteLinkId != -1 && stacks();
        }

        @Override
        public int getUnnotedId() {
            return noteLinkId != -1 && stacks() ? noteLinkId : getId();
        }

        @Override
        public int getNotedId() {
            if (noteLinkId != -1) {
                if (!stacks()) {
                    return noteLinkId;
                }
                return getId();
            }
            return -1;
        }

        @Override
        public int getCosmeticTemplateId() {
            return cosmeticLinkId;
        }

        @Override
        public boolean isCosmetic() {
            return cosmeticTemplateId != -1 && cosmeticLinkId != -1;
        }

        @Override
        public ItemDefinition getCosmeticDefinition() {
            if (!isCosmetic()) {
                return ItemDefinition.get(cosmeticLinkId);
            }
            return null;
        }

        @Override
        public ItemDefinition getCosmeticBaseDefinition() {
            if (isCosmetic()) {
                return ItemDefinition.get(cosmeticLinkId);
            }
            return null;
        }

        @Override
        public int getPlaceholderId() {
            if (isPlaceholder()) {
                return getId();
            }
            return placeholderLinkId;
        }

        @Override
        public ItemDefinition getPlaceholderDefinition() {
            if (!isPlaceholder()) {
                return ItemDefinition.get(placeholderLinkId);
            }
            return null;
        }

        @Override
        public ItemDefinition getPlaceholderBaseDefinition() {
            if (isPlaceholder()) {
                return ItemDefinition.get(placeholderLinkId);
            }
            return null;
        }

        @Override
        public Equipment.Slot getEquipmentSlot() {
            Equipment.Slot slot = getPrimaryEquipmentSlot();
            if (slot == null) {
                slot = getSecondaryEquipmentSlot();
            }
            if (slot == null) {
                slot = getTertiaryEquipmentSlot();
            }
            return slot;
        }

        @Override
        @Nullable
        public Equipment.Slot getPrimaryEquipmentSlot() {
            return Equipment.Slot.resolve(primaryEquipmentSlot);
        }

        @Override
        @Nullable
        public Equipment.Slot getSecondaryEquipmentSlot() {
            return Equipment.Slot.resolve(secondaryEquipmentSlot);
        }

        @Override
        @Nullable
        public Equipment.Slot getTertiaryEquipmentSlot() {
            return Equipment.Slot.resolve(tertiaryEquipmentSlot);
        }

        @Override
        public int getLowLevelAlchemyValue() {
            return (int) (value * .4);
        }

        @Override
        public int getHighLevelAlchemyValue() {
            return (int) (value * .6);
        }

        @Override
        public int getShopValue() {
            return value;
        }


        @Override
        public int getGroundModelId() {
            return modelId;
        }

        @Override
        public double getWeight() {
            return weight / 1000D;
        }

        @Override
        public List<Integer> getMaleModelIds() {
            List<Integer> modelIds = new ArrayList<>(3);
            if (firstEquippedMaleModelId != -1) {
                modelIds.add(firstEquippedMaleModelId);
            }
            if (secondEquippedMaleModelId != -1) {
                modelIds.add(secondEquippedMaleModelId);
            }
            if (thirdEquippedMaleModelId != -1) {
                modelIds.add(thirdEquippedMaleModelId);
            }
            return modelIds;
        }

        @Override
        public List<Integer> getFemaleModelIds() {
            List<Integer> modelIds = new ArrayList<>(3);
            if (firstEquippedFemaleModelId != -1) {
                modelIds.add(firstEquippedFemaleModelId);
            }
            if (secondEquippedFemaleModelId != -1) {
                modelIds.add(secondEquippedFemaleModelId);
            }
            if (thirdEquippedFemaleModelId != -1) {
                modelIds.add(thirdEquippedFemaleModelId);
            }
            return modelIds;
        }

        @Override
        public List<Integer> getMaleHeadModelIds() {
            List<Integer> modelIds = new ArrayList<>(2);
            if (firstMaleHeadModelId != -1) {
                modelIds.add(firstMaleHeadModelId);
            }
            if (secondMaleHeadModelId != -1) {
                modelIds.add(secondMaleHeadModelId);
            }
            return modelIds;
        }

        @Override
        public List<Integer> getFemaleHeadModelIds() {
            List<Integer> modelIds = new ArrayList<>(2);
            if (firstFemaleHeadModelId != -1) {
                modelIds.add(firstFemaleHeadModelId);
            }
            if (secondFemaleHeadModelId != -1) {
                modelIds.add(secondFemaleHeadModelId);
            }
            return modelIds;
        }

        @Override
        public boolean isMembersOnly() {
            return membersOnly;
        }


        /*@Override
        public boolean isLendable() {
            return lentTemplateId != -1 && lentLinkId != -1;
        }*/

        /*@Override
        public boolean isShard() {
            return combineTemplateId != -1 && combineLinkId != -1;
        }*/

        @Override
        public boolean isPlaceholder() {
            return placeholderTemplateId != -1 && placeholderLinkId != -1;
        }

        @Override
        public boolean isTradeable() {
            /*
            Typical Properties:
            1. Can be traded to another player as long as the item is either f2p or on a members world.
            2. When located on the ground, the dropped item(s) will eventually become visible to other players.
            3. May be sold to either a general store or a speciality shop
                * Assumption: must have a value > 0)

            Additional Notes:
            * Coins are tradeable but not tradeable on the market, yet have a value of 1gp each.
            *
            * The special teak and mahogany logs, and bane ore could be converted into notes despite not being tradeable.
            * Some tradeable items cannot be alchemised such as bonds and certain Treasure Hunter rewards
                * Assumption: must have a value == 0 or < 0
            * Jagex made some junk items untradeable due to them being used in scams.
            */
            return (!membersOnly || isCurrentWorldMembersOnlyPlatformSafe()) && stockMarket;
        }

        @Override
        public boolean isTradeableOnMarket() {
            return (!membersOnly || isCurrentWorldMembersOnlyPlatformSafe()) && stockMarket;
        }

        @Override
        public boolean isTwoHanded() {
            List<Equipment.Slot> slots = Arrays.asList(
                getPrimaryEquipmentSlot(),
                getSecondaryEquipmentSlot(),
                getTertiaryEquipmentSlot()
            );
            return slots.containsAll(Arrays.asList(Equipment.Slot.WEAPON, Equipment.Slot.SHIELD));
        }

        @Override
        public boolean stacks() {
            return stackable == Stackability.ALWAYS;
        }

        @Override
        public int getTeamId() {
            return teamId;
        }

        @Override
        public String getShiftClickAction() {
            if (inventoryActions != null) {
                if (shiftClickInventoryActionIndex >= 0) {
                    if (inventoryActions[shiftClickInventoryActionIndex] != null) {
                        return inventoryActions[shiftClickInventoryActionIndex];
                    }
                } else if ("Drop".equalsIgnoreCase(inventoryActions[4])) {
                    return inventoryActions[4];
                }
            }
            return null;
        }

        @NonNull
        @Override
        public String getName() {
            return !isMembersOnly() || isCurrentWorldMembersOnlyPlatformSafe() ? name : "Members object";
        }

        @InternalAPI
        public CacheItemDefinition getBase() {
            return CacheItemDefinition.this;
        }

        public PairList<Integer, ItemDefinition> getSubstitutions() {
            PairList<Integer, ItemDefinition> substitutions = new PairList<>();
            if (stackIds != null && stackQuantities != null) {
                for (int index = 0; index < stackIds.length; ++index) {
                    int quantity = stackQuantities[index];
                    if (quantity != 0) {
                        ItemDefinition substitution = ItemDefinition.get(stackIds[index]);
                        if (substitution != null) {
                            substitutions.add(quantity, substitution);
                        }
                    }
                }
            }
            return substitutions;
        }

        public ItemDefinition getSubstitution(int quantity) {
            ItemDefinition substitution = null;
            PairList<Integer, ItemDefinition> substitutions = getSubstitutions();
            for (Pair<Integer, ItemDefinition> substitute : substitutions) {
                if (quantity >= substitute.getLeft() && substitute.getLeft() != 0) {
                    substitution = substitute.getRight();
                }
            }
            return substitution;
        }

        @Override
        public Multimap<String, String> getSubActions() {
            if (subops == null) {
                return ImmutableMultimap.of();
            }

            Multimap<String, String> subActionsMap = MultimapBuilder.hashKeys().arrayListValues().build();
            for (int i = 0; i < subops.length; i++) {
                final String action = inventoryActions[i];
                if (action == null) {
                    continue;
                }

                final String[] subActions = subops[i];
                if (subActions == null) {
                    continue;
                }

                for (String subAction : subActions) {
                    if (subAction != null) {
                        subActionsMap.put(action, subAction);
                    }
                }
            }
            return subActionsMap;
        }
    }
}
