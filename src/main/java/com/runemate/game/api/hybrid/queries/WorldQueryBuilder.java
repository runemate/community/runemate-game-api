package com.runemate.game.api.hybrid.queries;

import com.runemate.game.api.client.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.osrs.location.*;
import com.runemate.game.api.script.annotations.*;
import java.util.*;
import java.util.concurrent.*;

public class WorldQueryBuilder extends QueryBuilder<WorldOverview, WorldQueryBuilder, WorldQueryResults> {

    private Boolean membersOnly;

    private EnumSet<WorldType> includes, excludes;
    private List<String> activities;
    private Integer minPopulation, maxPopulation;
    private WorldRegion region;

    public WorldQueryBuilder activity(String... activity) {
        return activity(Arrays.asList(activity));
    }

    public WorldQueryBuilder activity(List<String> activity) {
        this.activities = activity;
        return get();
    }

    /**
     * Will include worlds containing any of the provided WorldTypes.
     */
    public WorldQueryBuilder include(WorldType... types) {
        return include(EnumSet.copyOf(Arrays.asList(types)));
    }


    /**
     * Will include worlds containing any of the provided WorldTypes.
     */
    public WorldQueryBuilder include(EnumSet<WorldType> types) {
        this.includes = types;
        return get();
    }

    /**
     * Will exclude worlds containing any of the provided WorldTypes.
     */
    public WorldQueryBuilder exclude(WorldType... types) {
        return exclude(EnumSet.copyOf(Arrays.asList(types)));
    }

    /**
     * Will exclude worlds containing any of the provided WorldTypes.
     */
    public WorldQueryBuilder exclude(EnumSet<WorldType> types) {
        this.excludes = types;
        return get();
    }

    /**
     * Only include P2P worlds
     * <p>
     * default: F2P &#x26; P2P
     */
    public WorldQueryBuilder member() {
        this.membersOnly = true;
        return get();
    }

    /**
     * Only include F2P worlds
     * <p>
     * default: F2P &#x26; P2P
     */
    public WorldQueryBuilder free() {
        this.membersOnly = false;
        return get();
    }

    /**
     * Only include worlds using the accounts permanent primary profile, and excluding any PVP/deadman/seasonal worlds.
     */
    public WorldQueryBuilder regular() {
        return exclude(
            WorldType.PVP,
            WorldType.PVP_ARENA,
            WorldType.BOUNTY,
            WorldType.QUEST_SPEEDRUNNING,
            WorldType.LAST_MAN_STANDING,
            WorldType.NOSAVE_MODE,
            WorldType.TOURNAMENT_WORLD,
            WorldType.FRESH_START_WORLD,
            WorldType.DEADMAN,
            WorldType.SEASONAL,
            WorldType.HIGH_RISK,
            WorldType.SKILL_TOTAL,
            WorldType.BETA,
            WorldType.SUPER_SECRET
        );
    }

    public WorldQueryBuilder region(WorldRegion region) {
        this.region = region;
        return get();
    }

    public WorldQueryBuilder minPopulation(int min) {
        this.minPopulation = min;
        return get();
    }

    public WorldQueryBuilder maxPopulation(int max) {
        this.maxPopulation = max;
        return get();
    }

    @Override
    public WorldQueryBuilder get() {
        return this;
    }

    @Override
    public Callable<List<? extends WorldOverview>> getDefaultProvider() {
        return () -> Worlds.getLoaded().asList();
    }

    @Override
    public boolean accepts(WorldOverview overview) {
        if (activities != null) {
            boolean found = false;
            String thisActivity = overview.getActivity();
            for (String activity : activities) {
                if (activity.equals(thisActivity)) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                return false;
            }
        }
        if (includes != null && !includes.isEmpty()) {
            boolean found = false;
            for (final var type : overview.getWorldTypes()) {
                if (includes.contains(type)) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                return false;
            }
        }
        if (excludes != null && !excludes.isEmpty()) {
            for (final var type : overview.getWorldTypes()) {
                if (excludes.contains(type)) {
                    return false;
                }
            }
        }
        if (membersOnly != null && membersOnly != overview.isMembersOnly()) {
            return false;
        }
        if (minPopulation != null && overview.getPopulation() < minPopulation) {
            return false;
        }
        if (maxPopulation != null && overview.getPopulation() > maxPopulation) {
            return false;
        }
        if (region != null && !Objects.equals(region, overview.getRegion())) {
            return false;
        }
        return super.accepts(overview);
    }

    @Override
    protected WorldQueryResults results(Collection<? extends WorldOverview> entries, ConcurrentMap<String, Object> cache) {
        return new WorldQueryResults(entries, cache);
    }

    private WorldQueryBuilder append(WorldType type, boolean include) {
        final var coll = (include ? includes : excludes);
        if (coll == null) {
            return include ? include(type) : exclude(type);
        }
        coll.add(type);
        return get();
    }
}
