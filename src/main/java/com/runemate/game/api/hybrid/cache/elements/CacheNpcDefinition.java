package com.runemate.game.api.hybrid.cache.elements;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.cache.materials.*;
import com.runemate.game.api.hybrid.entities.attributes.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.entities.status.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.script.annotations.*;
import com.runemate.game.cache.io.*;
import com.runemate.game.cache.item.*;
import com.google.common.primitives.*;
import java.awt.*;
import java.io.*;
import java.util.List;
import java.util.*;
import javax.annotation.*;
import lombok.*;

public class CacheNpcDefinition extends IncrementallyDecodedItem {

    private final String[] actions;
    private final List<Integer> modelIds = new ArrayList<>();
    private final int id;
    private final boolean rs3;
    private int[][] modelOffsets;
    private HashMap<Long, Attribute> attributes;
    private boolean interactable = true;
    //familiars, pets, etc
    private boolean follower;
    private boolean lowPriorityFollowerOps;
    private int idleAnimationId;
    private int level = -1;
    private int movementCapabilitiesFlag;
    private String name = "null";
    private short[] originalColorsInHSV;
    private short[] originaTextureIds;
    private int overheadGaugeWidth = 30;
    private boolean prioritizeRendering = false;
    private short[] replacementColorsInHSV;
    private short[] replacementTextureIds;
    private boolean showOnMinimap = true;
    private int areaEdgeLength = 1;
    private int[] transformationIds;
    private int varbitIndex = -1;
    private int varpIndex = -1;
    private int walkingAnimationId;
    private int runningAnimationId;
    private int rotatingRightAnimationId;
    private int rotatingLeftAnimationId;
    private int[] chatHeadModelIds;
    private int scaleXZ;
    private int scaleY;
    private int mapIcon;
    private byte respawnDirection;
    private int attackActionCursorId;
    private int[] questIds;
    private boolean halfStep = true;
    private int category;
    private int height;
    private int[] stats = { 1, 1, 1, 1, 1, 1 };


    int[] headIconArchiveIds;
    short[] headIconSpriteIndex;

    private int field1920;
    private int field1946;
    private int field1922;
    private int field1923;
    private int field1924;
    private int field1904;
    private int field1926;
    private int field1910;

    public CacheNpcDefinition(final int id) {
        this.id = id;
        this.rs3 = false;
        actions = new String[5];
        scaleXZ = 128;
        scaleY = 128;
    }

    @Override
    protected void decode(Js5InputStream stream, int opcode) throws IOException {
        if (opcode == 1) {
            int length = stream.readUnsignedByte();
            for (int index = 0; index < length; ++index) {
                modelIds.add(rs3 ? stream.readDefaultableUnsignedSmart() : stream.readUnsignedShort());
            }
        } else if (opcode == 2) {
            name = stream.readCStyleLatin1String();
        } else if (opcode == 12) {
            areaEdgeLength = stream.readUnsignedByte();
        } else if (opcode == 13) {
            idleAnimationId = stream.readUnsignedShort();
        } else if (opcode == 14) {
            walkingAnimationId = stream.readUnsignedShort();
        } else if (opcode == 15 || opcode == 16) {
            stream.readUnsignedShort();
        } else if (opcode == 17) {
            walkingAnimationId = stream.readUnsignedShort();
            runningAnimationId = stream.readUnsignedShort();
            rotatingRightAnimationId = stream.readUnsignedShort();
            rotatingLeftAnimationId = stream.readUnsignedShort();
        } else if (opcode == 18) {
            category = stream.readUnsignedShort();
        } else if (opcode >= 30 && opcode < 35) {
            actions[opcode - 30] = stream.readCStyleLatin1String();
            if (!rs3 && "Hidden".equalsIgnoreCase(actions[opcode - 30])) {
                actions[opcode - 30] = null;
            }
        } else if (opcode == 40) {
            int length = stream.readUnsignedByte();
            originalColorsInHSV = new short[length];
            replacementColorsInHSV = new short[length];
            for (int index = 0; index < length; index++) {
                originalColorsInHSV[index] = (short) stream.readUnsignedShort();
                replacementColorsInHSV[index] = (short) stream.readUnsignedShort();
            }
        } else if (opcode == 41) {
            int length = stream.readUnsignedByte();
            originaTextureIds = new short[length];
            replacementTextureIds = new short[length];
            for (int index = 0; index < length; index++) {
                originaTextureIds[index] = (short) stream.readUnsignedShort();
                replacementTextureIds[index] = (short) stream.readUnsignedShort();
            }
        } else if (opcode == 42) {
            int length = stream.readUnsignedByte();
            for (int index = 0; index < length; ++index) {
                stream.readByte();
            }
        } else if (opcode == 44 || opcode == 45) {
            stream.readUnsignedShort();
        } else if (opcode == 60) {
            int length = stream.readUnsignedByte();
            chatHeadModelIds = new int[length];
            for (int index = 0; index < length; ++index) {
                if (rs3) {
                    chatHeadModelIds[index] =
                        stream.readDefaultableUnsignedSmart();//possible error?
                } else {
                    chatHeadModelIds[index] = stream.readUnsignedShort();
                }
            }
        } else if (opcode == 74) {
            //attack
            stats[0] = stream.readUnsignedShort();
        } else if (opcode == 75) {
            //defence
            stats[1] = stream.readUnsignedShort();
        } else if (opcode == 76) {
            //strength
            stats[2] = stream.readUnsignedShort();
        } else if (opcode == 77) {
            //hitpoints
            stats[3] = stream.readUnsignedShort();
        } else if (opcode == 78) {
            //ranged
            stats[4] = stream.readUnsignedShort();
        } else if (opcode == 79) {
            //magic
            stats[5] = stream.readUnsignedShort();
        } else if (opcode == 93) {
            showOnMinimap = false;
        } else if (opcode == 95) {
            level = stream.readUnsignedShort();
        } else if (opcode == 97) {
            scaleXZ = stream.readUnsignedShort();
        } else if (opcode == 98) {
            scaleY = stream.readUnsignedShort();
        } else if (opcode == 99) {
            prioritizeRendering = true;
        } else if (opcode == 100) {
            int lightnessDelta = stream.readByte();
        } else if (opcode == 101) {
            int intensityDelta = stream.readByte();
        } else if (opcode == 102) {
            int length = stream.readUnsignedByte();
            int iconCount = 0;
            for (int index = length; index != 0; index >>= 1) {
                ++iconCount;
            }
            headIconArchiveIds = new int[iconCount];
            headIconSpriteIndex = new short[iconCount];
            for (int index = 0; index < iconCount; ++index) {
                if ((length & 1 << index) == 0) {
                    headIconArchiveIds[index] = -1;
                    headIconSpriteIndex[index] = -1;
                } else {
                    headIconArchiveIds[index] = stream.readDefaultableUnsignedSmart();
                    headIconSpriteIndex[index] = (short) stream.readReducedShortSmart();
                }
            }
        } else if (opcode == 103) {
            stream.readUnsignedShort();
        } else if (opcode == 106 || opcode == 118) {
            varbitIndex = stream.readUnsignedShort();
            if (varbitIndex == 0xffff) {
                varbitIndex = -1;
            }
            varpIndex = stream.readUnsignedShort();
            if (varpIndex == 0xffff) {
                varpIndex = -1;
            }
            int finalChild = -1;
            if (opcode == 118) {
                finalChild = stream.readUnsignedShort();
                if (finalChild == 0xFFFF) {
                    finalChild = -1;
                }
            }
            int length = stream.readUnsignedByte();
            transformationIds = new int[length + 2];
            for (int index = 0; index <= length; ++index) {
                transformationIds[index] = stream.readUnsignedShort();
                if (transformationIds[index] == 0xFFFF) {
                    transformationIds[index] = -1;
                }
            }
            transformationIds[length + 1] = finalChild;
        } else if (opcode == 107) {
            interactable = false;
        } else if (opcode == 109) {
            halfStep = false;
        } else if (opcode == 111) {
            this.follower = true;
            this.lowPriorityFollowerOps = true;
        } else if (opcode == 112) {
            overheadGaugeWidth = stream.readUnsignedByte();
        } else if (opcode == 113) {
            stream.readUnsignedShort();
            stream.readUnsignedShort();
        } else if (opcode == 114) {
            stream.readByte();
            stream.readByte();
            //Probably marker/menu action related
        } else if (opcode == 115) {
            this.field1920 = stream.readUnsignedShort();
            this.field1946 = stream.readUnsignedShort();
            this.field1922 = stream.readUnsignedShort();
            this.field1923 = stream.readUnsignedShort();
        } else if (opcode == 116) {
            this.field1924 = stream.readUnsignedShort();
        } else if (opcode == 117) {
            this.field1924 = stream.readUnsignedShort();
            this.field1904 = stream.readUnsignedShort();
            this.field1926 = stream.readUnsignedShort();
            this.field1910 = stream.readUnsignedShort();
        } else if (opcode == 119) {
            movementCapabilitiesFlag = stream.readByte();
        } else if (opcode == 121) {
            modelOffsets = new int[modelIds.size()][];
            int length = stream.readUnsignedByte();
            for (int i = 0; i < length; ++i) {
                int index = stream.readUnsignedByte();
                int[] trans = modelOffsets[index] = new int[3];
                trans[0] = stream.readByte();
                trans[1] = stream.readByte();
                trans[2] = stream.readByte();
            }
        } else if (opcode == 122) {
            follower = true;
        } else if (opcode == 123) {
            lowPriorityFollowerOps = true;
        } else if (opcode == 124) {
            //height?
            stream.readUnsignedShort();
        } else if (opcode == 125) {
            respawnDirection = stream.readByte();
        } else if (opcode == 127) {
            stream.readUnsignedShort();
        } else if (opcode == 128) {
            stream.readUnsignedByte();
        } else if (opcode == 134) {
            stream.readUnsignedShort();
            stream.readUnsignedShort();
            stream.readUnsignedShort();
            stream.readUnsignedShort();
            stream.readUnsignedByte();
        } else if (opcode == 135 || opcode == 136) {
            stream.readUnsignedByte();
            stream.readUnsignedShort();
        } else if (opcode == 137) {
            attackActionCursorId = stream.readUnsignedShort();
        } else if (opcode == 138) {
            stream.readDefaultableUnsignedSmart();
        } else if (opcode == 140) {
            stream.readUnsignedByte();
        } else if (opcode == 141) {
            follower = true;
        } else if (opcode == 142) {
            mapIcon = stream.readUnsignedShort();
        } else if (opcode == 143) {
        } else if (opcode >= 150 && opcode < 155) {
            this.actions[opcode - 150] = stream.readCStyleLatin1String();
        } else if (opcode == 155) {
            stream.readByte();
            stream.readByte();
            stream.readByte();
            stream.readByte();
        } else if (opcode == 158 || opcode == 159) {
        } else if (opcode == 160) {
            int length = stream.readUnsignedByte();
            questIds = new int[length];
            for (int index = 0; index < length; ++index) {
                questIds[index] = stream.readUnsignedShort();
            }
        } else if (opcode == 162) {
        } else if (opcode == 163) {
            stream.readUnsignedByte();
        } else if (opcode == 164) {
            stream.readUnsignedShort();
            stream.readUnsignedShort();
        } else if (opcode == 165 || opcode == 168) {
            stream.readUnsignedByte();
        } else if (opcode == 169) {
        } else if (opcode >= 170 && opcode < 176) {
            stream.readUnsignedShort();
        } else if (opcode == 178) {
        } else if (opcode == 179) {
            stream.readQuarterBoundSmart();
            stream.readQuarterBoundSmart();
            stream.readQuarterBoundSmart();
            stream.readQuarterBoundSmart();
            stream.readQuarterBoundSmart();
            stream.readQuarterBoundSmart();
        } else if (opcode == 180) {
            stream.readUnsignedByte();
        } else if (opcode == 181) {
            stream.readUnsignedShort();
            stream.readUnsignedByte();
        } else if (opcode == 182) {
        } else if (opcode == 249) {
            int var4 = stream.readUnsignedByte();
            attributes = new HashMap<>();
            for (int index = 0; index < var4; ++index) {
                boolean isString = stream.readUnsignedByte() == 1;
                int key = stream.readBytes(3);
                attributes.put((long) key, new Attribute(
                    key,
                    isString ? stream.readCStyleLatin1String() : stream.readInt()
                ));
            }
        } else {
            throw new IllegalStateException("Opcode '" + opcode + "' is unsupported!");
        }
    }

    @Override
    public void decode(Js5InputStream stream) throws IOException {
        super.decode(stream);
    }

    public CacheNpcDefinition.Extended toNpcDefinition() {
        return new Extended();
    }

    public class Extended extends NpcDefinition {

        private Extended() {
        }

        public int[][] getXYZModelTranslations() {
            return modelOffsets;
        }

        public int getModelXZScale() {
            return scaleXZ;
        }

        public int getModelYScale() {
            return scaleY;
        }

        @NonNull
        @Override
        public String getName() {
            return JagTags.remove(name);
        }

        @Override
        public int getOverheadGaugeWidth() {
            return overheadGaugeWidth;
        }

        @Override
        public boolean appearsOnMinimap() {
            return showOnMinimap;
        }

        @NonNull
        @Override
        public List<String> getActions() {
            if (actions.length == 0) {
                return Collections.emptyList();
            }
            List<String> actionList = new ArrayList<>(actions.length);
            for (String action : actions) {
                if (action != null && !"null".equals(action)) {
                    actionList.add(action);
                }
            }
            return actionList;
        }

        @Nullable
        @Override
        public String[] getRawActions() {
            return actions;
        }

        @NonNull
        @Override
        public List<Integer> getAppearance() {
            return modelIds;
        }

        @NonNull
        @Override
        public List<Integer> getChatHeadAppearance() {
            return chatHeadModelIds != null ? Ints.asList(chatHeadModelIds) :
                Collections.emptyList();
        }

        @Override
        @Nullable
        public Attribute getAttribute(long id) {
            if (attributes == null) {
                return null;
            }
            return attributes.get(id);
        }

        @NonNull
        @Override
        public List<Attribute> getAttributes() {
            if (attributes == null) {
                return Collections.emptyList();
            }
            return new ArrayList<>(attributes.values());
        }

        @NonNull
        @Override
        public Map<Color, Color> getColorSubstitutions() {
            if (originalColorsInHSV == null) {
                return Collections.emptyMap();
            }
            Map<Color, Color> colorMapping = new HashMap<>(originalColorsInHSV.length);
            for (int i = 0; i < originalColorsInHSV.length; ++i) {
                colorMapping.put(
                    RSColors.fromHSV(originalColorsInHSV[i]),
                    RSColors.fromHSV(replacementColorsInHSV[i])
                );
            }
            return colorMapping;
        }

        @Override
        public int getId() {
            return id;
        }

        @Override
        public int getLevel() {
            return level;
        }

        @Override
        @Nullable
        public NpcDefinition getLocalState() {
            if (transformationIds != null) {
                int index = -1;
                if (varbitIndex != -1) {
                    Varbit vb = Varbits.load(varbitIndex);
                    if (vb != null) {
                        index = vb.getValue();
                    }
                } else if (varpIndex != -1) {
                    index = Varps.getAt(varpIndex).getValue();
                }
                int transformationId;
                if (index >= 0 && index < transformationIds.length - 1) {
                    transformationId = transformationIds[index];
                } else {
                    transformationId = transformationIds[transformationIds.length - 1];
                }
                return transformationId != -1 ? get(transformationId) : null;
            }
            return null;
        }

        @NonNull
        @Override
        public Map<Material, Material> getMaterialSubstitutions() {
            if (originaTextureIds == null) {
                return Collections.emptyMap();
            }
            Map<Material, Material> materialMapping = new HashMap<>(originaTextureIds.length);
            for (int i = 0; i < originaTextureIds.length; ++i) {
                materialMapping.put(
                    Materials.load(originaTextureIds[i]),
                    Materials.load(replacementTextureIds[i])
                );
            }
            return materialMapping;
        }

        @Override
        public int getMovementCapabilitiesFlag() {
            return movementCapabilitiesFlag;
        }

        @NonNull
        @Override
        public List<OverheadIcon> getOverheadIcons() {
            if (headIconArchiveIds == null || headIconSpriteIndex == null) {
                return Collections.emptyList();
            }
            return Collections.singletonList(new OverheadIcon(headIconSpriteIndex[0], OverheadIcon.Type.PRAYER));
        }

        @NonNull
        @Override
        public Collection<NpcDefinition> getTransformations() {
            if (transformationIds != null && transformationIds.length > 0) {
                Set<Integer> ids = new HashSet<>();
                Collection<NpcDefinition> transformations = new ArrayList<>(transformationIds.length);
                for (int childId : transformationIds) {
                    if (ids.contains(childId)) {
                        continue;
                    }
                    ids.add(childId);
                    if (childId != -1) {
                        NpcDefinition transformation = get(childId);
                        if (transformation != null) {
                            transformations.add(transformation);
                        }
                    }
                }
                return transformations;
            }
            return Collections.emptyList();
        }

        public int[] getTransformationIds() {
            return transformationIds;
        }

        @Override
        public boolean hasPrioritizedRendering() {
            return prioritizeRendering;
        }

        @Override
        public boolean isClickable() {
            return interactable;
        }

        @Override
        public boolean isFollower() {
            return follower;
        }

        //@Override
        //public boolean isHalfStepping() {
        //    return halfStep;
        //}

        @Override
        public int getAreaEdgeLength() {
            return areaEdgeLength;
        }

        @Override
        public Varp getStateVarp() {
            return varpIndex != -1 ? Varps.getAt(varpIndex) : null;
        }

        @Override
        public Varbit getStateVarbit() {
            return varbitIndex != -1 ? Varbits.load(varbitIndex) : null;
        }

        @Override
        public Map<Skill, Integer> getCombatLevels() {
            return Map.of(
                Skill.ATTACK, stats[0],
                Skill.DEFENCE, stats[1],
                Skill.STRENGTH, stats[2],
                Skill.CONSTITUTION, stats[3],
                Skill.RANGED, stats[4],
                Skill.MAGIC, stats[5]
            );
        }
    }
}
