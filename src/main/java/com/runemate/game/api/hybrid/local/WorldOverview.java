package com.runemate.game.api.hybrid.local;

import com.runemate.game.api.osrs.location.*;
import java.util.*;

public interface WorldOverview {

    /**
     * Gets the world id (number)
     */
    int getId();

    EnumSet<WorldType> getWorldTypes();

    String getActivity();

    boolean isMembersOnly();

    boolean isSkillTotal500();

    boolean isSkillTotal750();

    boolean isSkillTotal1250();

    boolean isSkillTotal1500();

    boolean isSkillTotal1750();

    boolean isSkillTotal2000();

    boolean isSkillTotal2200();

    int getPopulation();

    WorldRegion getRegion();

    WorldRegionQualifier getRegionQualifier();
}
