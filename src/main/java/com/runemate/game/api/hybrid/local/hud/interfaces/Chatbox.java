package com.runemate.game.api.hybrid.local.hud.interfaces;

import com.runemate.client.framework.open.*;
import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.api.script.*;
import com.runemate.game.api.script.data.*;
import java.awt.*;
import java.util.List;
import java.util.*;
import java.util.regex.*;
import java.util.stream.*;
import javax.annotation.*;
import lombok.*;
import lombok.extern.log4j.*;
import org.apache.logging.log4j.*;
import org.json.*;

@Log4j2
public final class Chatbox {

    private final static int[] OSRS_CONTAINERS = { 162 };

    private Chatbox() {
    }

    /**
     * Gets the viewport that holds the message components
     *
     * @return the message viewport
     */
    @Nullable
    public static InterfaceComponent getMessageViewport() {
        return OSRSChat.getMessageViewport();
    }

    /**
     * @param types the different types to include in the list
     * @return a List of all messages of the defined types
     */
    public static List<Message> getMessages(Message.Type... types) {
        return OSRSChat.getMessages(types);
    }

    /**
     * @return a List of all Messages
     */
    public static List<Message> getMessages() {
        return OSRSChat.getMessages();
    }

    public static ChatboxQueryBuilder newQuery() {
        return new ChatboxQueryBuilder();
    }

    public enum Filter {
        GAME("Game"),
        PUBLIC("Public"),
        PRIVATE("Private"),
        FRIENDS_CHAT(null) {
            @Override
            public State getState() {
                return State.ON;
            }
        },
        CLAN("Clan"),
        GUEST_CLAN(null) {
            @Override
            public State getState() {
                return State.UNAVAILABLE;
            }
        },
        GROUP(null) {
            @Override
            public State getState() {
                return State.UNAVAILABLE;
            }
        },
        TRADE("Trade"),
        ASSIST(null) {
            @Override
            public State getState() {
                return State.UNAVAILABLE;
            }
        },
        PROFANITY(null) {
            @Override
            public State getState() {
                return Varps.getAt(1074).getValue() == 0 ? State.ON : State.OFF;
            }

            @Override
            public boolean changeState(State desiredState) {
                State startState = getState();
                if (startState == desiredState) {
                    return true;
                }
                if (ControlPanelTab.SETTINGS.open()) {
                    //If the options tab is opened, or is opened successfully
                    InterfaceComponent chat =
                        Interfaces.newQuery().containers(261).types(InterfaceComponent.Type.SPRITE)
                            .actions("Chat").results().first();
                    if (chat == null) {
                        return false;
                    } else if (chat.getSpriteId() == 762) {
                        //Chat subtab is open, click on the profanity filter button
                        InterfaceComponent profanityFilter = Interfaces.newQuery().containers(261)
                            .types(InterfaceComponent.Type.SPRITE).grandchildren(false)
                            .actions("Toggle Profanity Filter").results().first();
                        return profanityFilter != null &&
                            profanityFilter.interact("Toggle Profanity Filter") &&
                            Execution.delayUntil(() -> startState != getState(), 600, 2400);
                    } else {
                        if (chat.click()) {
                            Execution.delayUntil(() -> chat.getSpriteId() == 762);
                        }
                    }
                }
                return false;
            }
        },
        BROADCAST(null);

        private final String osrsName;

        Filter(String osrsName) {
            this.osrsName = osrsName;
        }

        public static List<Filter> getAvailableFilters() {
            return Arrays.stream(values()).filter(v -> v.getState() != State.UNAVAILABLE)
                .collect(Collectors.toList());
        }

        public static List<Filter> getFiltersWithStates(State... states) {
            Arrays.sort(states);
            return Arrays.stream(values())
                .filter(v -> Arrays.binarySearch(states, v.getState()) >= 0)
                .collect(Collectors.toList());
        }

        public boolean changeState(State desiredState) {
            InterfaceComponent component = getInteractionComponent();
            State initialState = getState();
            if (initialState == desiredState) {
                return true;
            } else if (component != null) {
                //click the selected thing
                String action = getName() + ": ";
                if (this == GAME) {
                    action += "Filter";
                } else {
                    switch (desiredState) {
                        case AUTOCHAT:
                            action += "Show autochat";
                            break;
                        case ALL:
                        case ON:
                            switch (this) {
                                case PUBLIC:
                                    action += "Show standard";
                                    break;
                                case FRIENDS_CHAT:
                                case GUEST_CLAN:
                                case GROUP:
                                case ASSIST:
                                case PROFANITY:
                                case BROADCAST:
                                    return false;
                                default:
                                    action += "Show all";
                            }
                            break;
                        case OFF:
                            action += "Off";
                            break;
                        case FRIENDS:
                            action += "Show friends";
                            break;
                        case UNAVAILABLE:
                            return false;
                        case TEAM:
                            //TODO Slashnhax you never implemented this branch
                    }
                }
                if (component.interact(action)) {
                    Execution.delayUntil(() -> getState() == desiredState, 600, 2400);
                }
            }
            return getState() == desiredState;
        }

        @Nullable
        public InterfaceComponent getInteractionComponent() {
            InterfaceComponent named = getNamedComponent();
            if (named != null) {
                return Interfaces.getAt(named.getId() >> 16, named.getIndex() - 1);
            }

            return null;
        }

        @Nullable
        public String getName() {
            return osrsName;
        }

        @Nullable
        public InterfaceComponent getNamedComponent() {
            if (osrsName == null) {
                return null;
            }
            return Interfaces.newQuery()
                .containers(OSRS_CONTAINERS)
                .texts(getName())
                .types(InterfaceComponent.Type.LABEL)
                .visible()
                .grandchildren(false)
                .results()
                .first();
        }

        public State getState() {
            InterfaceComponent component = getNamedComponent();
            if (component == null) {
                return State.UNAVAILABLE;
            }
            InterfaceComponent stateComp = Interfaces.getAt(component.getId() >> 16, component.getIndex() + 1);
            if (stateComp != null) {
                String stateText = stateComp.getText();
                if (stateText != null) {
                    switch (stateText) {
                        case "All":
                            return State.ALL;
                        case "On":
                            return State.ON;
                        case "Off":
                            return State.OFF;
                        case "Friends":
                            return State.FRIENDS;
                    }
                }
            }
            return State.UNAVAILABLE;
        }

        @Override
        public String toString() {
            return name() + "{name=" + getName() + ", state=" + getState() + "}";
        }

        public enum State {
            ALL,
            AUTOCHAT,
            ON,
            OFF,
            FRIENDS,
            TEAM,
            UNAVAILABLE
        }
    }

    public static class Message implements Comparable<Message>, Interactable {

        private final long cycle;
        private final String message;
        private final String sender;
        @Getter
        private final Type type;
        @Getter
        private final int opcode;

        public Message(final int type, final String author, final String message, final long cycle) {
            this.opcode = type;
            this.sender = author;
            this.message = message;
            this.cycle = cycle;
            this.type = Type.resolve(this);
        }

        public Message(final OpenChatboxMessage rmi) {
            this(rmi.getOpcode(), rmi.getSender(), rmi.getMessage(), rmi.getCycle());
        }

        @Override
        public int compareTo(@NonNull Message m) {
            return Long.compare(cycle, m.cycle);
        }

        @Nullable
        public InterfaceComponent getComponent() {
            boolean rs3 = false;
            InterfaceComponent parent = getMessageViewport();
            if (parent == null) {
                return null;
            }
            List<InterfaceComponent> candidates = new ArrayList<>(1);
            List<InterfaceComponent> children = parent.getChildren();
            for (int i = 0; i < children.size(); i++) {
                InterfaceComponent component = children.get(i);
                String name = component.getText();
                if (name != null) {
                    String speaker = getSpeaker();
                    if (speaker == null || name.contains(speaker)) {
                        if (rs3) {
                            if (name.contains(getMessage())) {
                                candidates.add(component);
                            }
                        } else if (i < 999) {
                            InterfaceComponent next = children.get(i + 1);
                            String message = next.getText();
                            if (message != null && message.equals(getMessage())) {
                                candidates.add(next);
                                i++;    //To prevent searching a matched component again, as it's already identified and added
                            }
                        }
                    }
                }
            }
            if (candidates.size() == 1) {
                return candidates.get(0);
            } else {
                //index is reversed, newest messages are in a lower index in components
                List<Message> messages = getMessages().stream().filter(
                    m -> Objects.equals(this.sender, m.sender) &&
                        Objects.equals(this.message, m.message)).collect(Collectors.toList());
                Collections.reverse(messages);
                int index = messages.indexOf(this);
                if (index != -1 && index < candidates.size()) {
                    return candidates.get(index);
                }
            }
            return null;
        }

        public String getMessage() {
            return JagTags.remove(message);
        }

        @Deprecated
        public String getSender() {
            return getSpeaker();
        }

        public String getSpeaker() {
            return JagTags.remove(sender);
        }

        public boolean isPlayerModerator() {
            return sender != null && sender.startsWith("<img=0>");
        }

        public boolean isJagexModerator() {
            return sender != null && sender.startsWith("<img=1>");
        }

        public JSONObject toJsonObject() {
            final JSONObject res = new JSONObject();
            try {
                res.put("type", type.name());
                res.put("sender", sender);
                res.put("message", message);
            } catch (JSONException e) {
                log.warn("Failed to marshal {}", this, e);
                return null;
            }
            return res;
        }

        @Override
        public int hashCode() {
            return Objects.hash(opcode, cycle, sender, message);
        }

        @Override
        public boolean equals(final Object object) {
            if (object instanceof Message) {
                final Message message = (Message) object;
                return message.opcode == this.opcode
                    && message.cycle == this.cycle
                    && (Objects.equals(message.sender, this.sender))
                    && (Objects.equals(message.message, this.message));
            }
            return false;
        }

        @Override
        public String toString() {
            String display = "";
            String sender = getSpeaker();
            if (sender != null) {
                display += '[' + sender + "] ";
            }
            display += '"' + getMessage() + '"';
            if (type != Chatbox.Message.Type.UNKNOWN) {
                display += " [" + type + ']';
            }
            return display;
        }

        @Override
        public boolean isVisible() {
            InterfaceComponent component = getComponent();
            return component != null && component.isVisible();
        }

        @Override
        public double getVisibility() {
            InterfaceComponent component = getComponent();
            return component != null ? component.getVisibility() : 0;
        }

        @Override
        public boolean hasDynamicBounds() {
            InterfaceComponent component = getComponent();
            return component != null && component.hasDynamicBounds();
        }

        @Nullable
        @Override
        public InteractablePoint getInteractionPoint() {
            InterfaceComponent component = getComponent();
            return component != null ? component.getInteractionPoint() : null;
        }

        @Nullable
        @Override
        public InteractablePoint getInteractionPoint(Point origin) {
            InterfaceComponent component = getComponent();
            return component != null ? component.getInteractionPoint(origin) : null;
        }

        @Override
        public boolean contains(Point point) {
            InterfaceComponent component = getComponent();
            return component != null && component.contains(point);
        }

        @Override
        public boolean click() {
            InterfaceComponent component = getComponent();
            return component != null && component.click();
        }

        @Override
        public boolean hover() {
            InterfaceComponent component = getComponent();
            return component != null && component.hover();
        }

        @Override
        public boolean isHovered() {
            InterfaceComponent component = getComponent();
            return component != null && component.isHovered();
        }

        @Override
        public boolean interact(@Nullable Pattern action, @Nullable Pattern target) {
            InterfaceComponent component = getComponent();
            return component != null && component.interact(action, target);
        }

        @Override
        public boolean interact(@Nullable String action) {
            InterfaceComponent component = getComponent();
            return component != null && component.interact(action);
        }

        @Override
        public boolean interact(@Nullable Pattern action) {
            InterfaceComponent component = getComponent();
            return component != null && component.interact(action);
        }

        @Override
        public boolean interact(@Nullable String action, @Nullable Pattern target) {
            InterfaceComponent component = getComponent();
            return component != null && component.interact(action, target);
        }

        @Override
        public boolean interact(@Nullable Pattern action, @Nullable String target) {
            InterfaceComponent component = getComponent();
            return component != null && component.interact(action, target);
        }

        @Override
        public boolean interact(@Nullable String action, @Nullable String target) {
            InterfaceComponent component = getComponent();
            return component != null && component.interact(action, target);
        }

        public enum Type {
            AUTOTYPER(90, 91),
            EXAMINE_GAME_OBJECT(29),
            EXAMINE_NPC(28),
            EXAMINE_ITEM(27),
            FRIENDS_CHAT(9),
            CLAN_CHAT(41),
            CLAN_GUEST_CHAT(44),
            SENT_ABUSE_REPORT(26),
            SERVER(0, 4, 5, 11, 14, 17, 30, 31, 42, 43, 46,
                102, 105, 108, 109, 111, 115, 116, 117, 119, 121, 122, 123,
                124, 125, 127, 128, 129, 130, 131, 134, 135, 136, 137, 138, 139
            ),
            PUBLIC_CHAT(1, 2),
            PRIVATE_CHAT(3, 6, 7),
            QUICK_CHAT(18, 19, 20, 42, 45),
            TRADE(100, 101, 103, 120),
            DUEL(132),
            ASSISTANCE(104),
            TIP_RECEIVED(106),
            TEN_SECOND_TIMEOUT(107),
            UNKNOWN();

            private static final Logger log = LogManager.getLogger("Chatbox.Type");

            static {
                HashSet<Long> unique_values = new HashSet<>();
                for (Type type : values()) {
                    for (long opcode : type.opcodes) {
                        if (!unique_values.add(opcode)) {
                            log.warn("Duplicate chat opcode {} identified", opcode);
                        }
                    }
                }
            }

            long[] opcodes;

            Type(final long... opcodes) {
                this.opcodes = opcodes;
            }

            public static Type resolve(@NonNull final Message message) {
                return resolve(message.opcode, message.message, message.sender);
            }

            public static Type resolve(int messageOpcode, String message, String sender) {
                for (final Type type : values()) {
                    for (long opcode : type.opcodes) {
                        if (opcode == messageOpcode) {
                            return type;
                        }
                    }
                }
                if (messageOpcode != -1) {
                    ClientAlarms.onClientError(
                        "Unable to resolve chat opcode \"" + messageOpcode + "\"",
                        "[" + sender + "] \"" + message + "\""
                    );
                }
                UNKNOWN.opcodes = Arrays.copyOf(UNKNOWN.opcodes, UNKNOWN.opcodes.length + 1);
                UNKNOWN.opcodes[UNKNOWN.opcodes.length - 1] = messageOpcode;
                return UNKNOWN;
            }

            @Deprecated
            public static Type resolve(int messageOpcode, String message, String sender, GameType game) {
                return resolve(messageOpcode, message, sender);
            }

            @NonNull
            @Override
            public String toString() {
                String name = name();
                return name.charAt(0) + name.substring(1).toLowerCase().replace('_', ' ');
            }
        }
    }
}
