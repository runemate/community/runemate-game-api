package com.runemate.game.api.hybrid.entities.definitions;

import com.google.common.cache.*;
import com.runemate.game.api.client.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.cache.*;
import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.api.hybrid.cache.loaders.*;
import com.runemate.game.api.hybrid.cache.materials.*;
import com.runemate.game.api.hybrid.entities.attributes.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.cache.*;
import com.runemate.game.internal.exception.*;
import com.google.common.collect.*;
import java.awt.*;
import java.util.List;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.stream.*;
import lombok.*;
import javax.annotation.Nullable;

/**
 * The definition of an item
 */
public abstract class ItemDefinition implements Onymous, Identifiable {

    private static final ItemDefinitionLoader osrsDefLoader = new ItemDefinitionLoader(CacheIndex.CONFIGS.getId());
    private static final Cache<Integer, ItemDefinition> osrs_cache =
        CacheBuilder.newBuilder().expireAfterAccess(1, TimeUnit.MINUTES).build();

    /**
     * A list of definitions in the range [first, last]
     */
    public static List<ItemDefinition> get(final int first, final int last) {
        return get(first, last, null);
    }

    /**
     * A list of definitions in the range [first, last] that are accepted by the filter.
     */
    public static List<ItemDefinition> get(final int first, final int last, final Predicate<ItemDefinition> predicate) {
        final List<ItemDefinition> definitions = new ArrayList<>();
        for (int id = first; id <= last; ++id) {
            final ItemDefinition definition = get(id);
            if (definition != null && (predicate == null || predicate.test(definition))) {
                definitions.add(definition);
            }
        }
        return definitions;
    }

    /**
     * Gets the ItemDefinition of the item with the specified id and stores it in a local cache
     *
     * @return The definition if available, otherwise null.
     */
    @Nullable
    public static ItemDefinition get(final int id) {
        return get(true, id);
    }

    /**
     * Gets the ItemDefinition of the item with the specified id and optionally stores it in a local cache.
     *
     * @return The definition if available, otherwise null.
     */
    @Nullable
    public static ItemDefinition get(final int id, final boolean cache) {
        return get(cache, id);
    }

    private static ItemDefinition get(boolean cache, int id) {
        if (id >= 0) {
            try {
                ItemDefinition def = osrs_cache.getIfPresent(id);
                if (def != null) {
                    return def;
                }
                CacheItemDefinition bdef = osrsDefLoader.load(ConfigType.ITEM, id);
                if (bdef != null) {
                    def = bdef.extended();
                    if (cache) {
                        (osrs_cache).put(id, def);
                    }
                    return def;
                }
            } catch (final Exception e) {
                throw new UnableToParseBufferException(
                    "Unable to load item definition for \"" + id + ": " + e.getMessage() + '"', e);
            }
        }
        return null;
    }

    /**
     * Loads all definitions
     */
    public static List<ItemDefinition> loadAll() {
        return loadAll(null);
    }

    /**
     * Loads all definitions that are accepted by the filter
     */
    public static List<ItemDefinition> loadAll(final Predicate<ItemDefinition> filter) {
        int quantity =
            osrsDefLoader.getFiles(JS5CacheController.getLargestJS5CacheController(), 10).length;
        ArrayList<ItemDefinition> definitions = new ArrayList<>(quantity);
        for (int id = 0; id <= quantity; ++id) {
            final ItemDefinition definition = get(false, id);
            if (definition != null && (filter == null || filter.test(definition))) {
                definitions.add(definition);
            }
        }
        definitions.trimToSize();
        return definitions;
    }

    public static Predicate<ItemDefinition> getIdPredicate(final int... acceptedIds) {
        return definition -> {
            final int id = definition.getId();
            for (final int accepted : acceptedIds) {
                if (accepted == id) {
                    return true;
                }
            }
            return false;
        };
    }

    public static Predicate<ItemDefinition> getNamePredicate(final String... acceptedNames) {
        return definition -> {
            final String name = definition.getName();
            for (final String accepted : acceptedNames) {
                if (accepted.equals(name)) {
                    return true;
                }
            }
            return false;
        };
    }

    @NonNull
    @Override
    public abstract String getName();

    /**
     * Gets the attribute of this item with the specified id.
     */
    @Nullable
    public abstract Attribute getAttribute(final long id);

    /**
     * The attributes of the item, contains things such as how much it can heal and what items are needed to create it.
     */
    public abstract List<Attribute> getAttributes();

    public abstract Map<Color, Color> getColorSubstitutions();

    public abstract Map<Material, Material> getMaterialSubstitutions();

    public abstract String[] getRawGroundActions();

    @NonNull
    public final List<String> getGroundActions() {
        String[] gaa = getRawGroundActions();
        final List<String> actions = new ArrayList<>(gaa.length);
        for (final String action : gaa) {
            if (action != null && !action.isEmpty()) {
                actions.add(action);
            }
        }
        return actions;
    }

    /**
     * Gets the id of the item
     */
    public abstract int getId();

    public abstract String[] getRawInventoryActions();

    public final List<String> getInventoryActions() {
        String[] iaa = getRawInventoryActions();
        final List<String> actions = new ArrayList<>(iaa.length);
        for (final String action : iaa) {
            if (action != null && !action.isEmpty()) {
                actions.add(action);
            }
        }
        return actions;
    }

    public final List<String> getWornActions() {
        return IntStream.of(451, 452, 453, 454, 455, 456, 457, 458).mapToObj(this::getAttribute)
            .filter(Objects::nonNull).map(Attribute::getAsString).collect(Collectors.toList());
    }

    /**
     * Get the id of the item when it's noted (if available)
     */
    public abstract int getNotedId();

    public abstract ItemDefinition getPlaceholderDefinition();

    public abstract ItemDefinition getPlaceholderBaseDefinition();

    /**
     * Gets the first valid equipment slot occupied by this item
     * This is not guaranteed to be accurate (e.g Black mask will return null)
     */
    @Nullable
    public abstract Equipment.Slot getEquipmentSlot();

    /**
     * Primary equipment slot used by this item, according to the cache.
     * Items in the {@link Equipment.Slot#HEAD} contain this information in the secondary equipment slot
     */
    @Nullable
    public abstract Equipment.Slot getPrimaryEquipmentSlot();
    @Nullable
    public abstract Equipment.Slot getSecondaryEquipmentSlot();
    @Nullable
    public abstract Equipment.Slot getTertiaryEquipmentSlot();

    /**
     * Gets the low level alchemy value of the item
     */
    public abstract int getLowLevelAlchemyValue();

    /**
     * Gets the high level alchemy value of the item
     */
    public abstract int getHighLevelAlchemyValue();

    /**
     * Gets the shop value of an item
     */
    public abstract int getShopValue();

    /**
     * Get the id of the item when it's unnoted (if available)
     */
    public abstract int getUnnotedId();

    public abstract int getGroundModelId();

    /**
     * Gets the weight of the item in kg.
     * Weight-reducing items will have negative weight.
     */
    public abstract double getWeight();

    public abstract List<Integer> getMaleModelIds();

    public abstract List<Integer> getFemaleModelIds();

    public abstract List<Integer> getMaleHeadModelIds();

    public abstract List<Integer> getFemaleHeadModelIds();

    /**
     * Checks whether or not this item is members-only
     */
    public abstract boolean isMembersOnly();

    public abstract boolean isNoted();

    public abstract int getCosmeticTemplateId();

    public abstract boolean isCosmetic();

    public abstract boolean isPlaceholder();

    /**
     * Currently being rewritten to behave similarly to the way it's name implies and the way it used to perform.
     *
     * @see #isTradeableOnMarket()
     */
    public abstract boolean isTradeable();

    /**
     * Gets whether or not the item can be traded on the game's market in it's current state (currently, osrs and rs3 use the GrandExchange as it's market)
     * Noted items will return false as should be expected since you're not actually buying and selling the notes themselves, but rather the items they represent/contain.
     *
     * @return true if you can buy and sell the item on the game's market in it's current state
     */
    public abstract boolean isTradeableOnMarket();

    public abstract boolean isTwoHanded();

    public abstract int getTeamId();

    public abstract ItemDefinition getCosmeticDefinition();

    public abstract ItemDefinition getCosmeticBaseDefinition();

    public abstract int getPlaceholderId();

    /**
     * On osrs, it returns the action that is triggered when a shift click style drop is used on this item.
     *
     * @return a String or null if there is no shift click action defined.
     */
    @Nullable
    public abstract String getShiftClickAction();

    /**
     * Gets whether or not multiple of this item will stack into a single inventory slot
     *
     * @return true if it can stack, otherwise false.
     */
    public abstract boolean stacks();

    public ItemDefinition getUnnotedDefinition() {
        if (isNoted()) {
            return ItemDefinition.get(getUnnotedId());
        }
        return this;
    }

    @Override
    public String toString() {
        return "ItemDefinition(id: " + getId() + ", name: " + getName() + ')';
    }


    public abstract Multimap<String, String> getSubActions();
}
