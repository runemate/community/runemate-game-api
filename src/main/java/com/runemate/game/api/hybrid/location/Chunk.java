package com.runemate.game.api.hybrid.location;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.region.*;
import lombok.*;
import org.jetbrains.annotations.*;

/**
 * A chunk represents an 8x8 sub-region. These can be copied, tiled, translated and rotated to build instanced environments.
 */
@Getter
@ToString
public final class Chunk {

    public static final int WIDTH = 8;
    public static final int HEIGHT = 8;

    private final int id, orientation, x, y, plane;

    public Chunk(int id) {
        this.id = id;
        orientation = id >> 1 & 0x3;
        y = (id >> 3 & 0x7FF) * 8;
        x = (id >> 14 & 0x3FF) * 8;
        plane = id >> 24 & 0x3;
    }

    /**
     * @return the "global" coordinate representing the bottom-left of this chunk
     */
    @NonNull
    public Coordinate getBase() {
        return new Coordinate(x, y, plane);
    }

    /**
     * @return the "global" 8x8 area
     */
    @NonNull
    public Area.Rectangular getArea() {
        Coordinate base = getBase();
        return Area.rectangular(base, base.derive(7, 7));
    }

    /**
     * @return true if this chunk contains either the position of the passed {@link Locatable},
     * or the {@link Coordinate} as returned by {@link #uninstance(Locatable)}.
     */
    public boolean contains(@NonNull Locatable locatable) {
        Area.Rectangular area = getArea();
        return area.contains(locatable) || area.contains(uninstance(locatable));
    }

    /**
     * Attempts to translate an "instanced" {@link Coordinate} (contained within this chunk) to the base game world.
     */
    @Nullable
    public Coordinate uninstance(@NonNull Locatable locatable) {
        Coordinate position = locatable.getPosition();
        if (position == null) {
            return null;
        }

        Coordinate.SceneOffset offset = position.getSceneOffset();
        if (offset == null || !offset.isValid()) {
            return null;
        }

        int chunkX = offset.getX() & (WIDTH - 1);
        int chunkY = offset.getY() & (HEIGHT - 1);
        int rotation = (4 - orientation) % 4;
        return new Coordinate(x + rotateTemplateX(chunkX, chunkY, rotation), y + rotateTemplateY(chunkX, chunkY, rotation), plane);
    }

    /**
     * Attempts to translate a {@link Coordinate} in the base game world (contained within this chunk) to the current instance.
     */
    public Coordinate instance(@NonNull Locatable locatable) {
        Coordinate position = locatable.getPosition();
        if (position == null) {
            return null;
        }

        Coordinate base = Scene.getBase(position.getPlane());
        int[][] loaded = Scene.getLoadedChunkIds()[position.getPlane()];
        for (int x = 0; x < loaded.length; x++) {
            for (int y = 0; y < loaded[x].length; y++) {
                if (loaded[x][y] == id) {
                    return new Coordinate(
                        base.getX() + (x * WIDTH) + (position.getX() & (WIDTH - 1)),
                        base.getY() + (y * HEIGHT) + (position.getY() & (HEIGHT - 1)),
                        position.getPlane()
                    );
                }
            }
        }

        return null;
    }

    /**
     * Attempts to find a loaded {@link Chunk} that contains the provided {@link Locatable}.
     */
    @Nullable
    public static Chunk containing(Locatable locatable) {
        Coordinate position = locatable.getPosition();
        if (position == null) {
            return null;
        }

        Coordinate.SceneOffset offset = position.getSceneOffset();
        if (!offset.isValid()) {
            //If the offset isn't valid then we could be attempting to map a non-instanced position to an instance one, so we can do a deeper search
            return Scene.getLoadedChunks().stream()
                .filter(chunk -> chunk.contains(position))
                .findFirst()
                .orElse(null);
        }

        return containing(offset.getX(), offset.getY(), position.getPlane());
    }

    /**
     * Attempts to find a loaded {@link Chunk} that contains a coordinate with the provided coordinates.
     */
    @Nullable
    public static Chunk containing(int sceneX, int sceneY, int plane) {
        if (!Region.isInstanced()) {
            return null;
        }

        int[][][] templates = Scene.getLoadedChunkIds();
        return new Chunk(templates[plane][sceneX / 8][sceneY / 8]);
    }

    @Override
    public boolean equals(Object o) {
        return this == o || (o instanceof Chunk c && c.id == id);
    }

    @Override
    public int hashCode() {
        return id;
    }

    private static int rotateTemplateX(int chunkX, int chunkY, int rotation) {
        switch (rotation) {
            case 0:
                return chunkX;
            case 1:
                return chunkY;
            case 2:
                return (WIDTH - 1) - chunkX;
            case 3:
                return (HEIGHT - 1) - chunkY;
        }
        throw new IllegalStateException("Cannot rotate because the templates orientation is outside the bounds of [0,3]");
    }

    private static int rotateTemplateY(int chunkX, int chunkY, int rotation) {
        switch (rotation) {
            case 0:
                return chunkY;
            case 1:
                return (WIDTH - 1) - chunkX;
            case 2:
                return (HEIGHT - 1) - chunkY;
            case 3:
                return chunkX;
        }
        throw new IllegalStateException("Cannot rotate because the templates orientation is outside the bounds of [0,3]");
    }
}
