package com.runemate.game.api.hybrid.local.hud.interfaces;

import com.runemate.game.api.client.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.player_sense.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.api.script.*;
import com.runemate.game.api.script.annotations.*;
import java.util.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import org.jetbrains.annotations.*;
import lombok.extern.log4j.*;

@Log4j2
public final class Trade {

    public final static int CONFIRM_CONTAINER = 334;
    public final static int OFFERS_CONTAINER = 335;
    public final static Pattern VALUE_PATTERN = Pattern.compile("^(.*\\r?\\n?\\(Value: )?(?<value>(\\d+,?)+) coins\\)?|Nothing$");

    private Trade() {
    }

    public static boolean accept() {
        if (isWaitingForOtherPlayer()) {
            log.debug("We've accepted, now we're waiting for the other player.");
            return true;
        }
        //334, 25
        InterfaceComponent component = Interfaces.newQuery()
            .containers(OFFERS_CONTAINER, CONFIRM_CONTAINER)
            .actions("Accept")
            .grandchildren(false)
            .visible()
            .results()
            .first();
        log.info("Accepting on the {} screen by clicking {} ", getCurrentScreen().name(), component);
        return component != null && component.interact("Accept");
    }

    public static boolean atConfirmationScreen() {
        InterfaceComponent comp = Interfaces.getAt(CONFIRM_CONTAINER, 0);
        return comp != null && comp.isVisible();
    }

    public static boolean atOfferScreen() {
        InterfaceComponent comp = Interfaces.getAt(OFFERS_CONTAINER, 0);
        return comp != null && comp.isVisible();
    }

    public static boolean close() {
        return close(PlayerSense.getAsBoolean(PlayerSense.Key.CLOSE_BANK_WITH_ESCAPE));
    }

    public static boolean close(boolean keybind) {
        InterfaceComponent closeComp = Interfaces.newQuery()
            .containers(OFFERS_CONTAINER, CONFIRM_CONTAINER)
            .actions("Close")
            .visible()
            .results()
            .first();
        final var interaction = closeComp != null && closeComp.interact("Close");
        return interaction && Execution.delayUntil(() -> !isOpen(), 2000);
    }

    public static boolean decline() {
        InterfaceComponent component = Interfaces.newQuery()
            .containers(OFFERS_CONTAINER, CONFIRM_CONTAINER)
            .actions("Decline")
            .grandchildren(false)
            .visible()
            .results()
            .first();
        log.info("Declining on the {} screen by clicking {} ", getCurrentScreen().name(), component);
        return component != null && component.interact("Decline");
    }

    public static Trade.Screen getCurrentScreen() {
        if (atOfferScreen()) {
            return Screen.OFFER;
        } else if (atConfirmationScreen()) {
            return Screen.CONFIRMATION;
        } else {
            return Screen.NONE;
        }
    }

    @Nullable
    public static String getTradersName() {
        return OSRSTrade.getTradersName();
    }

    public static long getTransferValue() {
        return Incoming.getMarketValue() - Outgoing.getMarketValue();
    }

    public static boolean hasOtherPlayerAccepted() {
        return !Interfaces.newQuery()
            .containers(OFFERS_CONTAINER, CONFIRM_CONTAINER)
            .texts("Other player has accepted.")
            .grandchildren(false)
            .visible()
            .results()
            .isEmpty();
    }

    public static boolean isOpen() {
        return atOfferScreen() || atConfirmationScreen();
    }

    public static boolean isWaitingForOtherPlayer() {
        return !Interfaces.newQuery()
            .containers(OFFERS_CONTAINER, CONFIRM_CONTAINER)
            .texts("Waiting for other player...")
            .grandchildren(false)
            .visible()
            .results()
            .isEmpty();
    }

    public static boolean offer(SpriteItem item, int amount) {
        return OSRSTrade.offer(item, amount);
    }

    public static boolean offer(Predicate<SpriteItem> filter, int amount) {
        return offer(Inventory.getItems(filter).random(), amount);
    }

    public static boolean offer(String name, int amount) {
        return offer(Items.getNamePredicate(name), amount);
    }

    public static boolean offer(Pattern name, int amount) {
        return offer(Items.getNamePredicate(name), amount);
    }

    public static boolean offer(int id, int amount) {
        return offer(Items.getIdPredicate(id), amount);
    }

    public static boolean remove(SpriteItem item, int amount) {
        return OSRSTrade.remove(item, amount);
    }

    public static boolean remove(Predicate<SpriteItem> filter, int amount) {
        return remove(Outgoing.getItems(filter).random(), amount);
    }

    public static boolean remove(String name, int amount) {
        return remove(Items.getNamePredicate(name), amount);
    }

    public static boolean remove(Pattern name, int amount) {
        return remove(Items.getNamePredicate(name), amount);
    }

    public static boolean remove(int id, int amount) {
        return remove(Items.getIdPredicate(id), amount);
    }

    private static int parseOfferText(String offerText) {
        if ("Nothing".equals(offerText)) {
            return 0;
        } else {
            Matcher matcher = VALUE_PATTERN.matcher(offerText);
            if (!matcher.find()) {
                return -1;
            } else {
                String amount = matcher.group("value");
                return Integer.parseInt(amount.replaceAll(",", ""));
            }
        }
    }

    public enum Screen {
        OFFER() {
            @Override
            public boolean isOpen() {
                return atOfferScreen();
            }
        }, CONFIRMATION() {
            @Override
            public boolean isOpen() {
                return atConfirmationScreen();
            }
        }, NONE() {
            @Override
            public boolean isOpen() {
                return !atOfferScreen() && !atConfirmationScreen();
            }
        };

        public boolean close() {
            return Trade.close();
        }

        public abstract boolean isOpen();
    }

    public static final class Incoming {

        private Incoming() {
        }


        public static SpriteItemQueryResults getItems(Predicate<SpriteItem> filter) {
            return newQuery().filter(filter).results();
        }

        public static SpriteItemQueryResults getItems() {
            return getItems((Predicate<SpriteItem>) null);
        }

        public static SpriteItemQueryResults getItems(final int... ids) {
            return getItems(Items.getIdPredicate(ids));
        }

        public static SpriteItemQueryResults getItems(final String... names) {
            return getItems(Items.getNamePredicate(names));
        }

        public static SpriteItemQueryResults getItems(final Pattern... namePatterns) {
            return getItems(Items.getNamePredicate(namePatterns));
        }

        /**
         * Checks if any items match the given {@link Predicate filter}
         *
         * @param filter the predicate to check the items against
         * @return true if an item matches the filter
         */
        public static boolean contains(Predicate<SpriteItem> filter) {
            return Items.contains(getItems(), filter);
        }

        /**
         * Checks if any items match the given id
         *
         * @param id the id to check the items against
         * @return true if an item matches the id
         */
        public static boolean contains(int id) {
            return Items.contains(getItems(), id);
        }

        /**
         * Checks if any items match the given name
         *
         * @param name the name to check the items against
         * @return true if an item matches the name
         */
        public static boolean contains(String name) {
            return Items.contains(getItems(), name);
        }

        /**
         * Checks if any items match the given name
         *
         * @param name the name to check the items against
         * @return true if an item matches the name
         */
        public static boolean contains(Pattern name) {
            return Items.contains(getItems(), name);
        }

        /**
         * Checks if the supplied {@link Predicate filter} matches at least one item
         *
         * @param predicate the predicate to check the items against
         * @return true if the predicate matches an item
         */
        public static boolean containsAllOf(final Predicate<SpriteItem> predicate) {
            return Items.containsAllOf(getItems(), predicate);
        }

        /**
         * Checks if all of the supplied {@link Predicate filter}s match at least one item each
         *
         * @param filters the predicates to check the items against
         * @return true if all of the predicates have a match
         */
        @SafeVarargs
        public static boolean containsAllOf(final Predicate<SpriteItem>... filters) {
            return Items.containsAllOf(getItems(), filters);
        }

        /**
         * Checks if all of the supplied ids match at least one item each
         *
         * @param ids the ids to check the items against
         * @return true if all of the ids have a match
         */
        public static boolean containsAllOf(final int... ids) {
            return Items.containsAllOf(getItems(), ids);
        }

        /**
         * Checks if all of the supplied names match at least one item each
         *
         * @param names the names to check the items against
         * @return true if all of the names have a match
         */
        public static boolean containsAllOf(final String... names) {
            return Items.containsAllOf(getItems(), names);
        }

        /**
         * Checks if all of the supplied names match at least one item each
         *
         * @param names the names to check the items against
         * @return true if all of the names have a match
         */
        public static boolean containsAllOf(final Pattern... names) {
            return Items.containsAllOf(getItems(), names);
        }

        /**
         * Checks if any items don't match the given {@link Predicate filter}
         *
         * @param filter the predicate to check the items against
         * @return true if at least one item doesn't match the filter
         */
        public static boolean containsAnyExcept(final Predicate<SpriteItem> filter) {
            return Items.containsAnyExcept(getItems(), filter);
        }

        /**
         * Checks if any items don't match the given {@link Predicate filter}s
         *
         * @param filters the predicates to check the items against
         * @return true if at least one item doesn't match the filters
         */
        @SafeVarargs
        public static boolean containsAnyExcept(final Predicate<SpriteItem>... filters) {
            return Items.containsAnyExcept(getItems(), filters);
        }

        /**
         * Checks if any items don't match the given ids
         *
         * @param ids the ids to check the items against
         * @return true if at least one item doesn't match the ids
         */
        public static boolean containsAnyExcept(final int... ids) {
            return Items.containsAnyExcept(getItems(), ids);
        }

        /**
         * Checks if any items don't match the given names
         *
         * @param names the names to check the items against
         * @return true if at least one item doesn't match the names
         */
        public static boolean containsAnyExcept(final String... names) {
            return Items.containsAnyExcept(getItems(), names);
        }

        /**
         * Checks if any items don't match the given names
         *
         * @param names the names to check the items against
         * @return true if at least one item doesn't match the names
         */
        public static boolean containsAnyExcept(final Pattern... names) {
            return Items.containsAnyExcept(getItems(), names);
        }

        /**
         * Checks if any item matches the given {@link Predicate filter}
         *
         * @param filter the filter to check the items against
         * @return true if at least one item matches the filter
         */
        public static boolean containsAnyOf(final Predicate<SpriteItem> filter) {
            return Items.containsAnyOf(getItems(), filter);
        }

        /**
         * Checks if any item matches the given {@link Predicate filter}s
         *
         * @param filters the filters to check the items against
         * @return true if at least one item matches a filter
         */
        @SafeVarargs
        public static boolean containsAnyOf(final Predicate<SpriteItem>... filters) {
            return Items.containsAnyOf(getItems(), filters);
        }

        /**
         * Checks if any item matches the given ids
         *
         * @param ids the ids to check the items against
         * @return true if at least one item matches an id
         */
        public static boolean containsAnyOf(final int... ids) {
            return Items.containsAnyOf(getItems(), ids);
        }

        /**
         * Checks if any item matches the given names
         *
         * @param names the names to check the items against
         * @return true if at least one item matches a name
         */
        public static boolean containsAnyOf(final String... names) {
            return Items.containsAnyOf(getItems(), names);
        }

        /**
         * Checks if any item matches the given names
         *
         * @param names the names to check the items against
         * @return true if at least one item matches a name
         */
        public static boolean containsAnyOf(final Pattern... names) {
            return Items.containsAnyOf(getItems(), names);
        }

        /**
         * Checks if all of the items match the given {@link Predicate filter}
         *
         * @param filter the filter to check the items against
         * @return true if all items match the filter
         */
        public static boolean containsOnly(final Predicate<SpriteItem> filter) {
            return Items.containsOnly(getItems(), filter);
        }

        /**
         * Checks if all of the items match the given {@link Predicate filter}s
         *
         * @param filters the filters to check the items against
         * @return true if all items match at least one filter each
         */
        @SafeVarargs
        public static boolean containsOnly(final Predicate<SpriteItem>... filters) {
            return Items.containsOnly(getItems(), filters);
        }

        /**
         * Checks if all of the items match the given names
         *
         * @param names the filters to check the items against
         * @return true if all items match at least one name each
         */
        public static boolean containsOnly(final String... names) {
            return Items.containsOnly(getItems(), names);
        }

        /**
         * Checks if all of the items match the given names
         *
         * @param names the filters to check the items against
         * @return true if all items match at least one name each
         */
        public static boolean containsOnly(final Pattern... names) {
            return Items.containsOnly(getItems(), names);
        }

        /**
         * Gets the total quantity of items
         *
         * @return the total quantity of items
         */
        public static int getQuantity() {
            return Items.getQuantity(getItems());
        }

        /**
         * Gets the total quantity of items matching the filter
         *
         * @param filter the filter to check the items against
         * @return the total quantity of items matching the filter
         */
        public static int getQuantity(final Predicate<SpriteItem> filter) {
            return Items.getQuantity(getItems(), filter);
        }

        /**
         * Gets the total quantity of items matching the {@link Predicate filter}s
         *
         * @param filters the filters to check the items against
         * @return the total quantity of items matching the filters
         */
        @SafeVarargs
        public static int getQuantity(final Predicate<SpriteItem>... filters) {
            return Items.getQuantity(getItems(), filters);
        }

        /**
         * Gets the total quantity of items matching the ids
         *
         * @param ids the ids to check the items against
         * @return the total quantity of items matching the ids
         */
        public static int getQuantity(final int... ids) {
            return Items.getQuantity(getItems(), ids);
        }

        /**
         * Gets the total quantity of items matching the names
         *
         * @param names the ids to check the items against
         * @return the total quantity of items matching the names
         */
        public static int getQuantity(final String... names) {
            return Items.getQuantity(getItems(), names);
        }

        /**
         * Gets the total quantity of items matching the names
         *
         * @param names the ids to check the items against
         * @return the total quantity of items matching the names
         */
        public static int getQuantity(final Pattern... names) {
            return Items.getQuantity(getItems(), names);
        }

        public static List<InteractableRectangle> getBounds() {
            InterfaceComponentQueryResults candidates = Interfaces.newQuery()
                .containers(Trade.OFFERS_CONTAINER)
                .types(InterfaceComponent.Type.CONTAINER)
                .grandchildren(56)
                .results();
            return candidates.size() < 2
                ? null
                : candidates.get(1).getChildren().stream().map(InterfaceComponent::getBounds).collect(Collectors.toList());
        }

        public static InteractableRectangle getBoundsOf(int index) {
            List<InteractableRectangle> bounds = getBounds();
            return bounds == null ? null : bounds.get(index);
        }

        public static int getEmptySlots() {
            return 28 - getUsedSlots();
        }

        @Nullable
        public static SpriteItem getItemIn(int index) {
            return getItems(i -> i.getIndex() == index).first();
        }

        public static long getMarketValue() {
            InterfaceComponentQueryResults offerWorthComps = Interfaces.newQuery()
                .containers(Trade.OFFERS_CONTAINER, Trade.CONFIRM_CONTAINER)
                .texts(VALUE_PATTERN)
                .grandchildren(false)
                .results();
            InterfaceComponent offerWorthComp = offerWorthComps.size() < 2 ? null : offerWorthComps.get(1);
            String offerText;
            return (offerWorthComp == null || (offerText = offerWorthComp.getText()) == null) ? -1 : parseOfferText(offerText);
        }

        public static int getUsedSlots() {
            return getItems().size();
        }

        public static boolean isEmpty() {
            return getUsedSlots() == 0;
        }

        public static boolean isFull() {
            return getUsedSlots() == 28;
        }

        public static SpriteItemQueryBuilder newQuery() {
            return new SpriteItemQueryBuilder(Inventories.Documented.TRADEOTHER);
        }
    }

    public final static class Outgoing {

        private Outgoing() {
        }

        public static SpriteItemQueryResults getItems(Predicate<SpriteItem> filter) {
            return newQuery().filter(filter).results();
        }

        public static SpriteItemQueryResults getItems() {
            return getItems((Predicate<SpriteItem>) null);
        }

        public static SpriteItemQueryResults getItems(final int... ids) {
            return getItems(Items.getIdPredicate(ids));
        }

        public static SpriteItemQueryResults getItems(final String... names) {
            return getItems(Items.getNamePredicate(names));
        }

        public static SpriteItemQueryResults getItems(final Pattern... namePatterns) {
            return getItems(Items.getNamePredicate(namePatterns));
        }

        /**
         * Checks if any items match the given {@link Predicate filter}
         *
         * @param filter the predicate to check the items against
         * @return true if an item matches the filter
         */
        public static boolean contains(Predicate<SpriteItem> filter) {
            return Items.contains(getItems(), filter);
        }

        /**
         * Checks if any items match the given id
         *
         * @param id the id to check the items against
         * @return true if an item matches the id
         */
        public static boolean contains(int id) {
            return Items.contains(getItems(), id);
        }

        /**
         * Checks if any items match the given name
         *
         * @param name the name to check the items against
         * @return true if an item matches the name
         */
        public static boolean contains(String name) {
            return Items.contains(getItems(), name);
        }

        /**
         * Checks if any items match the given name
         *
         * @param name the name to check the items against
         * @return true if an item matches the name
         */
        public static boolean contains(Pattern name) {
            return Items.contains(getItems(), name);
        }

        /**
         * Checks if the supplied {@link Predicate filter} matches at least one item
         *
         * @param predicate the predicate to check the items against
         * @return true if the predicate matches an item
         */
        public static boolean containsAllOf(final Predicate<SpriteItem> predicate) {
            return Items.containsAllOf(getItems(), predicate);
        }

        /**
         * Checks if all of the supplied {@link Predicate filter}s match at least one item each
         *
         * @param filters the predicates to check the items against
         * @return true if all of the predicates have a match
         */
        @SafeVarargs
        public static boolean containsAllOf(final Predicate<SpriteItem>... filters) {
            return Items.containsAllOf(getItems(), filters);
        }

        /**
         * Checks if all of the supplied ids match at least one item each
         *
         * @param ids the ids to check the items against
         * @return true if all of the ids have a match
         */
        public static boolean containsAllOf(final int... ids) {
            return Items.containsAllOf(getItems(), ids);
        }

        /**
         * Checks if all of the supplied names match at least one item each
         *
         * @param names the names to check the items against
         * @return true if all of the names have a match
         */
        public static boolean containsAllOf(final String... names) {
            return Items.containsAllOf(getItems(), names);
        }

        /**
         * Checks if all of the supplied names match at least one item each
         *
         * @param names the names to check the items against
         * @return true if all of the names have a match
         */
        public static boolean containsAllOf(final Pattern... names) {
            return Items.containsAllOf(getItems(), names);
        }

        /**
         * Checks if any items don't match the given {@link Predicate filter}
         *
         * @param filter the predicate to check the items against
         * @return true if at least one item doesn't match the filter
         */
        public static boolean containsAnyExcept(final Predicate<SpriteItem> filter) {
            return Items.containsAnyExcept(getItems(), filter);
        }

        /**
         * Checks if any items don't match the given {@link Predicate filter}s
         *
         * @param filters the predicates to check the items against
         * @return true if at least one item doesn't match the filters
         */
        @SafeVarargs
        public static boolean containsAnyExcept(final Predicate<SpriteItem>... filters) {
            return Items.containsAnyExcept(getItems(), filters);
        }

        /**
         * Checks if any items don't match the given ids
         *
         * @param ids the ids to check the items against
         * @return true if at least one item doesn't match the ids
         */
        public static boolean containsAnyExcept(final int... ids) {
            return Items.containsAnyExcept(getItems(), ids);
        }

        /**
         * Checks if any items don't match the given names
         *
         * @param names the names to check the items against
         * @return true if at least one item doesn't match the names
         */
        public static boolean containsAnyExcept(final String... names) {
            return Items.containsAnyExcept(getItems(), names);
        }

        /**
         * Checks if any items don't match the given names
         *
         * @param names the names to check the items against
         * @return true if at least one item doesn't match the names
         */
        public static boolean containsAnyExcept(final Pattern... names) {
            return Items.containsAnyExcept(getItems(), names);
        }

        /**
         * Checks if any item matches the given {@link Predicate filter}
         *
         * @param filter the filter to check the items against
         * @return true if at least one item matches the filter
         */
        public static boolean containsAnyOf(final Predicate<SpriteItem> filter) {
            return Items.containsAnyOf(getItems(), filter);
        }

        /**
         * Checks if any item matches the given {@link Predicate filter}s
         *
         * @param filters the filters to check the items against
         * @return true if at least one item matches a filter
         */
        @SafeVarargs
        public static boolean containsAnyOf(final Predicate<SpriteItem>... filters) {
            return Items.containsAnyOf(getItems(), filters);
        }

        /**
         * Checks if any item matches the given ids
         *
         * @param ids the ids to check the items against
         * @return true if at least one item matches an id
         */
        public static boolean containsAnyOf(final int... ids) {
            return Items.containsAnyOf(getItems(), ids);
        }

        /**
         * Checks if any item matches the given names
         *
         * @param names the names to check the items against
         * @return true if at least one item matches a name
         */
        public static boolean containsAnyOf(final String... names) {
            return Items.containsAnyOf(getItems(), names);
        }

        /**
         * Checks if any item matches the given names
         *
         * @param names the names to check the items against
         * @return true if at least one item matches a name
         */
        public static boolean containsAnyOf(final Pattern... names) {
            return Items.containsAnyOf(getItems(), names);
        }

        /**
         * Checks if all of the items match the given {@link Predicate filter}
         *
         * @param filter the filter to check the items against
         * @return true if all items match the filter
         */
        public static boolean containsOnly(final Predicate<SpriteItem> filter) {
            return Items.containsOnly(getItems(), filter);
        }

        /**
         * Checks if all of the items match the given {@link Predicate filter}s
         *
         * @param filters the filters to check the items against
         * @return true if all items match at least one filter each
         */
        @SafeVarargs
        public static boolean containsOnly(final Predicate<SpriteItem>... filters) {
            return Items.containsOnly(getItems(), filters);
        }

        /**
         * Checks if all of the items match the given names
         *
         * @param names the filters to check the items against
         * @return true if all items match at least one name each
         */
        public static boolean containsOnly(final String... names) {
            return Items.containsOnly(getItems(), names);
        }

        /**
         * Checks if all of the items match the given names
         *
         * @param names the filters to check the items against
         * @return true if all items match at least one name each
         */
        public static boolean containsOnly(final Pattern... names) {
            return Items.containsOnly(getItems(), names);
        }

        /**
         * Gets the total quantity of items
         *
         * @return the total quantity of items
         */
        public static int getQuantity() {
            return Items.getQuantity(getItems());
        }

        /**
         * Gets the total quantity of items matching the filter
         *
         * @param filter the filter to check the items against
         * @return the total quantity of items matching the filter
         */
        public static int getQuantity(final Predicate<SpriteItem> filter) {
            return Items.getQuantity(getItems(), filter);
        }

        /**
         * Gets the total quantity of items matching the {@link Predicate filter}s
         *
         * @param filters the filters to check the items against
         * @return the total quantity of items matching the filters
         */
        @SafeVarargs
        public static int getQuantity(final Predicate<SpriteItem>... filters) {
            return Items.getQuantity(getItems(), filters);
        }

        /**
         * Gets the total quantity of items matching the ids
         *
         * @param ids the ids to check the items against
         * @return the total quantity of items matching the ids
         */
        public static int getQuantity(final int... ids) {
            return Items.getQuantity(getItems(), ids);
        }

        /**
         * Gets the total quantity of items matching the names
         *
         * @param names the ids to check the items against
         * @return the total quantity of items matching the names
         */
        public static int getQuantity(final String... names) {
            return Items.getQuantity(getItems(), names);
        }

        /**
         * Gets the total quantity of items matching the names
         *
         * @param names the ids to check the items against
         * @return the total quantity of items matching the names
         */
        public static int getQuantity(final Pattern... names) {
            return Items.getQuantity(getItems(), names);
        }

        public static List<InteractableRectangle> getBounds() {
            InterfaceComponent candidate = Interfaces.newQuery()
                .containers(Trade.OFFERS_CONTAINER)
                .types(InterfaceComponent.Type.CONTAINER)
                .grandchildren(56)
                .results()
                .first();
            return candidate == null
                ? null
                : candidate.getChildren().stream().map(InterfaceComponent::getBounds).collect(Collectors.toList());
        }

        public static InteractableRectangle getBoundsOf(int index) {
            List<InteractableRectangle> bounds = getBounds();
            return bounds == null ? null : bounds.get(index);
        }

        public static int getEmptySlots() {
            return 28 - getUsedSlots();
        }

        @Nullable
        public static SpriteItem getItemIn(int index) {
            return getItems(i -> i.getIndex() == index).first();
        }

        //TODO: RS3 Confirmation screen doesn't have interfaces to parse
        public static long getMarketValue() {
            InterfaceComponent offerWorthComp = Interfaces.newQuery()
                .containers(Trade.OFFERS_CONTAINER, Trade.CONFIRM_CONTAINER)
                .texts(VALUE_PATTERN)
                .grandchildren(false)
                .results()
                .first();
            String offerText;
            return (offerWorthComp == null || (offerText = offerWorthComp.getText()) == null) ? -1 : parseOfferText(offerText);
        }

        public static int getUsedSlots() {
            return getItems().size();
        }

        public static boolean isEmpty() {
            return getUsedSlots() == 0;
        }

        public static boolean isFull() {
            return getUsedSlots() == 28;
        }

        public static SpriteItemQueryBuilder newQuery() {
            return new SpriteItemQueryBuilder(Inventories.Documented.TRADE);
        }
    }
}
