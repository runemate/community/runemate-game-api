package com.runemate.game.api.hybrid.local.hud.interfaces;

import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.api.script.annotations.*;
import java.util.regex.*;

public final class NpcContact {

    private NpcContact() {
    }

    public static boolean isOpen() {
        return OSRSNpcContact.isOpen();
    }

    public static boolean cast(Contact contact) {
        return OSRSNpcContact.cast(contact);
    }

    public enum OSRS implements Contact {
        HONEST_JIMMY("Honest Jimmy"),
        BERT_THE_SANDMAN("Bert the Sandman"),
        ADVISOR_GHRIM("Advisor Ghrim"),
        DARK_MAGE("Dark Mage"),
        LANTHUS("Lanthus"),
        TURAEL("Turael"),
        MAZCHNA("Mazchna"),
        VANNAKA("Vannaka"),
        CHAELDAR("Chaeldar"),
        NIEVE_STEVE("Nieve|Steve"),

        /**
         * @deprecated Removed after While Guthix Sleeps release. Use DURADEL_KURADAL.
         */
        @Deprecated
        DURADEL("Duradel"),
        DURADEL_KURADAL("Duradel|Kuradal"),
        KRYSTILIA("Krystilia"),
        MURPHY("Murphy"),
        CYRISUS("Cyrisus"),
        SMOGGY("Smoggy"),
        GINEA("Ginea"),
        WATSON("Watson");
        private final Pattern name;

        OSRS(String name) {
            this.name = Pattern.compile("^" + name + "$");
        }

        @Override
        public InterfaceComponent getComponent() {
            return Interfaces.newQuery().actions(getAction()).containers(getContainerId())
                .types(InterfaceComponent.Type.CONTAINER).results().first();
        }

        @Override
        public int getContainerId() {
            return 75;
        }

        @Override
        public Pattern getAction() {
            return name;
        }

        @Override
        public String toString() {
            return "Contact." + name();
        }
    }

    public interface Contact {
        InterfaceComponent getComponent();

        Pattern getAction();

        int getContainerId();

        String name();
    }

}
