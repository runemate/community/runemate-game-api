package com.runemate.game.api.hybrid.queries;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;
import org.apache.commons.lang3.*;

public class GroundItemQueryBuilder
    extends LocatableEntityQueryBuilder<GroundItem, GroundItemQueryBuilder> {
    private int[] ids;
    private Boolean noted, stacks;
    private Integer minQuantity, maxQuantity;
    private Pattern[] names, actions, inventoryActions;
    private GroundItem.Ownership[] ownerships;

    public final GroundItemQueryBuilder names(final String... names) {
        return names(Regex.getPatternsForExactStrings(names).toArray(new Pattern[names.length]));
    }

    public final GroundItemQueryBuilder names(final Pattern... names) {
        this.names = names;
        return get();
    }

    public final GroundItemQueryBuilder noted() {
        this.noted = true;
        return get();
    }

    public final GroundItemQueryBuilder unnoted() {
        this.noted = false;
        return get();
    }

    public final GroundItemQueryBuilder stacks(boolean yes) {
        this.stacks = yes;
        return get();
    }

    public GroundItemQueryBuilder ids(final int... ids) {
        this.ids = ids;
        return get();
    }

    public final GroundItemQueryBuilder actions(final String... actions) {
        return actions(
            Regex.getPatternsForExactStrings(actions).toArray(new Pattern[actions.length]));
    }

    public final GroundItemQueryBuilder actions(final Pattern... actions) {
        this.actions = actions;
        return get();
    }

    public final GroundItemQueryBuilder inventoryActions(final String... inventoryActions) {
        return inventoryActions(Regex.getPatternsForExactStrings(inventoryActions)
            .toArray(new Pattern[inventoryActions.length]));
    }

    public final GroundItemQueryBuilder inventoryActions(final Pattern... inventoryActions) {
        this.inventoryActions = inventoryActions;
        return get();
    }

    /**
     * Filters GroundItems such that {@link GroundItem#getQuantity()} &gt;= minQuantity
     */
    public GroundItemQueryBuilder quantity(int minQuantity) {
        this.minQuantity = minQuantity;
        return get();
    }

    /**
     * Filters GroundItems such that minQuantity &lt;= {@link GroundItem#getQuantity()} &lt;= maxQuantity
     */
    public GroundItemQueryBuilder quantity(int minQuantity, int maxQuantity) {
        this.minQuantity = minQuantity;
        this.maxQuantity = maxQuantity;
        return get();
    }

    /**
     * Filters GroundItems that have the provided {@link GroundItem#getOwnership()}.
     */
    public final GroundItemQueryBuilder ownership(GroundItem.Ownership... ownerships) {
        this.ownerships = ownerships;
        return get();
    }

    /**
     * <ul>
     *     <li>For main accounts, this is the same as {@code ownership(NONE, SELF, OTHER, GROUP)}</li>
     *     <li>For iron accountss, this is the same as {@code ownership(NONE, SELF, GROUP)}</li>
     * </ul>
     */
    public GroundItemQueryBuilder mine() {
        if (AccountInfo.getType() != AccountInfo.AccountType.REGULAR) {
            return ownership(GroundItem.Ownership.NONE, GroundItem.Ownership.SELF, GroundItem.Ownership.GROUP);
        }
        return ownership(GroundItem.Ownership.values());
    }

    @Override
    public GroundItemQueryBuilder get() {
        return this;
    }

    @Override
    public Callable<List<? extends GroundItem>> getDefaultProvider() {
        return () -> {
            if (on != null) {
                if (on.size() == 1) {
                    return GroundItems.getLoadedOn(on.iterator().next()).asList();
                }
                List<GroundItem> items = new ArrayList<>();
                on.forEach(coordinate -> items.addAll(GroundItems.getLoadedOn(coordinate)));
                return items;
            }
            if (within != null) {
                if (within.size() == 1) {
                    return GroundItems.getLoadedWithin(within.iterator().next()).asList();
                }
                List<GroundItem> items = new ArrayList<>();
                within.forEach(coordinate -> items.addAll(GroundItems.getLoadedWithin(coordinate)));
                return items;
            }
            return GroundItems.getLoaded().asList();
        };
    }

    @Override
    public boolean accepts(GroundItem argument) {
        boolean condition;
        if (ids != null) {
            condition = false;
            int id = argument.getId();
            for (final int value : ids) {
                if (value == id) {
                    condition = true;
                    break;
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (minQuantity != null) {
            int quantity = argument.getQuantity();
            if (quantity < minQuantity || maxQuantity != null && quantity > maxQuantity) {
                return false;
            }
        }
        if (ownerships != null) {
            GroundItem.Ownership ownership = argument.getOwnership();
            if (!ArrayUtils.contains(ownerships, ownership)) {
                return false;
            }
        }
        ItemDefinition definition = null;
        if (noted != null) {
            condition = false;
            if (definition == null) {
                definition = argument.getDefinition();
            }
            if (definition != null) {
                condition = noted == definition.isNoted();
            }
            if (!condition) {
                return false;
            }
        }
        if (stacks != null) {
            condition = false;
            if (definition == null) {
                definition = argument.getDefinition();
            }
            if (definition != null) {
                condition = stacks == definition.stacks();
            }
            if (!condition) {
                return false;
            }
        }
        if (names != null) {
            condition = false;
            if (definition == null) {
                definition = argument.getDefinition();
            }
            if (definition != null) {
                String name = definition.getName();
                for (final Pattern value : this.names) {
                    if (value.matcher(name).find()) {
                        condition = true;
                        break;
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (actions != null) {
            if (definition == null) {
                definition = argument.getDefinition();
            }
            condition = false;
            if (definition != null) {
                List<String> actions = definition.getGroundActions();
                for (final String string : actions) {
                    for (final Pattern pattern : this.actions) {
                        if (pattern.matcher(string).find()) {
                            condition = true;
                            break;
                        }
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (inventoryActions != null) {
            if (definition == null) {
                definition = argument.getDefinition();
            }
            condition = false;
            if (definition != null) {
                List<String> inventoryActions = definition.getInventoryActions();
                for (final String string : inventoryActions) {
                    for (final Pattern pattern : this.inventoryActions) {
                        if (pattern.matcher(string).find()) {
                            condition = true;
                            break;
                        }
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        return super.accepts(argument);
    }

    @Override
    public String toString() {
        return "GroundItemQueryBuilder{" +
            "minQuantity=" + minQuantity +
            ", maxQuantity=" + maxQuantity +
            ", names=" + Arrays.toString(names) +
            ", actions=" + Arrays.toString(actions) +
            '}';
    }
}
