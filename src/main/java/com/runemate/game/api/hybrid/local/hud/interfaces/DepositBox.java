package com.runemate.game.api.hybrid.local.hud.interfaces;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.player_sense.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.hybrid.util.calculations.Random;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import com.runemate.game.api.script.*;
import java.awt.event.*;
import java.util.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import lombok.extern.log4j.*;

@Log4j2
public final class DepositBox {
    private static final int OSRS_CONTAINER = 192;

    private DepositBox() {
    }

    /**
     * Opens the nearest deposit box
     */
    public static boolean open() {
        GameObject deposit_box = DepositBoxes.getLoaded().nearest();
        return deposit_box != null && open(deposit_box);
    }

    /**
     * Open the deposit box specified
     *
     * @return returns true if opened
     */
    public static boolean open(GameObject depositBox) {
        if (isOpen()) {
            return true;
        }
        if (depositBox != null && depositBox.isVisible() && depositBox.interact("Deposit")) {
            Player local = Players.getLocal();
            return local != null && Execution.delayUntil(
                DepositBox::isOpen, local::isMoving, 2000, 2500);
        }
        return false;
    }

    /**
     * Determines if the deposit box interface is open
     *
     * @return Returns true if open
     */
    public static boolean isOpen() {
        return Inventories.opened(Inventories.Documented.INVENTORY) && !Interfaces.newQuery()
            .containers(OSRS_CONTAINER).types(InterfaceComponent.Type.CONTAINER)
            .grandchildren(false).grandchildren(28).visible().results().isEmpty();
    }

    /**
     * Closes deposit box, returns true if already closed
     *
     * @return Returns true if deposit box is not open
     */
    public static boolean close() {
        if (!isOpen()) {
            return true;
        }
        final InterfaceComponent component =
            Interfaces.newQuery().containers(OSRS_CONTAINER).types(InterfaceComponent.Type.SPRITE)
                .actions("Close").visible().results().first();
        if (component != null) {
            if (PlayerSense.getAsBoolean(PlayerSense.Key.CLOSE_BANK_WITH_ESCAPE)
                && OptionsTab.AllSettings.INTERFACES_CLOSABLE_WITH_ESCAPE.isEnabled()) {
                if (Keyboard.typeKey(KeyEvent.VK_ESCAPE)) {
                    return Execution.delayUntil(() -> !isOpen(), Random.nextInt(2000, 2500));
                }
            } else if (component.interact("Close")) {
                return Execution.delayUntil(() -> !isOpen(), Random.nextInt(2000, 2500));
            }
        }
        return false;
    }

    public static boolean deposit(final String name, final int amount) {
        return deposit(Items.getNamePredicate(name), amount);
    }

    /**
     * Deposits the first item that matches the specified filter.
     * If amount is equal to 0 it will deposit all of the item
     */
    public static boolean deposit(final Predicate<SpriteItem> filter, final int amount) {
        return deposit(Inventory.getItems(filter).first(), amount);
    }

    /**
     * Deposits the specified amount of the specified item
     *
     * @param item   the item you want to deposit
     * @param amount the amount you want to deposit
     * @return true if the item is deposited
     */
    public static boolean deposit(final SpriteItem item, final int amount) {
        log.info("Depositing {} {}", amount, item);
        if (item != null && item.hover()) {
            boolean manual = false;
            String action = "Deposit";
            Set<String> actions = Menu.getItems().stream().map(MenuItem::getAction).collect(Collectors.toSet());
            int inInventory = Inventory.getQuantity(item.getId());
            int defaultQuantity = Bank.getExactDefaultQuantity();
            String defaultQuantityAction = action + "-" + (defaultQuantity == 0 ? "All" : defaultQuantity);
            boolean depositRemaining = amount == 0 || amount >= inInventory;
            if (depositRemaining && (defaultQuantity == 0 ||defaultQuantity >= inInventory) && actions.contains(defaultQuantityAction)) {
                action = defaultQuantityAction;
            } else if (inInventory == 1 && actions.contains(action + "-1")) {
                action += "-1";
            } else if (amount == 0 && actions.contains(action + "-All")) {
                action += "-All";
            } else if (actions.contains(action + '-' + amount)) {
                action += "-" + amount;
            } else if (amount != 0 && actions.contains(action + "-X")) {
                manual = true;
                action += "-X";
            } else {
                log.warn("Action not found in " + actions);
            }
            log.debug("Depositing with action: " + action);
            if (actions.contains(action) && item.interact(action)) {
                if (manual) {
                    if (Execution.delayUntil(InputDialog::isOpen, 1400, 2200)) {
                        return InputDialog.enterAmount(amount) && Execution.delayWhile(
                            item::isValid, 1400, 2200);
                    }
                }
                return Execution.delayWhile(item::isValid, 1400, 2200);
            }
        }
        return false;
    }

    public static boolean depositAllExcept(final Pattern... names) {
        return depositAllExcept(Items.getNamePredicate(names));
    }

    public static boolean depositAllExcept(final String... names) {
        return depositAllExcept(item -> {
            final ItemDefinition definition = item.getDefinition();
            if (definition != null) {
                final String itemName = definition.getName();
                for (final String name : names) {
                    if (name.equals(itemName)) {
                        return true;
                    }
                }
            }
            return false;
        });
    }

    public static boolean depositAllExcept(final int... ids) {
        return depositAllExcept(item -> {
            final int itemId = item.getId();
            for (final int id : ids) {
                if (itemId == id) {
                    return true;
                }
            }
            return false;
        });
    }

    /**
     * Deposits all items except those that match the given Predicate
     *
     * @param exclusion filter to test against
     * @return if all non-matching items were successfully deposited
     */
    public static boolean depositAllExcept(final Predicate<SpriteItem> exclusion) {
        if (exclusion == null || !Inventory.containsAnyOf(exclusion)) {
            return depositInventory();
        }
        boolean result = true;
        for (final SpriteItem item : Inventory.getItems()) {
            //verify that it wasn't already deposited
            if (item.isValid() && !exclusion.test(item) && !deposit(item.getId(), 0)) {
                result = false;
            }
        }
        return result;
    }

    public static boolean deposit(final int id, final int amount) {
        return deposit(spriteItem -> spriteItem.getId() == id, amount);
    }

    /**
     * Deposits the inventory, using hotkeys if on rs3 and playersense says to do so.
     *
     * @return Returns true if inventory was deposited
     */
    public static boolean depositInventory() {
        return depositInventory(PlayerSense.getAsBoolean(PlayerSense.Key.USE_BANK_HOTKEYS));
    }

    /**
     * Deposits the inventory, using hotkeys if specified and on rs3
     *
     * @return Returns true if inventory was deposited
     */
    public static boolean depositInventory(boolean hotkeys) {
        if (!isOpen()) {
            return false;
        }
        final SpriteItem item = Inventory.newQuery().results().first();
        if (item == null) {
            return true;
        }
        final String action = "Deposit inventory";
        final InterfaceComponent component =
            Interfaces.newQuery().containers(OSRS_CONTAINER).types(InterfaceComponent.Type.CONTAINER)
                .actions(action).visible().results().first();
        return component != null && component.interact(action) && Execution.delayWhile(
            item::isValid, Random.nextInt(1800, 2400));
    }

    /**
     * Deposits equipment
     *
     * @return Returns true if equipment was deposited
     */
    public static boolean depositEquipment() {
        if (!isOpen()) {
            return false;
        }
        final SpriteItem item = Equipment.newQuery().results().first();
        if (item == null) {
            return true;
        }
        final InterfaceComponent component =
            Interfaces.newQuery().containers(OSRS_CONTAINER).types(InterfaceComponent.Type.SPRITE)
                .actions("Deposit worn items").visible().results().first();
        return component != null && component.interact("Deposit worn items")
            && Execution.delayWhile(item::isValid, Random.nextInt(2000, 2500));
    }
}