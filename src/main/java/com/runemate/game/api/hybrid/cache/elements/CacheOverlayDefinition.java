package com.runemate.game.api.hybrid.cache.elements;

import com.runemate.game.api.hybrid.cache.configs.*;
import com.runemate.game.api.hybrid.cache.materials.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.cache.io.*;
import com.runemate.game.cache.item.*;
import java.awt.*;
import java.io.*;

public class CacheOverlayDefinition extends IncrementallyDecodedItem implements OverlayDefinition {
    public int id;
    public int base_rgb_color;
    public int blendable_rgb_color = -1;
    private int hsl_color;
    private int hsl_blend_color = -1;
    private int materialId = -1;
    private boolean block_shadow = true;
    private int water_color = 0x122b3d;
    private int water_intensity = 127;
    private int water_scale = 64;
    private boolean hideUnderlay = true;
    private boolean unknownop12;
    private int scale = 512;
    private int unknownop11 = 8;
    private int unknownop20;
    private int unknownop21;
    private int unknownop22;
    private boolean unknownop8;

    public CacheOverlayDefinition(int id) {
        this.id = id;
    }

    private static int rgb2hsl(int rgb) {
        if (rgb == Color.MAGENTA.getRGB()) {
            return -1;
        }
        return RSColors.rgbToHsl(rgb);
    }

    @Override
    protected void decode(Js5InputStream stream, int opcode) throws IOException {
        if (opcode == 1) {
            base_rgb_color = stream.readBytes(3);
            hsl_color = rgb2hsl(base_rgb_color);
        } else if (opcode == 2) {
            materialId = stream.readUnsignedByte();
        } else if (opcode == 3) {
            materialId = stream.readUnsignedShort();
            if (materialId == 0xffff) {
                materialId = -1;
            }
        } else if (opcode == 5) {
            hideUnderlay = false;
        } else if (opcode == 7) {
            blendable_rgb_color = stream.readBytes(3);
            hsl_blend_color = rgb2hsl(blendable_rgb_color);
        } else if (opcode == 8) {
            unknownop8 = true;
        } else if (opcode == 9) {
            scale = stream.readUnsignedShort() << 2;
        } else if (opcode == 10) {
            block_shadow = false;
        } else if (opcode == 11) {
            unknownop11 = stream.readUnsignedByte();
        } else if (opcode == 12) {
            unknownop12 = true;
        } else if (opcode == 13) {
            water_color = stream.readBytes(3);
        } else if (opcode == 14) {
            water_scale = stream.readUnsignedByte() << 2;
        } else if (opcode == 16) {
            water_intensity = stream.readUnsignedByte();
        } else if (opcode == 20) {
            unknownop20 = stream.readUnsignedShort();
        } else if (opcode == 21) {
            unknownop21 = stream.readUnsignedByte();
        } else if (opcode == 22) {
            unknownop22 = stream.readUnsignedShort();
        } else {
            throw new DecodingException(-1);
        }
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public Material getMaterial() {
        if (materialId < 0) {
            return null;
        }
        return Materials.load(materialId);
    }

    /*@Override
    public Color getOpaqueColor() {
        if (!hideUnderlay || base_rgb_color == Color.MAGENTA.getRGB()) {
            return null;
        }
        return new Color(base_rgb_color);
    }

    @Override
    public Color getTranslucentColor() {
        if (blendable_rgb_color == -1) {
            return null;
        }
        return new Color(blendable_rgb_color);
    }*/

    @Override
    public String toString() {
        return "OverlayDefinition{id=" + id + '}';
    }
}
