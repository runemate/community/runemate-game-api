package com.runemate.game.api.hybrid.location.navigation.web.requirements;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.region.*;
import java.io.*;
import lombok.*;

public class CombatLevelRequirement extends WebRequirement implements SerializableRequirement {
    private int level;

    public CombatLevelRequirement(int protocol, ObjectInput stream) {
        super(protocol, stream);
    }

    public CombatLevelRequirement(int level) {
        this.level = level;
    }

    @Override
    protected boolean isMet0() {
        Player local = Players.getLocal();
        return local != null && local.getCombatLevel() >= level;
    }

    @Override
    public int getOpcode() {
        return 11;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean serialize(ObjectOutput stream) {
        stream.writeInt(level);
        return true;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean deserialize(int protocol, ObjectInput stream) {
        this.level = stream.readInt();
        return true;
    }

    @Override
    public String toString() {
        return "CombatLevelRequirement(level: " + level + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CombatLevelRequirement that = (CombatLevelRequirement) o;
        return level == that.level;
    }

    @Override
    public int hashCode() {
        return level;
    }
}
