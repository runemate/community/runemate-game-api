package com.runemate.game.api.hybrid.location.navigation.web.vertex_types.objects;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.web.requirements.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.script.*;
import java.io.*;
import java.util.*;
import java.util.regex.*;
import lombok.*;
import lombok.extern.log4j.*;
import org.apache.commons.lang3.builder.*;

@Log4j2
public class BasicObjectVertex extends ObjectVertex implements SerializableVertex {
    private Behavior behavior;

    public BasicObjectVertex(
        Coordinate position, String name, Pattern action,
        Collection<WebRequirement> requirements
    ) {
        super(position, name, action, requirements);
    }

    public BasicObjectVertex(
        Coordinate position, Pattern name, Pattern action,
        Collection<WebRequirement> requirements
    ) {
        super(position, name, action, requirements);
    }

    public BasicObjectVertex(
        int x, int y, int plane, String name, Pattern action,
        Collection<WebRequirement> requirements
    ) {
        super(new Coordinate(x, y, plane), name, action, requirements);
    }

    public BasicObjectVertex(
        int x, int y, int plane, Pattern name, Pattern action,
        Collection<WebRequirement> requirements
    ) {
        super(new Coordinate(x, y, plane), name, action, requirements);
    }

    public BasicObjectVertex(
        Coordinate position, String name, String action,
        Collection<WebRequirement> requirements
    ) {
        super(position, name, action, requirements);
    }

    public BasicObjectVertex(
        int x, int y, int plane, String name, String action,
        Collection<WebRequirement> requirements
    ) {
        super(new Coordinate(x, y, plane), name, action, requirements);
    }

    public BasicObjectVertex(
        int x, int y, int plane, String name, String action,
        Collection<WebRequirement> requirements,
        Collection<WebRequirement> blockingConditions
    ) {
        super(new Coordinate(x, y, plane), name, action, requirements, blockingConditions);
    }

    public BasicObjectVertex(
        Coordinate position, Collection<WebRequirement> requirements,
        Collection<WebRequirement> restrictions, int protocol,
        ObjectInput stream
    ) {
        super(position, requirements, restrictions, protocol, stream);
    }

    @Override
    public String toString() {
        Coordinate position = getPosition();
        return new StringJoiner(", ", "BasicObjectVertex(", ")").add(target.pattern())
            .add(action.pattern())
            .add(String.valueOf(position.getX()))
            .add(String.valueOf(position.getY()))
            .add(String.valueOf(position.getPlane()))
            .add(behavior().name())
            .toString();
    }

    @Override
    public int getOpcode() {
        return 1;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean serialize(ObjectOutput stream) {
        Pattern pattern = getTargetPattern();
        stream.writeUTF(pattern.pattern());
        stream.writeInt(pattern.flags());
        pattern = getActionPattern();
        stream.writeUTF(pattern.pattern());
        stream.writeInt(pattern.flags());
        stream.writeByte(behavior().opcode);
        return true;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean deserialize(int protocol, ObjectInput stream) {
        if (protocol >= 6) {
            target = Pattern.compile(stream.readUTF(), stream.readInt());
            action = Pattern.compile(stream.readUTF(), stream.readInt());
            if (protocol >= 13) {
                behavior = Behavior.opcode(stream.readByte());
            }
        } else if (protocol >= 4) {
            target = Regex.getPatternForExactString(stream.readUTF());
            action = Regex.getPatternForExactString(stream.readUTF());
        } else {
            StringBuilder nameStr = new StringBuilder();
            short length = stream.readShort();
            for (int index = 0; index < length; ++index) {
                nameStr.append(stream.readChar());
            }
            StringBuilder actionStr = new StringBuilder();
            length = stream.readShort();
            for (int index = 0; index < length; ++index) {
                actionStr.append(stream.readChar());
            }
            target = Regex.getPatternForExactString(nameStr.toString());
            action = Regex.getPatternForExactString(actionStr.toString());
        }
        return true;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder builder = new HashCodeBuilder();
        builder.append(getPosition());
        builder.append(getTargetPattern().pattern());
        builder.append(getActionPattern().pattern());
        return builder.toHashCode();
    }

    @Override
    public boolean step() {
        if (Bank.isOpen()) {
            Bank.close();
        }
        final GameObject object = getObject();
        if (object != null) {
            boolean visible = object.getVisibility() >= 25;
            if (!visible) {
                visible = Camera.turnTo(object);
            }
            boolean reclick = visible && !object.interact(action, target);
            if (visible && (!reclick || Camera.turnTo(object) && object.interact(action, target))) {
                final Player avatar = Players.getLocal();
                if (avatar == null) {
                    return false;
                }
                final Coordinate start_base = Scene.getBase();
                final Coordinate start_position = avatar.getPosition(start_base);
                if (behavior() == Behavior.UNDEFINED) {
                    log.debug("No behavior is defined for {}", this);
                    return start_position != null && Execution.delayWhile(() -> {
                        if (object.isValid() || avatar.isMoving()) {
                            return Objects.equals(avatar.getPosition(), start_position)
                                || start_position.isLoaded(start_base)
                                //TODO performance...
                                && position.getReachableCoordinates().contains(start_position);
                        }
                        return false;
                    }, 3000, 4800);
                } else if (behavior() == Behavior.DESPAWNS) {
                    Coordinate object_position = object.getPosition(start_base);
                    return Execution.delayUntil(
                        () -> GameObjects.newQuery().on(object_position).names(target)
                            .actions(action).results().isEmpty(), 3000, 4800);
                } else if (behavior() == Behavior.PASS_THROUGH) {
                    Execution.delayUntil(() -> avatar.isMoving() || avatar.getAnimationId() != -1,
                        1800, 2400
                    );
                    return Execution.delayWhile(
                        () -> avatar.isMoving() || avatar.getAnimationId() != -1,
                        () -> avatar.isMoving() || avatar.getAnimationId() != -1,
                        1800, 2400
                    );
                } else if (behavior() == Behavior.CHANGES_REGION) {
                    return start_position != null
                        &&
                        Execution.delayWhile(() -> start_position.isLoaded(start_base), 3000, 4800);
                }
            }
        }
        return false;
    }

    public BasicObjectVertex behavior(Behavior behavior) {
        this.behavior = behavior;
        return this;
    }

    public Behavior behavior() {
        if (behavior == null) {
            behavior = Behavior.UNDEFINED;
        }
        return behavior;
    }

    public BasicObjectVertex type(GameObject.Type type) {
        this.type = type;
        return this;
    }

    public GameObject.Type type() {
        return type;
    }

    public enum Behavior {
        /**
         * Results in using a generic behavior that works in most cases but doesn't work well for much
         */
        UNDEFINED(0),
        /**
         * A behavior that expects the object to despawn, as in disappear, unload, etc.
         */
        DESPAWNS(1),
        /**
         * A behavior that indicates that the player passes through, crosses, or otherwise moves past the object.
         */
        PASS_THROUGH(2),
        /**
         * A behavior that expects the object to cause the current region to change upon successful traversal
         */
        CHANGES_REGION(3),
        @Deprecated
        DISAPPEARING(),
        @Deprecated
        MAP_ENTRANCE();
        private final int opcode;

        Behavior() {
            this(-1);
        }

        Behavior(int opcode) {
            this.opcode = opcode;
        }

        public static Behavior opcode(int opcode) {
            for (Behavior behavior : values()) {
                if (behavior.opcode >= 0 && behavior.opcode == opcode) {
                    return behavior;
                }
            }
            return UNDEFINED;
        }
    }
}
