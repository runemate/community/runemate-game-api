package com.runemate.game.api.hybrid.util.shapes;

import com.runemate.game.api.hybrid.local.hud.*;
import java.awt.*;
import java.util.List;
import java.util.*;

public class Line {
    private final Point start, end;

    public Line(Point start, Point end) {
        this.start = start;
        this.end = end;
    }

    public Point getStart() {
        return start;
    }

    public Point getEnd() {
        return end;
    }

    public double getLength() {
        return Math.sqrt((start.x - end.x) ^ 2 + (start.y - end.y) ^ 2);
    }

    public List<InteractablePoint> getPoints() {
        int currentX = start.x;
        final int desiredX = end.x;
        int deltaX = desiredX - currentX;
        if (deltaX < 0) {
            deltaX = -deltaX;
        }
        int currentY = start.y;
        final int desiredY = end.y;
        int deltaY = desiredY - currentY;
        if (deltaY < 0) {
            deltaY = -deltaY;
        }
        //Controls whether we increment or decrement x every loop until we reach its desired value
        final int changeX = currentX < desiredX ? 1 : -1;
        //Controls whether we increment or decrement y every loop until we reach its desired value
        final int changeY = currentY < desiredY ? 1 : -1;
        //Controls the direction and distance that we'll search for the point within
        int difference = deltaX - deltaY;
        final List<InteractablePoint> line = new ArrayList<>(deltaX + deltaY);
        while (true) {
            line.add(new InteractablePoint(currentX, currentY));
            if (currentX == desiredX && currentY == desiredY) {
                break;
            }
            final int difference_times_two = difference << 1;
            if (difference_times_two > -deltaY) {
                difference -= deltaY;
                currentX += changeX;
            }
            if (difference_times_two < deltaX) {
                difference += deltaX;
                currentY += changeY;
            }
        }
        return line;
    }
}
