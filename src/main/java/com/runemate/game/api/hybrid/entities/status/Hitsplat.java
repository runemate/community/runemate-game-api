package com.runemate.game.api.hybrid.entities.status;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.util.*;
import java.util.*;
import lombok.*;

@Value
public class Hitsplat implements Validatable {

    int id;
    int damage;
    int endCycle;
    int secondaryId;
    int secondaryDamage;

    @NonNull
    public Classification getClassification() {
        return Arrays.stream(Classification.values())
            .filter(classification -> classification.id == id)
            .findFirst()
            .orElse(Classification.UNCLASSIFIED);
    }

    @Override
    public boolean isValid() {
        return endCycle > RuneScape.getCurrentCycle();
    }

    @Getter
    public enum Classification {

        //Events triggered by (or against) the local player
        BLOCK_ME(12, true),
        DAMAGE_ME(16, true),
        DAMAGE_ME_CYAN(18, true),
        DAMAGE_ME_ORANGE(20, true),
        DAMAGE_ME_YELLOW(22, true),
        DAMAGE_ME_WHITE(24, true),
        DAMAGE_MAX_ME(43, true),
        DAMAGE_MAX_ME_CYAN(44, true),
        DAMAGE_MAX_ME_ORANGE(45, true),
        DAMAGE_MAX_ME_YELLOW(46, true),
        DAMAGE_MAX_ME_WHITE(47, true),
        DAMAGE_ME_POISE(53, true),
        DAMAGE_MAX_ME_POISE(55, true),

        //Events triggered by other entities
        BLOCK_OTHER(13, false),
        DAMAGE_OTHER(17, false),
        DAMAGE_OTHER_CYAN(19, false),
        DAMAGE_OTHER_ORANGE(21, false),
        DAMAGE_OTHER_YELLOW(23, false),
        DAMAGE_OTHER_WHITE(25, false),
        DAMAGE_OTHER_POISE(54, false),

        //Remaining
        POISON(65),
        DISEASE(4),
        VENOM(5),
        HEALING(6),
        CYAN_UP(11),
        CYAN_DOWN(15),
        PRAYER_DRAIN(60),
        BLEED(67),
        SANITY_DRAIN(71),
        SANITY_RESTORE(72),
        DOOM(73),
        BURN(74),
        FREEZE(75),
        CORRUPTION(0),

        UNCLASSIFIED()
        ;

        private final int id;
        private final boolean players;

        Classification(int id, boolean players) {
            this.id = id;
            this.players = players;
        }

        Classification(int id) {
            this(id, false);
        }

        Classification() {
            this(-1);
        }

    }

}
