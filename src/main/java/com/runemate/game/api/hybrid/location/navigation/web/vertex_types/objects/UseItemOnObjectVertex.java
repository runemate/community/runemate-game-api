package com.runemate.game.api.hybrid.location.navigation.web.vertex_types.objects;

import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.location.navigation.web.requirements.*;
import com.runemate.game.api.hybrid.location.navigation.web.vertex_types.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.script.*;
import java.io.*;
import java.util.*;
import java.util.regex.*;
import lombok.*;
import org.apache.commons.lang3.builder.*;

/**
 * A vertex for using an inventory item on an object
 */
@Getter
public class UseItemOnObjectVertex extends ObjectVertex implements SerializableVertex {
    private String item;

    public UseItemOnObjectVertex(
        final int x, final int y, final int plane, final String name,
        final String item,
        final Collection<WebRequirement> requirements
    ) {
        this(new Coordinate(x, y, plane), name, item, requirements);
    }

    public UseItemOnObjectVertex(
        final Coordinate position, final String name, final String item,
        final Collection<WebRequirement> requirements
    ) {
        super(position, Regex.getPatternForContainsString(name), "Use", requirements);
        this.item = item;
    }

    public UseItemOnObjectVertex(
        Coordinate position, Collection<WebRequirement> requirements,
        Collection<WebRequirement> forbiddingRequirements, int protocol,
        ObjectInput stream
    ) {
        super(position, requirements, forbiddingRequirements, protocol, stream);
    }

    @Override
    public boolean step() {
        final GameObject object = getObject();
        if (object != null && (object.isVisible() || Camera.turnTo(object))) {
            SpriteItem item = Inventory.getSelectedItem();
            if (item != null && !Objects.equals(Optional.ofNullable(item.getDefinition()).map(ItemDefinition::getName).orElse(null), getItem()) && item.click()) {
                Execution.delayWhile(() -> Inventory.getSelectedItem() != null, 1500, 2500);
                item = Inventory.getSelectedItem();
            }
            if (item == null) {
                item = Inventory.newQuery().names(getItem()).results().limit(4).random();
                if (item != null && item.interact("Use")) {
                    Execution.delayUntil(() -> Inventory.getSelectedItem() != null, 1500, 2500);
                }
            }
            if (item != null && item.equals(Inventory.getSelectedItem()) &&
                object.interact(getActionPattern(), getTargetPattern())) {
                final Player avatar = Players.getLocal();
                if (avatar == null) {
                    return false;
                }
                final Coordinate start_base = Scene.getBase();
                final Coordinate start_position = avatar.getPosition(start_base);
                return start_position != null && Execution.delayWhile(() -> {
                    if (object.isValid()) {
                        Coordinate position = avatar.getPosition();
                        if (Objects.equals(position, start_position)) {
                            return true;
                        } else if (!start_position.isLoaded(start_base)) {
                            return false;
                        } else {
                            return position != null && position.getReachableCoordinates().contains(start_position);
                        }
                    }
                    return false;
                }, 3000, 4800);
            }
        }
        return false;
    }

    @Override
    public String toString() {
        Coordinate position = getPosition();
        return "UseItemOnObjectVertex(" + target + ", " + action + " -> " + target + ", " +
            position.getX() + ", " + position.getY() + ", " + position.getPlane() + ')';
    }

    @Override
    public int getOpcode() {
        return 15;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean serialize(ObjectOutput stream) {
        stream.writeUTF(getTargetPattern().pattern());
        stream.writeUTF(getActionPattern().pattern());
        stream.writeUTF(getItem());
        return true;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean deserialize(int protocol, ObjectInput stream) {
        target = Pattern.compile(stream.readUTF());
        action = Pattern.compile(stream.readUTF());
        item = stream.readUTF();
        return true;
    }

    @Override
    public int hashCode() {
        HashCodeBuilder builder = new HashCodeBuilder();
        builder.append(getPosition());
        builder.append(getTargetPattern().pattern());
        builder.append(getActionPattern().pattern());
        builder.append(getItem());
        return builder.toHashCode();
    }
}
