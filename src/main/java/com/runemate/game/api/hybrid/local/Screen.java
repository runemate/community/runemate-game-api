package com.runemate.game.api.hybrid.local;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.local.hud.InteractablePoint;
import com.runemate.game.api.hybrid.local.hud.InteractableRectangle;

import javax.annotation.Nullable;
import java.awt.image.BufferedImage;

public final class Screen {

    private Screen() {
    }

    /**
     * Gets the location of the game's canvas on the screen.
     */
    @Nullable
    public static InteractablePoint getLocation() {
        int[] dimensions = OpenClient.getPosition();
        if (dimensions == null || dimensions.length != 2) {
            return null;
        }
        return new InteractablePoint(dimensions[0], dimensions[1]);
    }


    @Nullable
    public static InteractableRectangle getBounds() {
        int[] dimensions = OpenClient.getDimensions();
        if (dimensions == null || dimensions.length != 2) {
            return null;
        }
        return new InteractableRectangle(0, 0, dimensions[0], dimensions[1]);
    }

    /**
     * Captures the contents of RuneScape's canvas. This does not work as expected when using an OpenGL graphics mode.
     */

    @Nullable
    public static BufferedImage capture() {
        int producerWidth = OpenClient.getGraphicsProducerWidth();
        if (producerWidth <= 0) {
            return null;
        }
        int producerHeight = OpenClient.getGraphicsProducerHeight();
        if (producerHeight <= 0) {
            return null;
        }
        int[] pixels;
        int[] lastFrame = OpenClient.getLastFrame();
        if (lastFrame != null) {
            pixels = lastFrame;
        }
        else {
            pixels = OpenClient.getGraphicsProducerRgbBuffer();
            if (pixels == null) {
                return null;
            }
        }
        BufferedImage captured = new BufferedImage(producerWidth, producerHeight, BufferedImage.TYPE_INT_RGB);
        for (int x = 0; x < producerWidth; x++) {
            for (int y = 0; y < producerHeight; y++) {
                captured.setRGB(x, y, pixels[x + (y * producerWidth)]);
            }
        }
        return captured;
    }

    public static boolean isFixedMode() {
        InteractableRectangle bounds = Screen.getBounds();
        if (bounds != null) {
            return bounds.width == 765 && bounds.height == 503;
        }
        return false;
    }
}