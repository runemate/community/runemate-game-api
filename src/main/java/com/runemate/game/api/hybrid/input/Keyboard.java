package com.runemate.game.api.hybrid.input;

import com.runemate.client.game.open.*;
import com.runemate.game.api.client.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.util.calculations.*;
import com.runemate.game.api.script.*;
import java.awt.event.*;
import java.rmi.*;
import lombok.*;
import lombok.extern.log4j.*;

@Log4j2
public final class Keyboard {

    public static final int[] KEYCODE_MAPPING = { 
        -1, -1, -1, -1, -1, -1, -1, -1, 85, 80, 84, -1, 91, -1, -1, -1, 81, 82, 86, -1, -1, -1, -1, -1, -1, -1, -1, 13, -1, -1, -1, -1, 
        83, 104, 105, 103, 102, 96, 98, 97, 99, -1, -1, -1, -1, -1, -1, -1, 25, 16, 17, 18, 19, 20, 21, 22, 23, 24, -1, -1, -1, -1, -1, 
        -1, -1, 48, 68, 66, 50, 34, 51, 52, 53, 39, 54, 55, 56, 70, 69, 40, 41, 32, 35, 49, 36, 38, 67, 33, 65, 37, 64, -1, -1, -1, -1, 
        -1, 228, 231, 227, 233, 224, 219, 225, 230, 226, 232, 89, 87, -1, 88, 229, 90, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, -1, -1, -1, 
        101, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 100, -1, -1, -1, 
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 
        -1, -1, -1, -1, -1, -1, -1, -1, -1, -1
    };

    static {
        if (System.getProperty("java.vendor").toLowerCase().contains("microsoft")) {
            KEYCODE_MAPPING[186] = 57;
            KEYCODE_MAPPING[187] = 27;
            KEYCODE_MAPPING[188] = 71;
            KEYCODE_MAPPING[189] = 26;
            KEYCODE_MAPPING[190] = 72;
            KEYCODE_MAPPING[191] = 73;
            KEYCODE_MAPPING[192] = 58;
            KEYCODE_MAPPING[219] = 42;
            KEYCODE_MAPPING[220] = 74;
            KEYCODE_MAPPING[221] = 43;
            KEYCODE_MAPPING[222] = 59;
            KEYCODE_MAPPING[223] = 28;
        } else {
            KEYCODE_MAPPING[44] = 71;
            KEYCODE_MAPPING[45] = 26;
            KEYCODE_MAPPING[46] = 72;
            KEYCODE_MAPPING[47] = 73;
            KEYCODE_MAPPING[59] = 57;
            KEYCODE_MAPPING[61] = 27;
            KEYCODE_MAPPING[91] = 42;
            KEYCODE_MAPPING[92] = 74;
            KEYCODE_MAPPING[93] = 43;
            KEYCODE_MAPPING[192] = 28;
            KEYCODE_MAPPING[222] = 58;
            KEYCODE_MAPPING[520] = 59;
        }
    }


    private Keyboard() {
    }

    public static boolean isPressed(final int keycode) {
        int mapped = keycode;
        if (mapped >= 0 && mapped < KEYCODE_MAPPING.length) {
            mapped = KEYCODE_MAPPING[mapped];
            if ((mapped & 0b10000000) != 0) {
                mapped = -1;
            }
        } else {
            mapped = -1;
        }
        if (mapped == -1) {
            return false;
        }
        final boolean[] pressed = OpenInput.getPressedKeys();
        return pressed != null && pressed[mapped];
    }

    private static int getModifiers() {
        int modifiers = 0;
        if (isPressed(KeyEvent.VK_SHIFT)) {
            modifiers |= InputEvent.SHIFT_MASK;
        }
        if (isPressed(KeyEvent.VK_CONTROL)) {
            modifiers |= InputEvent.CTRL_MASK;
        }
        if (isPressed(KeyEvent.VK_ALT)) {
            modifiers |= InputEvent.ALT_MASK;
        }
        if (isPressed(KeyEvent.VK_META)) {
            modifiers |= InputEvent.META_MASK;
        }
        if (isPressed(KeyEvent.VK_ALT_GRAPH)) {
            modifiers |= InputEvent.ALT_GRAPH_MASK;
        }
        return modifiers;
    }

    @SneakyThrows(RemoteException.class)
    public static boolean pressKey(Key key) {
        OpenInput.pressKey(key.keycode,
            key.keychar,
            key.default_location,
            getModifiers(),
            key.rawCode,
            key.primaryLevelUnicode,
            isWindows() ? key.scancode : 0
        );
        if (!key.isModifierKey() && !key.isActionKey()) {
            OpenInput.typeKey(key.keychar, getModifiers(), key.rawCode, key.primaryLevelUnicode, isWindows() ? key.scancode : 0);
        }
        return true;
    }

    public static boolean pressKey(int keycode) {
        final Key key = Key.getKey(keycode);
        return key != null && pressKey(key);
    }

    public static boolean releaseKey(Key key) {
        try {
            OpenInput.releaseKey(key.keycode,
                key.keychar,
                key.default_location,
                getModifiers(),
                key.rawCode,
                key.primaryLevelUnicode,
                isWindows() ? key.scancode : 0
            );
        } catch (Throwable t) {
            if (t instanceof ConnectException) {
            } else {
                log.warn("Failed to press key {}", key, t);
            }
            return false;
        }
        return true;
    }

    public static boolean releaseKey(int keycode) {
        final Key key = Key.getKey(keycode);
        return key != null && releaseKey(key);
    }

    public static boolean typeKey(Key key, int delay) {
        return pressKey(key) && Execution.delay(delay) && releaseKey(key);
    }

    public static boolean typeKey(Key key) {
        return typeKey(key, generateKeyPressLength());
    }

    public static boolean typeKey(int keycode) {
        return typeKey(keycode, generateKeyPressLength());
    }

    public static boolean typeKey(int keycode, int delay) {
        final Key key = Key.getKey(keycode);
        return key != null && typeKey(key, delay);
    }

    private static int generateKeyPressLength() {
        //TODO more human like
        return (int) Random.nextGaussian(20, 200, 130);
    }

    /**
     * Maps the specified character to a keycode all then types it
     *
     * @param character The character to be mapped all typed
     * @return Whether or not the character was successfully typed
     */
    public static boolean typeKey(final char character, int delay) {
        final Key key = Key.getKey(character);
        return key != null && typeKey(key, delay);
    }

    public static boolean typeKey(final char character) {
        return typeKey(character, generateKeyPressLength());
    }

    /**
     * Types a single character passed in as a String object.
     */
    public static boolean typeKey(final String character) {
        if (character == null || character.length() != 1) {
            throw new IllegalArgumentException("The provided string must be non-null and have a length of 1.");
        }
        return typeKey(character.charAt(0));
    }

    /**
     * Types the string into the focused widget.
     *
     * @param text       The String to be typed
     * @param pressEnter Whether or not to press enter after typing the string
     * @return Whether or not typing was successful
     */
    public static boolean type(final String text, final boolean pressEnter) {
        if (text == null) {
            return false;
        }
        boolean success = true;
        for (final char c : text.toCharArray()) {
            if (!typeKey(c)) {
                //log.warn("[Error] Failed to type key " + c);
                success = false;
            }
        }
        if (pressEnter && !typeKey(KeyEvent.VK_ENTER)) {
            success = false;
        }
        return success;
    }

    public static boolean isInputAllowed() {
        return !OpenInput.isKeyboardInputBlocked();
    }

    private static boolean isWindows() {
        return System.getProperty("os.name").startsWith("Windows");
    }

    enum Key {
        //TODO make some players use left side shift, ctrl, etc and some use right.
        //TODO make some players use numpad and some use others. For numbers on the numpad, they have unique keycodes.
        KEY_BACKSPACE(8, (char) KeyEvent.VK_BACK_SPACE, KeyEvent.KEY_LOCATION_STANDARD, 22, 8, 14),
        KEY_TAB(9, (char) KeyEvent.VK_TAB, KeyEvent.KEY_LOCATION_STANDARD, 0, 0, 0),//why is this practically blank at the end
        KEY_ENTER(10, (char) KeyEvent.VK_ENTER, KeyEvent.KEY_LOCATION_STANDARD, 36, 13, 28),
        KEY_SHIFT(16, KeyEvent.CHAR_UNDEFINED, KeyEvent.KEY_LOCATION_LEFT, 50, 0, 42),
        KEY_CTRL(17, KeyEvent.CHAR_UNDEFINED, KeyEvent.KEY_LOCATION_LEFT, 37, 0, 29),
        KEY_ALT(18, KeyEvent.CHAR_UNDEFINED, KeyEvent.KEY_LOCATION_LEFT, 64, 0, 56),
        KEY_CAPS_LOCK(20, KeyEvent.CHAR_UNDEFINED, KeyEvent.KEY_LOCATION_STANDARD, 66, 0, 58),
        KEY_ESCAPE(27, (char) KeyEvent.VK_ESCAPE, KeyEvent.KEY_LOCATION_STANDARD, 9, 27, 1),
        KEY_SPACE(32, ' ', KeyEvent.KEY_LOCATION_STANDARD, 65, 32, 57),
        KEY_COMMA(44, ',', KeyEvent.KEY_LOCATION_STANDARD, 59, 44, 51),
        KEY_LESS_THAN(44, '<', KeyEvent.KEY_LOCATION_STANDARD, 59, 44, 51),
        KEY_LEFT(37, KeyEvent.CHAR_UNDEFINED, KeyEvent.KEY_LOCATION_STANDARD, 113, 0, 75),
        KEY_UP(38, KeyEvent.CHAR_UNDEFINED, KeyEvent.KEY_LOCATION_STANDARD, 111, 0, 72),
        KEY_RIGHT(39, KeyEvent.CHAR_UNDEFINED, KeyEvent.KEY_LOCATION_STANDARD, 114, 0, 77),
        KEY_DOWN(40, KeyEvent.CHAR_UNDEFINED, KeyEvent.KEY_LOCATION_STANDARD, 116, 0, 80),
        KEY_MINUS(45, '-', KeyEvent.KEY_LOCATION_STANDARD, 20, 45, 12),
        KEY_UNDERSCORE(45, '_', KeyEvent.KEY_LOCATION_STANDARD, 20, 45, 12),
        KEY_PERIOD(46, '.', KeyEvent.KEY_LOCATION_STANDARD, 60, 46, 52),
        KEY_GREATER_THAN(46, '>', KeyEvent.KEY_LOCATION_STANDARD, 60, 46, 52),
        KEY_SLASH(47, '/', KeyEvent.KEY_LOCATION_STANDARD, 61, 47, 53),
        KEY_QUESTION_MARK(47, '?', KeyEvent.KEY_LOCATION_STANDARD, 61, 47, 53),
        KEY_0(48, '0', KeyEvent.KEY_LOCATION_STANDARD, 19, 48, 11),
        KEY_CLOSE_PARENTHESIS(48, ')', KeyEvent.KEY_LOCATION_STANDARD, 19, 48, 11),
        KEY_1(49, '1', KeyEvent.KEY_LOCATION_STANDARD, 10, 49, 2),
        KEY_EXCLAMATION_POINT(49, '!', KeyEvent.KEY_LOCATION_STANDARD, 10, 49, 2),
        KEY_2(50, '2', KeyEvent.KEY_LOCATION_STANDARD, 11, 50, 3),
        KEY_AT_SIGN(50, '@', KeyEvent.KEY_LOCATION_STANDARD, 11, 50, 3),
        KEY_3(51, '3', KeyEvent.KEY_LOCATION_STANDARD, 12, 51, 4),
        KEY_POUND_SIGN(51, '#', KeyEvent.KEY_LOCATION_STANDARD, 12, 51, 4),
        KEY_4(52, '4', KeyEvent.KEY_LOCATION_STANDARD, 13, 52, 5),
        KEY_DOLLAR_SIGN(52, '$', KeyEvent.KEY_LOCATION_STANDARD, 13, 52, 5),
        KEY_5(53, '5', KeyEvent.KEY_LOCATION_STANDARD, 14, 53, 6),
        KEY_PERCENT_SIGN(53, '%', KeyEvent.KEY_LOCATION_STANDARD, 14, 53, 6),
        KEY_6(54, '6', KeyEvent.KEY_LOCATION_STANDARD, 15, 54, 7),
        KEY_CARET(54, '^', KeyEvent.KEY_LOCATION_STANDARD, 15, 54, 7),
        KEY_7(55, '7', KeyEvent.KEY_LOCATION_STANDARD, 16, 55, 8),
        KEY_AMPERSAND(55, '&', KeyEvent.KEY_LOCATION_STANDARD, 16, 55, 8),
        KEY_8(56, '8', KeyEvent.KEY_LOCATION_STANDARD, 17, 56, 9),
        KEY_ASTERISK(56, '*', KeyEvent.KEY_LOCATION_STANDARD, 17, 56, 9),
        KEY_9(57, '9', KeyEvent.KEY_LOCATION_STANDARD, 18, 57, 10),
        KEY_OPEN_PARENTHESIS(57, '(', KeyEvent.KEY_LOCATION_STANDARD, 18, 57, 10),
        KEY_SEMICOLON(59, ';', KeyEvent.KEY_LOCATION_STANDARD, 47, 59, 39),
        KEY_COLON(59, ':', KeyEvent.KEY_LOCATION_STANDARD, 47, 59, 39),
        KEY_EQUALS(61, '=', KeyEvent.KEY_LOCATION_STANDARD, 21, 61, 13),
        KEY_ADDITION(61, '+', KeyEvent.KEY_LOCATION_STANDARD, 21, 61, 13),
        KEY_A(65, 'a', KeyEvent.KEY_LOCATION_STANDARD, 38, 97, 30),
        KEY_UPPER_A(65, 'A', KeyEvent.KEY_LOCATION_STANDARD, 38, 97, 30),
        KEY_B(66, 'b', KeyEvent.KEY_LOCATION_STANDARD, 56, 98, 48),
        KEY_UPPER_B(66, 'B', KeyEvent.KEY_LOCATION_STANDARD, 56, 98, 48),
        KEY_C(67, 'c', KeyEvent.KEY_LOCATION_STANDARD, 54, 99, 46),
        KEY_UPPER_C(67, 'C', KeyEvent.KEY_LOCATION_STANDARD, 54, 99, 46),
        KEY_D(68, 'd', KeyEvent.KEY_LOCATION_STANDARD, 40, 100, 32),
        KEY_UPPER_D(68, 'D', KeyEvent.KEY_LOCATION_STANDARD, 40, 100, 32),
        KEY_E(69, 'e', KeyEvent.KEY_LOCATION_STANDARD, 26, 101, 18),
        KEY_UPPER_E(69, 'E', KeyEvent.KEY_LOCATION_STANDARD, 26, 101, 18),
        KEY_F(70, 'f', KeyEvent.KEY_LOCATION_STANDARD, 41, 102, 33),
        KEY_UPPER_F(70, 'F', KeyEvent.KEY_LOCATION_STANDARD, 41, 102, 33),
        KEY_G(71, 'g', KeyEvent.KEY_LOCATION_STANDARD, 42, 103, 34),
        KEY_UPPER_G(71, 'G', KeyEvent.KEY_LOCATION_STANDARD, 42, 103, 34),
        KEY_H(72, 'h', KeyEvent.KEY_LOCATION_STANDARD, 43, 104, 35),
        KEY_UPPER_H(72, 'H', KeyEvent.KEY_LOCATION_STANDARD, 43, 104, 35),
        KEY_I(73, 'i', KeyEvent.KEY_LOCATION_STANDARD, 31, 105, 23),
        KEY_UPPER_I(73, 'I', KeyEvent.KEY_LOCATION_STANDARD, 31, 105, 23),
        KEY_J(74, 'j', KeyEvent.KEY_LOCATION_STANDARD, 44, 106, 36),
        KEY_UPPER_J(74, 'J', KeyEvent.KEY_LOCATION_STANDARD, 44, 106, 36),
        KEY_K(75, 'k', KeyEvent.KEY_LOCATION_STANDARD, 45, 107, 37),
        KEY_UPPER_K(75, 'K', KeyEvent.KEY_LOCATION_STANDARD, 45, 107, 37),
        KEY_L(76, 'l', KeyEvent.KEY_LOCATION_STANDARD, 46, 108, 38),
        KEY_UPPER_L(76, 'L', KeyEvent.KEY_LOCATION_STANDARD, 46, 108, 38),
        KEY_M(77, 'm', KeyEvent.KEY_LOCATION_STANDARD, 58, 109, 50),
        KEY_UPPER_M(77, 'M', KeyEvent.KEY_LOCATION_STANDARD, 58, 109, 50),
        KEY_N(78, 'n', KeyEvent.KEY_LOCATION_STANDARD, 57, 110, 49),
        KEY_UPPER_N(78, 'N', KeyEvent.KEY_LOCATION_STANDARD, 57, 110, 49),
        KEY_O(79, 'o', KeyEvent.KEY_LOCATION_STANDARD, 32, 111, 24),
        KEY_UPPER_O(79, 'O', KeyEvent.KEY_LOCATION_STANDARD, 32, 111, 24),
        KEY_P(80, 'p', KeyEvent.KEY_LOCATION_STANDARD, 33, 112, 25),
        KEY_UPPER_P(80, 'P', KeyEvent.KEY_LOCATION_STANDARD, 33, 112, 25),
        KEY_Q(81, 'q', KeyEvent.KEY_LOCATION_STANDARD, 24, 113, 16),
        KEY_UPPER_Q(81, 'Q', KeyEvent.KEY_LOCATION_STANDARD, 24, 113, 16),
        KEY_R(82, 'r', KeyEvent.KEY_LOCATION_STANDARD, 27, 114, 19),
        KEY_UPPER_R(82, 'R', KeyEvent.KEY_LOCATION_STANDARD, 27, 114, 19),
        KEY_S(83, 's', KeyEvent.KEY_LOCATION_STANDARD, 39, 115, 31),
        KEY_UPPER_S(83, 'S', KeyEvent.KEY_LOCATION_STANDARD, 39, 115, 31),
        KEY_T(84, 't', KeyEvent.KEY_LOCATION_STANDARD, 28, 116, 20),
        KEY_UPPER_T(84, 'T', KeyEvent.KEY_LOCATION_STANDARD, 28, 116, 20),
        KEY_U(85, 'u', KeyEvent.KEY_LOCATION_STANDARD, 30, 117, 22),
        KEY_UPPER_U(85, 'U', KeyEvent.KEY_LOCATION_STANDARD, 30, 117, 22),
        KEY_V(86, 'v', KeyEvent.KEY_LOCATION_STANDARD, 55, 118, 47),
        KEY_UPPER_V(86, 'V', KeyEvent.KEY_LOCATION_STANDARD, 55, 118, 47),
        KEY_W(87, 'w', KeyEvent.KEY_LOCATION_STANDARD, 25, 119, 17),
        KEY_UPPER_W(87, 'W', KeyEvent.KEY_LOCATION_STANDARD, 25, 119, 17),
        KEY_X(88, 'x', KeyEvent.KEY_LOCATION_STANDARD, 53, 120, 45),
        KEY_UPPER_X(88, 'X', KeyEvent.KEY_LOCATION_STANDARD, 53, 120, 45),
        KEY_Y(89, 'y', KeyEvent.KEY_LOCATION_STANDARD, 29, 121, 21),
        KEY_UPPER_Y(89, 'Y', KeyEvent.KEY_LOCATION_STANDARD, 29, 121, 21),
        KEY_Z(90, 'z', KeyEvent.KEY_LOCATION_STANDARD, 52, 122, 44),
        KEY_UPPER_Z(90, 'Z', KeyEvent.KEY_LOCATION_STANDARD, 52, 122, 44),
        KEY_OPEN_BRACKET(91, '[', KeyEvent.KEY_LOCATION_STANDARD, 34, 91, 26),
        KEY_OPEN_BRACE(91, '{', KeyEvent.KEY_LOCATION_STANDARD, 34, 91, 26),
        KEY_BACK_SLASH(92, '\\', KeyEvent.KEY_LOCATION_STANDARD, 51, 92, 43),
        KEY_PIPE(92, '|', KeyEvent.KEY_LOCATION_STANDARD, 51, 92, 43),
        KEY_CLOSE_BRACKET(93, ']', KeyEvent.KEY_LOCATION_STANDARD, 35, 93, 27),
        KEY_CLOSE_BRACE(93, '}', KeyEvent.KEY_LOCATION_STANDARD, 35, 93, 27),
        KEY_F1(112, KeyEvent.CHAR_UNDEFINED, KeyEvent.KEY_LOCATION_STANDARD, 67, 0, 59),
        KEY_F2(113, KeyEvent.CHAR_UNDEFINED, KeyEvent.KEY_LOCATION_STANDARD, 68, 0, 60),
        KEY_F3(114, KeyEvent.CHAR_UNDEFINED, KeyEvent.KEY_LOCATION_STANDARD, 69, 0, 61),
        KEY_F4(115, KeyEvent.CHAR_UNDEFINED, KeyEvent.KEY_LOCATION_STANDARD, 70, 0, 62),
        KEY_F5(116, KeyEvent.CHAR_UNDEFINED, KeyEvent.KEY_LOCATION_STANDARD, 71, 0, 63),
        KEY_F6(117, KeyEvent.CHAR_UNDEFINED, KeyEvent.KEY_LOCATION_STANDARD, 72, 0, 64),
        KEY_F7(118, KeyEvent.CHAR_UNDEFINED, KeyEvent.KEY_LOCATION_STANDARD, 73, 0, 65),
        KEY_F8(119, KeyEvent.CHAR_UNDEFINED, KeyEvent.KEY_LOCATION_STANDARD, 74, 0, 66),
        KEY_F9(120, KeyEvent.CHAR_UNDEFINED, KeyEvent.KEY_LOCATION_STANDARD, 75, 0, 67),
        KEY_F10(121, KeyEvent.CHAR_UNDEFINED, KeyEvent.KEY_LOCATION_STANDARD, 76, 0, 68),
        KEY_F11(122, KeyEvent.CHAR_UNDEFINED, KeyEvent.KEY_LOCATION_STANDARD, 95, 0, 69),
        KEY_F12(123, KeyEvent.CHAR_UNDEFINED, KeyEvent.KEY_LOCATION_STANDARD, 96, 0, 70),
        KEY_DELETE(127, (char) KeyEvent.VK_DELETE, KeyEvent.KEY_LOCATION_STANDARD, 0, 0, 0),//maybe?
        KEY_BACK_QUOTE(192, '`', KeyEvent.KEY_LOCATION_STANDARD, 49, 96, 41),
        KEY_TILDE(192, '~', KeyEvent.KEY_LOCATION_STANDARD, 49, 96, 41),
        KEY_QUOTE(222, '\'', KeyEvent.KEY_LOCATION_STANDARD, 48, 39, 40),
        KEY_DOUBLE_QUOTE(222, '"', KeyEvent.KEY_LOCATION_STANDARD, 48, 39, 40);
        public final int keycode, default_location;
        public final long rawCode, primaryLevelUnicode, scancode;
        public final char keychar;

        Key(int keycode, char keychar, int default_location, long rawCode, long primaryLevelUnicode, long scancode) {
            this.keycode = keycode;
            this.keychar = keychar;
            this.default_location = default_location;
            this.rawCode = rawCode;
            this.primaryLevelUnicode = primaryLevelUnicode;
            this.scancode = scancode;
        }

        public static Key getKey(final int keycode) {
            for (Key key : values()) {
                if (key.keycode == keycode) {
                    return key;
                }
            }
            log.warn("Unable to resolve '{}' - Unmapped keycode (Please report)", keycode);
            return null;
        }

        public static Key getKey(char keychar) {
            for (Key key : values()) {
                if (key.keychar == keychar) {
                    return key;
                }
            }
            log.warn("Unable to resolve '{}' - Unmapped keychar (Please report)", keychar);
            return null;
        }

        public boolean isActionKey() {
            switch (keycode) {
                case KeyEvent.VK_HOME:
                case KeyEvent.VK_END:
                case KeyEvent.VK_PAGE_UP:
                case KeyEvent.VK_PAGE_DOWN:
                case KeyEvent.VK_UP:
                case KeyEvent.VK_DOWN:
                case KeyEvent.VK_LEFT:
                case KeyEvent.VK_RIGHT:
                case KeyEvent.VK_BEGIN:
                case KeyEvent.VK_KP_LEFT:
                case KeyEvent.VK_KP_UP:
                case KeyEvent.VK_KP_RIGHT:
                case KeyEvent.VK_KP_DOWN:
                case KeyEvent.VK_F1:
                case KeyEvent.VK_F2:
                case KeyEvent.VK_F3:
                case KeyEvent.VK_F4:
                case KeyEvent.VK_F5:
                case KeyEvent.VK_F6:
                case KeyEvent.VK_F7:
                case KeyEvent.VK_F8:
                case KeyEvent.VK_F9:
                case KeyEvent.VK_F10:
                case KeyEvent.VK_F11:
                case KeyEvent.VK_F12:
                case KeyEvent.VK_F13:
                case KeyEvent.VK_F14:
                case KeyEvent.VK_F15:
                case KeyEvent.VK_F16:
                case KeyEvent.VK_F17:
                case KeyEvent.VK_F18:
                case KeyEvent.VK_F19:
                case KeyEvent.VK_F20:
                case KeyEvent.VK_F21:
                case KeyEvent.VK_F22:
                case KeyEvent.VK_F23:
                case KeyEvent.VK_F24:
                case KeyEvent.VK_PRINTSCREEN:
                case KeyEvent.VK_SCROLL_LOCK:
                case KeyEvent.VK_CAPS_LOCK:
                case KeyEvent.VK_NUM_LOCK:
                case KeyEvent.VK_PAUSE:
                case KeyEvent.VK_INSERT:
                case KeyEvent.VK_FINAL:
                case KeyEvent.VK_CONVERT:
                case KeyEvent.VK_NONCONVERT:
                case KeyEvent.VK_ACCEPT:
                case KeyEvent.VK_MODECHANGE:
                case KeyEvent.VK_KANA:
                case KeyEvent.VK_KANJI:
                case KeyEvent.VK_ALPHANUMERIC:
                case KeyEvent.VK_KATAKANA:
                case KeyEvent.VK_HIRAGANA:
                case KeyEvent.VK_FULL_WIDTH:
                case KeyEvent.VK_HALF_WIDTH:
                case KeyEvent.VK_ROMAN_CHARACTERS:
                case KeyEvent.VK_ALL_CANDIDATES:
                case KeyEvent.VK_PREVIOUS_CANDIDATE:
                case KeyEvent.VK_CODE_INPUT:
                case KeyEvent.VK_JAPANESE_KATAKANA:
                case KeyEvent.VK_JAPANESE_HIRAGANA:
                case KeyEvent.VK_JAPANESE_ROMAN:
                case KeyEvent.VK_KANA_LOCK:
                case KeyEvent.VK_INPUT_METHOD_ON_OFF:
                case KeyEvent.VK_AGAIN:
                case KeyEvent.VK_UNDO:
                case KeyEvent.VK_COPY:
                case KeyEvent.VK_PASTE:
                case KeyEvent.VK_CUT:
                case KeyEvent.VK_FIND:
                case KeyEvent.VK_PROPS:
                case KeyEvent.VK_STOP:
                case KeyEvent.VK_HELP:
                case KeyEvent.VK_WINDOWS:
                case KeyEvent.VK_CONTEXT_MENU:
                    return true;
                default:
                    return false;
            }
        }

        public boolean isModifierKey() {
            return keycode == KeyEvent.VK_ENTER
                || keycode == KeyEvent.VK_ALT
                || keycode == KeyEvent.VK_SHIFT
                || keycode == KeyEvent.VK_CONTROL
                || keycode == KeyEvent.VK_ESCAPE;
        }
    }
}