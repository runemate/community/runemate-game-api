package com.runemate.game.api.hybrid.util;

import com.runemate.game.api.hybrid.location.*;
import com.runemate.ui.setting.open.*;
import com.google.common.base.*;
import java.lang.reflect.*;

public class CoordinateSettingConverter implements SettingConverter {

    @Override
    public Object fromString(final String s, final Type type) {
        if (type == Coordinate.class) {
            return fromString(s);
        }
        return null;
    }

    @Override
    public String toString(final Object object) {
        if (object instanceof Coordinate) {
            final var coord = (Coordinate) object;
            return coord.getX() + ":" + coord.getY() + ":" + coord.getPlane();
        }
        return null;
    }

    public static Coordinate fromString(final String s) {
        if (Strings.emptyToNull(s) == null) {
            return null;
        }
        final var split = s.split(":");
        int x = Integer.parseInt(split[0]);
        int y = Integer.parseInt(split[1]);
        int plane = Integer.parseInt(split[2]);
        return new Coordinate(x, y, plane);
    }
}
