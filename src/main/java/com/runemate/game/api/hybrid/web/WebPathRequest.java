package com.runemate.game.api.hybrid.web;

import com.runemate.client.boot.*;
import com.runemate.game.api.client.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.location.navigation.*;
import com.runemate.game.api.hybrid.region.*;
import java.util.*;
import lombok.*;
import lombok.experimental.*;
import lombok.extern.log4j.*;
import org.jetbrains.annotations.*;

@Data
@Log4j2
@Accessors(chain = true)
public class WebPathRequest {

    private static boolean SHOWN_ALERT = false;
    private static EnumSet<Landmark> SUPPORTED_LANDMARKS = EnumSet.of(
        Landmark.BANK,
        Landmark.PRAYER_ALTAR,
        Landmark.GRAND_EXCHANGE_CLERK
    );

    private static WebPathBuilder builder;

    private Locatable start;
    private Locatable destination;
    private Landmark landmark;
    private boolean usingTeleports = true;

    private WebPathRequest() {

    }

    public static WebPathRequest builder() {
        return new WebPathRequest();
    }

    private boolean validate() {
        if (destination == null && landmark == null) {
            log.warn("Must defined one of 'destination' or 'landmark'");
            return false;
        }

        if (landmark != null) {
            if (!SUPPORTED_LANDMARKS.contains(landmark)) {
                log.warn("Default web currently only supports Landmark.BANK and Landmark.PRAYER_ALTAR");
                return false;
            }
        }

        if (start == null) {
            start = Optional.ofNullable(Players.getLocal()).map(Actor::getServerPosition).orElse(null);
        }
        if (start == null) {
            log.warn("Failed to determine a valid start position");
            return false;
        }

        return true;
    }

    @Nullable
    public WebPath build() {
        if (!validate()) {
            return null;
        }

        try {
            if (builder == null) {
                log.error("Failed to create WebPathBuilder");
                return null;
            }

            var path = builder.build(this);
            if (path != null) {
                path.setRequest(this);
            }
            return path;
        } catch (NoClassDefFoundError e) {
            log.error("Failed to load navigation library", e);
            if (!SHOWN_ALERT) {
                SHOWN_ALERT = true;
                ClientUI.showAlert(ClientUI.AlertLevel.ERROR, "Failed to load the web navigation library which could result in bots failing to work correctly.");
            }
        }

        return null;
    }

}
