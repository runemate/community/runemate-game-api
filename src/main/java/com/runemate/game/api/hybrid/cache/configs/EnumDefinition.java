package com.runemate.game.api.hybrid.cache.configs;

import java.io.*;

public interface EnumDefinition {
    int getId();

    int[] getKeys();

    int[] getKeys(Serializable value);

    Serializable[] getValues();

    Serializable get(int key);

    Serializable get(int key, Serializable dflt);

    int getInt(int key);

    int getDefaultInt();

    String getString(int key);

    String getDefaultString();

    int getEntryCount();
}
