package com.runemate.game.api.hybrid.entities;

import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import java.util.*;
import javax.annotation.*;
import lombok.*;

/**
 * A real-player's avatar
 */
public interface Player extends Actor {

    /**
     * Gets the combat level of this player
     *
     * @return the combat level, otherwise -1
     */
    int getCombatLevel();

    /**
     * The id of this players alternative, npc-based form.
     * Typically available when a player is transformed such as when a monkey at Ape Atoll.
     */
    int getNpcTransformationId();

    /**
     * The total skill level of this player.
     *
     * @return 0 if your weapon is unsheathed on RS3 and 0 if some unknown condition is fufilled on OSRS, otherwise the total level of the player
     */
    int getTotalLevel();

    /**
     * Whether or not the player is female
     *
     * @return True if female, otherwise false
     */
    boolean isFemale();

    @NonNull
    List<ItemDefinition> getWornItems();

    @Nullable
    ItemDefinition getWornItem(Equipment.Slot slot);

    int getTeamId();

    long getAppearanceHash();
}
