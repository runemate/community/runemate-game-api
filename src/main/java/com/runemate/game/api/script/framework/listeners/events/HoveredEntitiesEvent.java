package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.entities.*;
import java.util.*;
import lombok.*;

@Value
public class HoveredEntitiesEvent implements Event {

    Collection<Entity> entities;

}
