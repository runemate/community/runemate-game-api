package com.runemate.game.api.script.framework.listeners.events;

import com.runemate.game.api.hybrid.location.*;
import lombok.*;

@Value
public class SceneUpdatedEvent implements Event {

    Coordinate previousBase;
    Coordinate currentBase;
}
