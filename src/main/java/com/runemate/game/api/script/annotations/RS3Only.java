package com.runemate.game.api.script.annotations;

import java.lang.annotation.*;

/**
 * An annotation marking a method as only being compatible with RS3.
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Deprecated
public @interface RS3Only {
}
