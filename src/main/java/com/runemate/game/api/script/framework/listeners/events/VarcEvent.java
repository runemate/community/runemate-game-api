package com.runemate.game.api.script.framework.listeners.events;

import lombok.*;

@Value
public class VarcEvent implements Event {
    boolean string;
    int index;
    Object previousValue;
    Object value;

    public VarcEvent(boolean string, int index, Object previousValue, Object value) {
        this.string = string;
        this.index = index;
        this.previousValue = previousValue;
        this.value = value;
    }

    public boolean isString() {
        return string;
    }

    public boolean isInt() {
        return !string;
    }
}
