package com.runemate.game.api.osrs.local.hud.interfaces;

import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.script.*;
import java.util.*;
import java.util.regex.*;
import java.util.stream.*;
import lombok.*;
import lombok.extern.log4j.*;

@Log4j2
public enum Prayer {
    THICK_SKIN(115, 0, 1),
    BURST_OF_STRENGTH(116, 1, 4),
    CLARITY_OF_THOUGHT(117, 2, 7),
    ROCK_SKIN(118, 3, 10),
    SUPERHUMAN_STRENGTH(119, 4, 13),
    IMPROVED_REFLEXES(120, 5, 16),
    RAPID_RESTORE(121, 6, 19),
    RAPID_HEAL(122, 7, 22),
    PROTECT_ITEM(123, 8, 25),
    STEEL_SKIN(124, 9, 28),
    ULTIMATE_STRENGTH(125, 10, 31),
    INCREDIBLE_REFLEXES(126, 11, 34),
    PROTECT_FROM_MAGIC(127, 12, 37),
    PROTECT_FROM_MISSILES(128, 13, 40),
    PROTECT_FROM_MELEE(129, 14, 43),
    RETRIBUTION(131, 15, 46),
    REDEMPTION(130, 16, 49),
    SMITE(132, 17, 52),
    SHARP_EYE(133, 18, 8),
    MYSTIC_WILL(134, 19, 9),
    HAWK_EYE(502, 20, 26),
    MYSTIC_LORE(503, 21, 27),
    EAGLE_EYE(504, 22, 44),
    MYSTIC_MIGHT(505, 23, 45),
    CHIVALRY(945, 25, 60),
    PIETY(946, 26, 70),
    RIGOUR(1420, 24, 74),
    AUGURY(1421, 27, 77),
    PRESERVE(947, 28, 55),
    DEADEYE(-1, 22, 62), // TODO: prayer spriteId is not used in game-api
    MYSTIC_VIGOUR(-1, 23, 63);
    private static final int ACTIVATED_PRAYERS = 83;
    private static final int PRAYER_INTERFACE = 541;
    private static final int QUICK_PRAYERS_ACTIVATED = 375;
    private static final int QUICK_PRAYER_BUTTON_CONTAINER = 160;
    private static final int SELECTED_QUICK_PRAYERS = 84;
    private static final Pattern TOGGLE_ACTION = Pattern.compile("^(Dea|A)ctivate$");
    private final String name;
    private final int spriteId;
    private final int varpIndex;
    private final int requiredLevel;

    Prayer(int spriteId, int varpIndex, int requiredLevel) {
        String[] words = name().split("_");
        StringBuilder name = new StringBuilder();
        for (int i = 0; i < words.length; i++) {
            String word = words[i];
            if (i != 0) {
                name.append(' ');
            }
            name.append("OF".equals(word) || "FROM".equals(word) ? word.toLowerCase() :
                word.charAt(0) + word.substring(1).toLowerCase());
        }
        this.name = name.toString();
        this.spriteId = spriteId;
        this.varpIndex = varpIndex;
        this.requiredLevel = requiredLevel;
    }

    public static List<Prayer> getActivePrayers() {
        List<Prayer> active = new ArrayList<>();
        Varp activated = Varps.getAt(ACTIVATED_PRAYERS);
        for (Prayer p : values()) {
            if (activated.getValueOfBit(p.varpIndex) == 1 && p.isUnlocked()) {
                active.add(p);
            }
        }
        return active;
    }

    public static List<Prayer> getSelectedQuickPrayers() {
        return Arrays.stream(values()).filter(Prayer::isSelectedAsQuickPrayer).collect(Collectors.toList());
    }

    public static boolean isQuickPraying() {
        return Varps.getAt(QUICK_PRAYERS_ACTIVATED).getValueOfBit(0) == 1;
    }

    public static boolean toggleQuickPrayers() {
        log.info("Toggling quick-prayers");
        InterfaceComponent toggleComponent = getQuickPrayerToggle();
        if (toggleComponent != null) {
            boolean activated = isQuickPraying();
            if (toggleComponent.interact(TOGGLE_ACTION, "Quick-prayers")) {
                return Execution.delayUntil(() -> isQuickPraying() != activated, 2000);
            }
        }
        return false;
    }

    private static InterfaceComponent getQuickPrayerToggle() {
        return Interfaces.newQuery()
            .containers(QUICK_PRAYER_BUTTON_CONTAINER)
            .types(InterfaceComponent.Type.CONTAINER)
            .actions(TOGGLE_ACTION)
            .results()
            .first();
    }

    private static boolean openQuickPrayerSetup() {
        log.info("Opening quick-prayer configuration");
        final InterfaceComponent mmButton = getQuickPrayerToggle();
        return isQuickPrayerSetupOpen()
            || mmButton != null
            && mmButton.interact("Setup")
            && Execution.delayUntil(Prayer::isQuickPrayerSetupOpen, 1200, 1800);
    }

    /**
     * Sets the players quick-prayers to be only the ones provided
     *
     * @param prayers prayers to set
     * @return if selected quick-prayers are correct and we successfully confirmed
     */
    public static boolean setQuickPrayers(final Prayer... prayers) {
        return setQuickPrayers(Arrays.asList(prayers));
    }

    public static boolean setQuickPrayers(final Collection<Prayer> prayers) {
        if (getNeededQuickPrayers(prayers).isEmpty() && getUnneededQuickPrayers(prayers).isEmpty()) {
            log.debug("Quick-prayers are already set correctly");
            return true;
        }
        log.info("Setting quick-prayers to {}", prayers);
        if (openQuickPrayerSetup()) {
            final var needed = getNeededQuickPrayers(prayers);
            if (!needed.stream().allMatch(Prayer::toggleQuickPrayerSelected)) {
                return false;
            }
            if (!getUnneededQuickPrayers(prayers).isEmpty()) {
                if (!needed.isEmpty()) {
                    // Wait for unneeded quick prayers to be deselected from selecting others
                    Execution.delayUntil(
                        () -> getUnneededQuickPrayers(prayers).stream().noneMatch(Prayer::isSelectedAsQuickPrayer),
                        800,
                        1200
                    );
                }
                if (!getUnneededQuickPrayers(prayers).stream().allMatch(Prayer::toggleQuickPrayerSelected)) {
                    return false;
                }
            }
        }
        return Arrays.stream(Prayer.values()).allMatch(p -> p.isSelectedAsQuickPrayer() == prayers.contains(p))
            && confirmQuickPrayerSelection();
    }

    private static List<Prayer> getNeededQuickPrayers(final Collection<Prayer> prayers) {
        return Arrays.stream(values())
            .filter(prayer -> prayers.contains(prayer) && !prayer.isSelectedAsQuickPrayer())
            .collect(Collectors.toList());
    }

    private static List<Prayer> getUnneededQuickPrayers(final Collection<Prayer> prayers) {
        return Arrays.stream(values())
            .filter(prayer -> !prayers.contains(prayer) && prayer.isSelectedAsQuickPrayer())
            .collect(Collectors.toList());
    }

    private static boolean toggleQuickPrayerSelected(final Prayer prayer) {
        final boolean selected = prayer.isSelectedAsQuickPrayer();
        final InterfaceComponent ic = Interfaces.newQuery()
            .containers(77)
            .types(InterfaceComponent.Type.SPRITE)
            .names(prayer.toString())
            .actions("Toggle")
            .results()
            .first();
        return ic != null
            && ic.isVisible()
            && ic.interact("Toggle")
            && Execution.delayUntil(() -> prayer.isSelectedAsQuickPrayer() != selected, 1200, 1800);
    }

    /**
     * Determines whether the quick-prayer configuration menu is open or not
     *
     * @return if the quick-prayer config menu is open
     */
    //Exposing in case setQuickPrayers fails to confirm and user needs to confirm the interface has closed
    public static boolean isQuickPrayerSetupOpen() {
        final InterfaceComponent ic = getQuickPrayerConfirmButton();
        return ic != null && ic.isVisible();
    }

    private static InterfaceComponent getQuickPrayerConfirmButton() {
        return Interfaces.newQuery().containers(77).types(InterfaceComponent.Type.SPRITE).actions("Done").results().first();
    }

    public static boolean confirmQuickPrayerSelection() {
        log.info("Confirming quick-prayer configuration");
        final var ic = Interfaces.newQuery().containers(77).types(InterfaceComponent.Type.SPRITE).actions("Done").results().first();
        return !isQuickPrayerSetupOpen()
            || ic != null
            && ic.isVisible()
            && ic.interact("Done")
            && Execution.delayWhile(Prayer::isQuickPrayerSetupOpen, 1200, 1800);
    }

    public static int getPoints() {
        return Skill.PRAYER.getCurrentLevel();
    }

    public static int getMaximumPoints() {
        return Skill.PRAYER.getBaseLevel();
    }

    public boolean isActivatable() {
        if (!isUnlocked()) {
            return false;
        }
        Varbit unidentifiedVarbit = Varbits.load(5314);
        if (unidentifiedVarbit == null) {
            return false;
        }
        int unidentifiedVarbitValue = unidentifiedVarbit.getValue();
        if (Skill.PRAYER.getBaseLevel() < requiredLevel && unidentifiedVarbitValue == 0) {
            return false;
        }
        WorldOverview worldOverview = Worlds.getCurrentOverview();
        if (worldOverview == null) {
            return false;
        }
        Varbit kingsRansomVarbit = Varbits.load(3909);
        switch (varpIndex) {
            case 8:
                if (unidentifiedVarbitValue == 1
                    || worldOverview.getWorldTypes().contains(WorldType.HIGH_RISK)
                    || worldOverview.getWorldTypes().contains(WorldType.DEADMAN)) {
                    return false;
                }
                break;
            case 15:
            case 16:
            case 17:
                if (unidentifiedVarbitValue == 0 && !worldOverview.isMembersOnly()) {
                    return false;
                }
                break;
            case 24:
                Varbit rigourVarbit = Varbits.load(5451);
                if ((unidentifiedVarbitValue == 0 && (rigourVarbit == null || rigourVarbit.getValue() == 0))
                    || !worldOverview.isMembersOnly()
                    || Skill.DEFENCE.getBaseLevel() < 70) {
                    return false;
                }
                break;
            case 25:
                if ((unidentifiedVarbitValue == 0 && (kingsRansomVarbit == null || kingsRansomVarbit.getValue() < 8))
                    || !worldOverview.isMembersOnly()
                    || Skill.DEFENCE.getBaseLevel() < 65) {
                    return false;
                }
                break;
            case 26:
                if ((unidentifiedVarbitValue == 0 && (kingsRansomVarbit == null || kingsRansomVarbit.getValue() < 8))
                    || !worldOverview.isMembersOnly()
                    || Skill.DEFENCE.getBaseLevel() < 70) {
                    return false;
                }
                break;
            case 27:
                Varbit auguryVarbit = Varbits.load(5452);
                if ((unidentifiedVarbitValue == 0 && (auguryVarbit == null || auguryVarbit.getValue() == 0))
                    || !worldOverview.isMembersOnly()
                    || Skill.DEFENCE.getBaseLevel() < 70) {
                    return false;
                }
                break;
            case 28:
                Varbit preserveVarbit = Varbits.load(5453);
                if (unidentifiedVarbitValue == 1
                    || preserveVarbit == null
                    || preserveVarbit.getValue() == 0
                    || !worldOverview.isMembersOnly()) {
                    return false;
                }
                break;
            default:
                return true;
        }
        return true;
    }

    public boolean activate() {
        log.info("Activating {}", this);
        return isActivated() || toggle();
    }

    public boolean deactivate() {
        log.info("Deactivating {}", this);
        return !isActivated() || toggle();
    }

    public InterfaceComponent getComponent() {
        return Interfaces.newQuery()
            .containers(PRAYER_INTERFACE)
            .grandchildren(false)
            .types(InterfaceComponent.Type.CONTAINER)
            .names(name, "\"" + name + "\"")
            .results()
            .first();
    }

    public int getRequiredLevel() {
        return requiredLevel;
    }

    public boolean isActivated() {
        if (!isUnlocked()) {
            return false;
        }
        return Varps.getAt(ACTIVATED_PRAYERS).getValueOfBit(varpIndex) == 1;
    }

    public boolean isSelectedAsQuickPrayer() {
        if (!isUnlocked()) {
            return false;
        }
        return Varps.getAt(SELECTED_QUICK_PRAYERS).getValueOfBit(varpIndex) == 1;
    }

    @Override
    public String toString() {
        return name;
    }

    private boolean toggle() {
        final boolean initial = isActivated();
        if (Skill.PRAYER.getBaseLevel() >= getRequiredLevel()
            && Skill.PRAYER.getCurrentLevel() > 0) {
            if (ControlPanelTab.PRAYER.isOpen() || ControlPanelTab.PRAYER.open()) {
                if (isQuickPrayerSetupOpen()) { //Account for Quickprayer setup support
                    confirmQuickPrayerSelection();
                    return false;
                }
                final InterfaceComponent button = getComponent();
                if (button != null && button.isVisible() && button.click()) {
                    return Execution.delayUntil(() -> isActivated() != initial, 2000);
                }
            }
        }
        return isActivated() != initial;
    }

    /**
     * Jagex introduced prayers which replaced their weaker counterpart but did not change the varpIndex
     *
     * @return this prayer is unlocked for this player
     */
    private boolean isUnlocked() {
        if (this == MYSTIC_MIGHT || this == MYSTIC_VIGOUR) {
            int mv = VarbitID.MYSTIC_VIGOUR_UNLOCKED.getValue();
            return (mv != 1 && this == MYSTIC_MIGHT) || (mv == 1 && this == MYSTIC_VIGOUR);
        }
        if (this == EAGLE_EYE || this == DEADEYE) {
            int de = VarbitID.DEADEYE_UNLOCKED.getValue();
            return (de != 1 && this == EAGLE_EYE) || (de == 1 && this == DEADEYE);
        }
        return true;
    }

    @RequiredArgsConstructor
    public enum Filter {
        SHOW_LOWER_TIERS(6605),
        SHOW_LOWER_TIERS_MULTISKILL(6609),
        SHOW_RAPID_HEALING(6606),
        SHOW_MISSING_LEVEL(6607),
        SHOW_MISSING_REQUIREMENTS(12137);

        private final int varbit;

        public boolean isEnabled() {
            final var varbit = Varbits.load(this.varbit);
            return varbit != null && varbit.getValue() == 0;
        }
    }
}