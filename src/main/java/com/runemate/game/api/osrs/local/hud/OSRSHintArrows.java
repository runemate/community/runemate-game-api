package com.runemate.game.api.osrs.local.hud;

import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.queries.results.*;
import java.util.*;
import java.util.function.*;

public final class OSRSHintArrows {
    private OSRSHintArrows() {
    }

    public static HintArrowQueryResults getLoaded(final Predicate<HintArrow> filter) {
        final List<HintArrow> arrows = new ArrayList<>(1);
        final OSRSHintArrow arrow = OSRSHintArrow.getInstance();
        if (arrow.isValid() && (filter == null || filter.test(arrow))) {
            arrows.add(arrow);
        }
        return new HintArrowQueryResults(arrows, null);
    }
}
