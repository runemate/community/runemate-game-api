package com.runemate.game.api.osrs.projection;

import static com.runemate.game.api.hybrid.region.Region.RenderFlags.*;

import com.runemate.client.bind.*;
import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.region.*;
import com.runemate.game.api.osrs.local.hud.interfaces.*;
import java.awt.*;
import java.awt.geom.Area;
import java.util.*;
import java.util.concurrent.*;
import lombok.*;
import lombok.extern.log4j.*;

@Log4j2(topic = "Projection")
public final class OSRSProjection {

    private static final int MAX_MINIMAP_DISTANCE = 18;
    private static final double UNIT = Math.PI / 1024d;
    public static final int[] COSINE = new int[2048];
    public static final int[] SINE = new int[2048];
    private static final Map<String, Integer> projectionCache = new ConcurrentHashMap<>(5);

    static {
        for (int i = 0; i < 2048; ++i) {
            SINE[i] = (int) (65536.0D * StrictMath.sin((double) i * UNIT));
            COSINE[i] = (int) (65536.0D * StrictMath.cos((double) i * UNIT));
        }
    }

    private OSRSProjection() {
    }

    public static InteractablePoint coordinateToMinimap(final Coordinate coordinate, Map<String, Object> cache) {
        if (!prepareCache(cache)) {
            return null;
        }

        Coordinate.HighPrecision hp = coordinate.getHighPrecisionPosition();
        Coordinate.HighPrecision php = ((Coordinate) cache.get("LocalPosition")).getHighPrecisionPosition();

        final int dx = hp.getX() - php.getX();
        final int dy = hp.getY() - php.getY();

        final double minimapZoomSetting;
        if (Environment.getClientType() == ClientType.RUNELITE) {
            minimapZoomSetting = OpenRuneLite.getMinimapZoom();
        } else {
            minimapZoomSetting = 4d;
        }

        final int render = MAX_MINIMAP_DISTANCE << 7;
        final double scale = (4d / minimapZoomSetting);
        final int maxDistance = (int) (render * scale);

        //No point calculating if it's out of view
        if (dx * dx + dy * dy >= maxDistance * maxDistance) {
            return null;
        }

        final double zoom = minimapZoomSetting / (1 << 7);
        final int x = (int) (dx * zoom);
        final int y = (int) (dy * zoom);

        final int angle = OpenClient.getMinimapAngle() & 0x7ff;
        final int sin = SINE[angle];
        final int cos = COSINE[angle];

        final int rx = cos * x + sin * y >> 16;
        final int ry = sin * x - cos * y >> 16;

        final Rectangle bounds = (Rectangle) cache.get("MinimapBounds");
        int minimapX = bounds.x + (bounds.width >> 1) + rx;
        int minimapY = bounds.y + (bounds.height >> 1) + ry;
        return new InteractablePoint(minimapX, minimapY);

    }

    public static InteractablePoint coordinateToMinimap(final Coordinate coordinate) {
        return coordinateToMinimap(coordinate, new HashMap<>());
    }

    public static InterfaceComponent getMinimap() {
        final int rootWindow = InterfaceContainers.getRootIndex();
        if (projectionCache.containsKey("MM-" + rootWindow)) {
            InterfaceComponent component = Interfaces.getAt(rootWindow, projectionCache.get("MM-" + rootWindow));
            if (component != null) {
                return component;
            } else {
                projectionCache.remove("MM-" + rootWindow);
            }
        }
        final InterfaceComponent component = Interfaces.newQuery()
            .types(InterfaceComponent.Type.SPRITE)
            .grandchildren(false)
            .containers(rootWindow)
            .specializationIndicators(1338)
            .results()
            .first();
        //log.info(component + " is the minimap");
        if (component == null) {
            return null;
        }
        projectionCache.put("MM-" + rootWindow, component.getIndex());
        return component;
    }

    public static Shape getViewport() {
        if (OptionsTab.getCanvasMode() == OptionsTab.CanvasMode.FIXED) {
            InterfaceComponent viewport = Interfaces.getAt(InterfaceContainers.getRootIndex(), 9);
            return viewport != null ? viewport.getBounds() : null;
        }
        return dynamicViewport();
    }

    public static Point[] multiAbsoluteToScreen(int height, int[] xs, int[] ys, int[] zs, Map<String, Object> cache) {
        int viewportInset = (Integer) cache.computeIfAbsent(
            "ViewportInset",
            key -> OptionsTab.CanvasMode.FIXED.equals(OptionsTab.getCanvasMode()) ? 4 : 0
        );
        return multiAbsoluteToScreen(viewportInset, viewportInset, height, xs, ys, zs, cache);
    }

    public static Point[] multiAbsoluteToScreen(
        int viewportXOffset,
        int viewportYOffset,
        int elevate,
        int[] xs,
        int[] ys,
        int[] zs,
        Map<String, Object> cache
    ) {
        if (xs.length != ys.length || ys.length != zs.length) {
            throw new IllegalArgumentException("[Projection] All three arrays of points must be the same size (currently "
                + xs.length
                + ", "
                + ys.length
                + ", "
                + zs.length
                + ')');
        }
        Point[] projected = new Point[xs.length];
        if (projected.length == 0) {
            return projected;
        }

        final OpenWorldView world = OpenWorldView.getTopLevelWorldView();
        final OpenRegion scene = world.getScene();

        final int cameraX;
        final int cameraY; //TODO review potential inversions
        final int cameraZ; //TODO review potential inversions
        if (Environment.getClientType() == ClientType.RUNELITE) {
            cameraX = (Integer) computeIfAbsent(cache, "Client.cameraX", OpenCamera::getLegacyCameraX);
            cameraY = (Integer) computeIfAbsent(cache, "Client.cameraY", OpenCamera::getLegacyCameraY); //intentional inversion
            cameraZ = (Integer) computeIfAbsent(cache, "Client.cameraZ", OpenCamera::getLegacyCameraZ); //intentional inversion
        } else {
            cameraX = (Integer) computeIfAbsent(cache, "Client.cameraX", scene::getCameraX);
            cameraY = (Integer) computeIfAbsent(cache, "Client.cameraY", scene::getCameraZ); //intentional inversion
            cameraZ = (Integer) computeIfAbsent(cache, "Client.cameraZ", scene::getCameraY); //intentional inversion
        }
        final int yaw = (Integer) computeIfAbsent(cache, "Client.cameraYaw", OpenCamera::getJagYaw);
        final int pitch = (Integer) computeIfAbsent(cache, "Client.cameraPitch", OpenCamera::getJagPitch);
        final int viewportScale = (Integer) computeIfAbsent(cache, "Client.viewportScale", OpenCamera::getJagZoom);
        final int viewportWidth = (Integer) computeIfAbsent(cache, "Client.viewportWidth", OpenClient::getViewportWidth);
        final int viewportHeight = (Integer) computeIfAbsent(cache, "Client.viewportHeight", OpenClient::getViewportHeight);

        Coordinate regionBase = (Coordinate) computeIfAbsent(cache, "region_base", Scene::getBase);
        Coordinate playerPosition = (Coordinate) computeIfAbsent(cache, "player_position", () -> {
            Player player = Players.getLocal();
            return player != null ? player.getPosition(regionBase) : null;
        });
        if (playerPosition == null) {
            return null;
        }
        Set<Point> spanningCoordinates = new HashSet<>(4);
        for (int index = 0; index < projected.length; ++index) {
            int x = xs[index];
            int z = zs[index];
            if (x < 128 || x > 13056 || z < 128 || z > 13056) {
                continue;
            }
            spanningCoordinates.add(new Point(x >> 7, z >> 7));
        }

        for (Point coordinate : spanningCoordinates) {
            String key = coordinate.x + "_" + coordinate.y + "_drawn";
            if (!cache.containsKey(key)) {
                //Stores the results of calculating which tiles were drawn since if they weren't drawn the vertices on it wouldn't be either..
                cache.put(key, Camera.isSceneTileDrawn(regionBase.getPlane(), coordinate.x, coordinate.y, cache));
            }
        }

        for (int index = 0; index < projected.length; ++index) {
            int x = xs[index];
            int z = zs[index];
            if (x < 128 || x > 13056 || z < 128 || z > 13056) {
                continue;
            }
            int y = ys[index];
            //Checks to see if the tile was drawn since if it wasn't then it's not visible.
            if ((boolean) cache.get((x >> 7) + "_" + (z >> 7) + "_drawn")) {
                //Below is exactly how y is supposed to be, don't touch it
                y += getLinearInterpolatedHeight(x, z, regionBase.getPlane(), cache);
                y += elevate;
                y -= cameraZ;
                x -= cameraX;
                z -= cameraY;
                int sinYaw = SINE[yaw];
                int cosYaw = COSINE[yaw];
                int angle = z * sinYaw + x * cosYaw >> 16;
                z = z * cosYaw - x * sinYaw >> 16;
                x = angle;
                int sinPitch = SINE[pitch];
                int cosPitch = COSINE[pitch];
                angle = y * cosPitch - z * sinPitch >> 16;
                z = y * sinPitch + z * cosPitch >> 16;
                y = angle;
                if (z < 50) {
                    continue;
                }
                int projectedX = (x * viewportScale / z) + (viewportWidth / 2);
                if (projectedX < 0) {
                    continue;
                }
                int projectedY = (y * viewportScale / z) + (viewportHeight / 2);
                if (projectedY < 0) {
                    continue;
                }
                projected[index] = new Point(viewportXOffset + projectedX, viewportYOffset + projectedY);
            }
        }
        return projected;
    }

    //Logic by BrainFree
    private static int getLinearInterpolatedHeight(int hpX, int hpY, int plane, Map<String, Object> cache) {
        int x = hpX >> 7; //The macro local coordinate.
        int y = hpY >> 7;
        if (x < 0 || y < 0 || x >= 104 || y >= 104) {
            return 0; //Not within the local region
        }
        byte[][][] renderFlags = (byte[][][]) computeIfAbsent(cache, "LandscapeData", Scene::getRenderFlags);
        int zidx;
        if (plane < 3 && ((renderFlags[1][x][y] & LOWER_OBJECTS_TO_OVERRIDE_CLIPPING)
            == LOWER_OBJECTS_TO_OVERRIDE_CLIPPING)) {
            zidx = plane + 1;
        } else if (plane < 3 && renderFlags[plane + 1][x][y] == Region.RenderFlags.CLIPPED) {
            return getAverageCoordinateHeight(hpX, hpY, plane, cache);
        } else {
            zidx = plane;
        }
        int[][] heights = (int[][]) computeIfAbsent(cache, "GroundHeightsFor" + zidx, () -> Scene.getTileHeights()[zidx]);
        int dx = hpX & 127;
        int dy = hpY & 127;
        int xOff = dx * heights[x + 1][y] + heights[x][y] * (128 - dx) >> 7;
        int yOff = (128 - dx) * heights[x][y + 1] + dx * heights[x + 1][y + 1] >> 7;
        return dy * yOff + (128 - dy) * xOff >> 7;
    }

    private static int getAverageCoordinateHeight(int x, int y, int plane, Map<String, Object> cache) {
        int localX = x >> 7; //The macro local coordinate.
        int localY = y >> 7;
        if (localX < 0 || localY < 0 || localX >= 104 || localY >= 104) {
            return 0; //Not within the local region
        }
        byte[][][] landscapeBytes = (byte[][][]) computeIfAbsent(cache, "LandscapeData", () -> Scene.getRenderFlags()[1]);
        int zidx;
        if (plane < 3 && ((landscapeBytes[1][localX][localY] & LOWER_OBJECTS_TO_OVERRIDE_CLIPPING) == LOWER_OBJECTS_TO_OVERRIDE_CLIPPING)) {
            zidx = plane + 1;
        } else {
            zidx = plane;
        }
        int[][] planeHeights = (int[][]) computeIfAbsent(cache, "GroundHeightsFor" + zidx, () -> Scene.getTileHeights()[zidx]);
        double total = planeHeights[localX][localY] + planeHeights[localX + 1][localY] + planeHeights[localX][localY + 1] + planeHeights[
            localX
                + 1][localY + 1];
        return (int) (total / 4);
    }

    private static int getHeightOfSWCoordinate(int x, int y, int plane, Map<String, Object> cache) {
        int localX = x >> 7; //The macro local coordinate.
        int localY = y >> 7;
        if (localX < 0 || localY < 0 || localX >= 104 || localY >= 104) {
            return 0; //Not within the local region
        }
        byte[][][] landscapeBytes = (byte[][][]) computeIfAbsent(cache, "LandscapeData", () -> Scene.getRenderFlags()[1]);
        int zidx;
        if (plane < 3 && ((landscapeBytes[1][localX][localY] & LOWER_OBJECTS_TO_OVERRIDE_CLIPPING)
            == LOWER_OBJECTS_TO_OVERRIDE_CLIPPING)) {
            zidx = plane + 1;
        } else {
            zidx = plane;
        }
        int[][] planeHeights = (int[][]) computeIfAbsent(cache, "GroundHeightsFor" + zidx, () -> Scene.getTileHeights()[zidx]);
        return planeHeights[localX][localY];
    }

    @SneakyThrows(Exception.class)
    private static Object computeIfAbsent(Map<String, Object> map, String key, Callable<Object> function) {
        Object value = map.get(key);
        if (value == null) {
            map.put(key, value = function.call());
        }
        return value;
    }

    private static boolean prepareCache(Map<String, Object> cache) {
        if (!cache.containsKey("LocalPosition")) {
            if (!cache.containsKey("LocalPlayer")) {
                Player local = Players.getLocal();
                if (local != null) {
                    cache.put("LocalPlayer", local);
                } else {
                    return false;
                }
            }
            Coordinate localPos = ((Player) cache.get("LocalPlayer")).getPosition((Coordinate) cache.get("RegionBase"));
            if (localPos != null) {
                cache.put("LocalPosition", localPos);
            } else {
                return false;
            }
        }
        if (!cache.containsKey("MinimapBounds")) {
            InterfaceComponent component = getMinimap();
            if (component != null) {
                Rectangle bounds = component.getBounds();
                if (bounds != null) {
                    cache.put("MinimapBounds", bounds);
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
        return true;
    }

    private static Shape dynamicViewport() {
        boolean paneled = OptionsTab.isUsingSidePanels();
        Area area = new Area(Screen.getBounds());

        //For paneled mode, exclude last 5 (minimap, chat, leftMenuButtons, rightMenuButtons, menu
        //Otherwise exclude last 3 (minimap, chat, menu)
        Interfaces.newQuery().containers(paneled ? 164 : 161).grandchildren(false).results()
            .stream()
            .sorted(Comparator.comparingInt(InterfaceComponent::getIndex).reversed())
            .limit(paneled ? 5 : 3)
            .forEach(r -> {
                Rectangle bounds = r.getBounds();
                if (bounds != null) {
                    area.subtract(new Area(bounds));
                }
            });

        return area;
    }
}
