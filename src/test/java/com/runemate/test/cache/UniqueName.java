package com.runemate.test.cache;

import com.runemate.client.game.open.*;
import java.util.*;

class UniqueName {

    private final Set<String> used = new HashSet<>();

    private static String sanitize(String in) {
        String s = JagTags.remove(in)
            .toUpperCase()
            .replace(' ', '_')
            .replaceAll("[^a-zA-Z0-9_]", "");
        if (s.isEmpty()) {
            return null;
        }
        if (Character.isDigit(s.charAt(0))) {
            return "_" + s;
        } else {
            return s;
        }
    }

    public String name(String input, int id) {
        String name = sanitize(input);
        if (name == null) {
            return null;
        }

        if (used.contains(name)) {
            name = name + "_" + id;
        }

        used.add(name);
        return name;
    }
}
