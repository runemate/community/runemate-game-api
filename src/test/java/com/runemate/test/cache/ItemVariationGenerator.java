package com.runemate.test.cache;

import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.cache.*;
import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.datatype.guava.*;
import com.google.common.collect.*;
import java.io.*;
import org.apache.commons.cli.*;

public class ItemVariationGenerator {

    public static void main(String[] args) throws JsonProcessingException {
        Options options = new Options();
        options.addOption("c", "cache", true, "Cache root");
        options.addOption("d", "directory", true, "Output directory");

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd;
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.err.println("Error parsing command line options: " + e.getMessage());
            System.exit(-1);
            return;
        }


        if (!cmd.hasOption("c")) {
            System.err.println("Cache root not specified");
            System.exit(-1);
            return;
        }

        File cache = new File(cmd.getOptionValue("c"));
        if (!cache.exists()) {
            System.err.println("Cache does not exist: " + cache);
            System.exit(-1);
            return;
        }

        File directory = new File(cmd.getOptionValue("directory"));
        if (!directory.exists()) {
            directory.mkdirs();
        }


        JS5CacheController.setDefaultCacheController(cache);
        Multimap<String, Integer> variations = HashMultimap.create();
        for (ItemDefinition ext : ItemDefinition.loadAll()) {
            CacheItemDefinition parsed = ((CacheItemDefinition.Extended) ext).getBase();
            String name = cleanItemName(parsed.getName());
            if (name.isEmpty() || "null".equals(name)) {
                continue;
            }
            variations.put(name, parsed.getId());
        }

        // Remove items that have no variations
        variations.keySet().removeIf(key -> variations.get(key).size() == 1);
        System.out.println("Found " + variations.keySet().size() + " items with variations");

        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        mapper.registerModule(new GuavaModule());

        String json = mapper.writeValueAsString(variations);
        try (PrintWriter writer = new PrintWriter(new File(directory, "item_variations.json"))) {
            writer.write(json);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static String cleanItemName(String name) {
        return name.replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
    }
}
